angular.module('fortinsocial.controllers', [])


        .run(function($rootScope,$http, $location, $timeout,$ionicPlatform) {
            //$http.defaults.withCredentials = true;
            //$http.defaults.headers.common['Access-Control-Allow-Origin'] = 'http://localhost:8100';
            $http.defaults.headers.common['Authorization'] = 'Basic cmVmcmlwbGF5c2VydmljZTpiZjlhNDIyZWUwMTgwZjI5ZWEzN2Q5NTFkNDkxNzdjZg==';
            $http.defaults.headers.get = {};

            $rootScope.$on('app:notification', function ( evt, data ) {
                if ( data.redirectTo )
                    $timeout(function(){
                        $location.path( data.redirectTo );
                    }, 1);
            });

            $rootScope.$on('$stateChangeSuccess', function(event, to, toParams, from, fromParams) {
                //save the previous state in a rootScope variable so that it's accessible from everywhere
                $rootScope.previousState = from;
            });

            $rootScope.analytics = function(title)
            {
                    if(window.ga)
                    {
                        window.ga.startTrackerWithId('UA-106020434-1');
                        window.ga.trackView(title);
                    }
            };

            $rootScope.isIOS = $ionicPlatform.is( 'ios' );

        })
        .controller('LandingCtrl', function ($scope, $state, $ionicPopup, $window, $ionicLoading) {

        })
        .controller('TabsCtrl', function($rootScope, $scope, Notifications, $window, $ionicTabsDelegate){
            $scope.profiledata = JSON.parse($window.localStorage['profile']);
            var userid = $scope.profiledata.userid;
            $rootScope.qntNotifications = 0;

            $rootScope.checkNotifications = function() {
                Notifications.getQntNotifications(userid).success(
                function(result) {
                    if(result.length > 0)
                        $rootScope.qntNotifications = result.length;

                }).error(function(error) {
                    console.log(error);
                });
            }

        })
        .controller('LoginCtrl', function ($scope, userauth,usersignupfb, $state, $ionicPopup, $window, $ionicLoading, updatepushid,$ionicNavBarDelegate,$ionicLoading,$rootScope,$ionicPlatform) {
            //$ionicNavBarDelegate.showBackButton(true)
            $ionicPlatform.ready(function() {
                $rootScope.analytics('Login');
            });


            $ionicLoading.hide();
            if ($window.localStorage['profile']) {
              var userdata = JSON.parse($window.localStorage['profile']);
                if (localStorage.getItem("playerid") && userdata.pushid.length < 1) {
                    var pushid = localStorage.getItem("playerid");

                    updatepushid.updatepushid(pushid, userdata.userid).success(function (successdata) {});
                }
              userauth.dologinSemMd5(userdata.email, userdata.password).success(function (successdata) {
                  $ionicLoading.hide();
                  //console.log("successdata",successdata);
                  if (successdata.message.trim().toString() === 'LoginSuccess') {
                      userdata = successdata.userdetails;
                      $window.localStorage['profile'] = JSON.stringify(userdata);
                  }
              });
                if(userdata.feed_view)
                {
                    $state.go('tab.feeds');
                }
                else
                {
                    $state.go('selecionar-estado');
                }
            }

            $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
                viewData.enableBack = true;
            });

            $scope.data = {
                'email': "",
                'password': "",
                'name': ""
            };

            $scope.facebookAuth = function()
            {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });

                facebookConnectPlugin.logout(function(){},function(){});

                var fbLoginSuccess = function (userData) {
                    console.log('login_success',userData);
                    var userId = userData['authResponse']['userID'];
                    OnGetFacebookInfo(userId);
                };

                facebookConnectPlugin.login(["email","public_profile"], fbLoginSuccess,
                    function loginError (error) {
                        console.log('get_face_data',error);
                        $ionicLoading.hide();
                    }
                );

                function OnGetFacebookInfo(fb_user_id)
                {
                   return facebookConnectPlugin.api(fb_user_id+"/?fields=id,name,email", ["public_profile"],
                        function (result) {
                            $ionicLoading.hide();
                            usersignupfb.signup(result['email'], fb_user_id, result['name']).success(function (successdata) {
                                if (successdata.message.trim().toString() === 'success' || successdata.message.trim().toString() === 'exists') {
                                    var userdata = successdata.userdetails;
                                    if (localStorage.getItem("playerid")) {
                                        var pushid = localStorage.getItem("playerid");

                                        updatepushid.updatepushid(pushid, userdata.userid).success(function (successdata) {});
                                    }
                                    if(userdata.flagged == 'yes')
                                    {
                                         $state.go('conta-reprovada');
                                    }
                                    else if (userdata.user_active != 'yes')
                                    {
                                         $state.go('aguardando-aprovacao');
                                    }
                                    else
                                    {
                                        $window.localStorage['profile'] = JSON.stringify(userdata);
                                        if(userdata.feed_view)
                                        {
                                            $state.go('tab.feeds');
                                        }
                                        else
                                        {
                                            $state.go('selecionar-estado');
                                        }
                                    }
                                } else {
                                    //console.log('NÃO PEGOU OS DADOS');
                                    var alertPopup = $ionicPopup.alert({
                                        title: 'Refriplay',
                                        okType: 'button-balanced',
                                        template: 'Houve algum erro no cadastro'
                                    });
                                    return false;
                                }
                            });
                        },
                        function (error){
                            console.log('connect_face_api',error);
                            $ionicLoading.hide();
                            return error;
                        });

                }
            };

            $scope.login = function () {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });

                userauth.dologin($scope.data.email, $scope.data.password).success(function (successdata) {
                    $ionicLoading.hide();
                    if (successdata.message.trim().toString() === 'LoginSuccess') {
                        var userdata = successdata.userdetails;
                        $window.localStorage['profile'] = JSON.stringify(userdata);
                        //alert($window.localStorage['profile']);
                        $scope.profiledata = JSON.parse($window.localStorage['profile']);
                        var userid = $scope.profiledata.userid;
                        //alert(localStorage.getItem("playerid"));
                        if (localStorage.getItem("playerid")) {
                            var pushid = localStorage.getItem("playerid");

                            updatepushid.updatepushid(pushid, userid).success(function (successdata) {
                                if(userdata.feed_view)
                                {
                                    $state.go('tab.feeds');
                                }
                                else
                                {
                                    $state.go('selecionar-estado');
                                }
                            });
                        } else {
                            if(userdata.feed_view)
                            {
                                $state.go('tab.feeds');
                            }
                            else
                            {
                                $state.go('selecionar-estado');
                            }
                        }

                    }
                    else if (successdata.message.trim().toString() == 'UserBan')
                    {
                        //var userdata = successdata.userdetails;
                        //$window.localStorage['profile'] = JSON.stringify(userdata);
                        $state.go('conta-reprovada');
                        return false;
                    }
                    else if (successdata.message.trim().toString() == 'InactiveUser')
                    {
                        //var userdata = successdata.userdetails;
                        //$window.localStorage['profile'] = JSON.stringify(userdata);
                        $state.go('aguardando-aprovacao');
                        return false;
                    }
                    else {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Refriplay',
                            okType: 'button-balanced',
                            template: 'Login ou Senha inválido'
                        });
                        return false;
                    }
                });
            };

        })
        .controller('SignupCtrl', function ($scope, $state, $ionicPopup, usersignup, $window, $ionicLoading,userpass,userChangePass,$rootScope,$ionicPlatform,updatepushid) {

            $ionicPlatform.ready(function() {
                $rootScope.analytics('Cadastro');
            });


            $ionicLoading.hide();
            $scope.data = {
                email: '',
                password: '',
                name: ''
            };

            $scope.signUp = function (form) {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });

                if (form.$valid) {

                    usersignup.signup($scope.data.email, $scope.data.password, $scope.data.name).success(function (successdata) {
                        $ionicLoading.hide();
                        if (successdata.message.trim().toString() === 'exists') {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Este email já existe, tente fazer login'
                            });
                            return false;
                        } else if (successdata.message.trim().toString() === 'success') {
                            var userdata = successdata.userdetails;
                            if (localStorage.getItem("playerid")) {
                                var pushid = localStorage.getItem("playerid");

                                updatepushid.updatepushid(pushid, userdata.userid).success(function (successdata) {});
                            }
                            if(userdata.flagged == 'yes')
                            {
                                $state.go('conta-reprovada');
                            }
                            else if (userdata.user_active == 'no')
                            {
                                $state.go('aguardando-aprovacao');
                            }
                            else
                            {
                                $window.localStorage['profile'] = JSON.stringify(userdata);
                                if(userdata.feed_view)
                                {
                                    $state.go('tab.feeds');
                                }
                                else
                                {
                                    $state.go('selecionar-estado');
                                }
                            }
                        } else {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Falha ao registrar'
                            });
                            return false;
                        }
                    });

                } else {
                    $ionicLoading.hide();
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Por favor preencha o formulário'
                    });
                }
            };

            $scope.forgotMyPass = function(form){
                if (form.$valid)
                {
                    $ionicLoading.show({
                        content: 'Loading',
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showDelay: 0
                    });
                    userpass.forgotPass($scope.data.email).success(function(data){
                        if(data.result == 1)
                        {
                            $state.go('senha-confirmacao');
                        }
                        else if(data.result == -1)
                        {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Email não existe',
                                okType: 'button-balanced',
                                template: 'Este email não está cadastrado!'
                            });
                        }
                        else
                        {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Erro ao enviar',
                                okType: 'button-balanced',
                                template: 'Tente novamente ou mande um email para suporte@refriplay.com.br'
                            });
                        }
                        $ionicLoading.hide();
                    });
                }
            };

            $scope.changePass = function(form)
            {
                if (form.$valid)
                {
                    $ionicLoading.show({
                        content: 'Loading',
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showDelay: 0
                    });
                    userChangePass.changePass($scope.data.code,$scope.data.password).success(function(data){
                        if(data.result == 1)
                        {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay Senha',
                                okType: 'button-balanced',
                                template: 'Senha alterada com sucesso!'
                            });
                            $state.go('login-intro');
                        }
                        else
                        {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay Senha',
                                okType: 'button-balanced',
                                template: 'A senha não foi alterada, seu código está inválido, tente gerar outro!'
                            });
                        }
                        $ionicLoading.hide();
                    });
                }
            };

        })

        .controller('FeedsCtrl', function ($scope, Comments, getuserfeeds, S3_URL, SERVER_URL, $ionicModal, addpostlike, addpostcomment, $window, $sce, $stateParams, $compile,$state,adv_report, $location, $rootScope, $timeout, $ionicSlideBoxDelegate,$ionicPlatform,deletepost,$ionicLoading,$ionicPopup,$document,setFeedState,Error,$ionicHistory,$ionicNavBarDelegate) {
            //$scope.$sce = $sce;
            var postid = 0;
            if($stateParams.postId)
            {
                postid = $stateParams.postId;
                $scope.UPost = postid;
                $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
                    viewData.enableBack = true;
                });
            }

            // $scope.$compile = $compile;
            $scope.profiledata = JSON.parse($window.localStorage['profile']);
            $scope.SERVER_URL = SERVER_URL;
            $scope.youtubeLink = '';

            $scope.trustSrc = function(youtubelink) {
                return $sce.trustAsResourceUrl(youtubelink);
              }

            $scope.$on('$ionicView.enter', function(e) {
                $ionicNavBarDelegate.showBar(true);
                    $rootScope.checkNotifications();
            });


            var userid = $scope.profiledata.userid;
            var username = $scope.profiledata.name;
            var profile_pic = $scope.profiledata.profile_pic;
            var page = 0;
            var hashtagid = $stateParams.hashtagid;
            $scope.feeds = [];
            $scope.noMoreItemsAvailable = false;
            $scope.advid = 0;

            //$scope.pofilepic_baseurl = SERVER_URL + "profileuploads/";
            $scope.photo_baseurl = SERVER_URL;

            $scope.currhashtag = $stateParams.hashtag;

            $scope.loadMore = function (type) {
                $rootScope.videoProgress = 0;
                if (type === 'iscroll')
                {
                    adv_report.count($scope.advid,userid, $scope.profiledata.feed_view).success(function (feedsdata) {});
                }

                if($scope.profiledata.feed_view == 'all'){

                    $scope.profiledata.feed_view = "Brasil";

                    getuserfeeds.getuserfeedsAll(userid, page).then(
                        function (feedsdata) {
                            var success = feedsdata.data.success;
                            $scope.advid = feedsdata.data.has_adv;

                            if (success === 1) {
                                if (type === 'iscroll') {
                                    $scope.feeds = $scope.feeds.concat(feedsdata.data.feeds);
                                } else {
                                    $scope.feeds = feedsdata.data.feeds;
                                }
                                page++;
                        }
                        else {
                            $scope.noMoreItemsAvailable = true;
                        }

                        $scope.$broadcast('scroll.infiniteScrollComplete');
                    });

                } else {

                    // $scope.profiledata.feed_view = "Publicações em "+$scope.profiledata.feed_view;
                    getuserfeeds.getuserfeeds(userid, page, hashtagid).then(function (feedsdata) {
                        var success = feedsdata.data.success;
                        $scope.advid = feedsdata.data.has_adv;
                        if (success === 1) {
                            if (type === 'iscroll') {
                                $scope.feeds = $scope.feeds.concat(feedsdata.data.feeds);
                            } else {
                                $scope.feeds = feedsdata.data.feeds;
                            }
                            page++;
                        }
                        else {
                            $scope.noMoreItemsAvailable = true;
                        }
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                    });
                }

            };

            $rootScope.videoProgress = 0;

            $scope.profileAdmin = 0;
            $scope.lastDeleteEleClick = -1;
            $document.on('click', function (event) {
                var isMoreOption = $(event.target).hasClass('ion-android-more-vertical');
                if($scope.lastDeleteEleClick >= 0 && $scope.feeds[$scope.lastDeleteEleClick].showExclude && !isMoreOption)
                {
                    $scope.feeds[$scope.lastDeleteEleClick].showExclude = false;
                    $scope.lastDeleteEleClick = -1;
                    $scope.$apply();
                }
            });

            $scope.finalScript = false;
            if($window.localStorage['profile'] === undefined)
            {
                $state.go('login-intro');
            }

            $scope.profiledata = JSON.parse($window.localStorage['profile']);
            $ionicPlatform.ready(function() {
                $rootScope.analytics('Publicações em ' + $scope.profiledata.feed_view);
            });


            if($scope.profiledata.flagged == 'yes')
            {
                $state.go('conta-reprovada');
            }
            else if ($scope.profiledata.user_active != 'yes')
            {
                $state.go('aguardando-aprovacao');
            }

            $scope.showAddComment = false;
            $scope.showAddCommentFunction = function(){
                $scope.showAddComment = true;
            };

            $scope.TrustDangerousSnippet = function (p) {
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
                // var _link = $compile(p)($scope);

                //return $sce.trustAsHtml(_link[0].outerHTML);
                return $sce.trustAsHtml(p);

            };

            $scope.doRefresh = function () {
                $scope.feeds = [];
                page = 0;
                $scope.noMoreItemsAvailable = false;
                $scope.loadMore('refresh');
                $scope.$broadcast('scroll.refreshComplete');

            };
            $scope.galleryOpened = 0;
            $scope.openGallery = function(){
                $scope.galleryOpened = 1;
            }

            $scope.likeFeed = function (postid, feedindex) {
                $scope.feeds[feedindex].selflike = !$scope.feeds[feedindex].selflike;
                if($scope.feeds[feedindex].selflike)
                {
                    $scope.feeds[feedindex].post_likescount = parseInt($scope.feeds[feedindex].post_likescount)+parseInt(1);
                }
                else
                {
                    $scope.feeds[feedindex].post_likescount = parseInt($scope.feeds[feedindex].post_likescount)-parseInt(1);
                }
                addpostlike.addpostlike(userid, postid).success(function (likedata) {
                    var new_count = likedata.post_likescount;
                    /*if(new_count != $scope.feeds[feedindex].post_likescount)
                    {
                        $scope.feeds[feedindex].post_likescount = new_count;
                    }*/
                });
            }

            $scope.likeFeedPost = function (feed) {
                if(!feed.selflike)
                {
                    feed.post_likescount = parseInt(feed.post_likescount)+parseInt(1);
                    feed.selflike = true;
                }
                else
                {
                    feed.post_likescount = parseInt(feed.post_likescount)-parseInt(1);
                    feed.selflike = false;
                }
                addpostlike.addpostlike(userid, feed.post_id).success(function (likedata) {
                    var new_count = likedata.post_likescount;
                    /*if(new_count != $scope.feeds[feedindex].post_likescount)
                    {
                        $scope.feeds[feedindex].post_likescount = new_count;
                    }*/
                });
            }

            Error.getErrorAll().then(function (result) {
                $rootScope.errorList = result.data;
            });

            $scope.showcomments = function (feedid, feedindex,create) {
                $scope.cur_feedid = feedid;
                $scope.feedindex = feedindex;
                $scope.commentsArray = [];

                /*if(!create) {
                    $ionicLoading.show({
                        content: 'Loading',
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showDelay: 0
                    });
                }*/

                Comments.getCommentsByPost(feedid).success(function(result){
                    $scope.commentsArray = result;
                    if(!create)
                    $ionicLoading.hide();
                });

                /*if(create == true)
                {
                    $scope.showAddComment = true;
                    //$window.document.getElementById('age');
                }
                else
                {
                    $scope.showAddComment = false;
                }*/

                $scope.modal.show();
            }


            $scope.setFeedState = function(state){

                setFeedState.setFeedState(state.short_name,$scope.profiledata.userid).success(function (result) {
                    if(result.result == 1)
                    {
                        $scope.profiledata.feed_view = state.short_name;
                        $window.localStorage['profile'] = JSON.stringify($scope.profiledata);
                    }
                    $state.go('tab.feeds',{},{reload:true});
                });
            }

            // For comments modal
            $ionicModal.fromTemplateUrl('templates/comments.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.iframeHeight = $window.innerHeight-100;
                $scope.modal = modal;
            });
            $scope.openModal = function () {
                $scope.modal.show();
            }
            $scope.closeModal = function () {
                $scope.modal.hide();
            }
            // For comments modal ends
            $scope.commentFocus = function(){
                $timeout(function() {
                    $('#comment').focus();
                },750);
            }

            $scope.comment_text = '';
            $scope.addComment = function ( txt ) {
                $scope.comment_text = txt;
                if (!$scope.comment_text) {
                    return false;
                }
                if($scope.UPost)
                {
                    addpostcomment.addpostcomment(userid, $scope.UPost, $scope.comment_text).success(function (postcommentdata) {
                        if (postcommentdata.message.trim().toString() === 'success') {

                            var curr_comments = {"comment_text": txt, "user_id": $scope.profiledata.userid, "user_name": $scope.profiledata.name, "profile_pic": $scope.profiledata.profile_pic,"comment_date":"Agora mesmo"};
                            $scope.feed.comments.push(curr_comments);
                            var new_count = postcommentdata.post_commentscount;

                        }
                    });
                }
                else
                {

                    var postid = $scope.cur_feedid;
                    addpostcomment.addpostcomment(userid, postid, $scope.comment_text).success(function (postcommentdata) {
                        if (postcommentdata.message.trim().toString() === 'success') {

                            var curr_comments = {"comment_text": $scope.comment_text, "user_id": userid, "user_name": username, "profile_pic": $scope.profiledata.profile_pic,"comment_date":"Agora mesmo"};
                            $scope.commentsArray.push(curr_comments);
                            var new_count = postcommentdata.post_commentscount;
                            $scope.feeds[$scope.feedindex].post_commentscount = new_count;

                        }
                    });
                }

            }

            $scope.loadPost = function( postId )
            {
                getuserfeeds
                    .getpostdetails( postId, userid )
                    .then( function ( resp ) {
                        $scope.feed = resp.data.feeds[0];
                        $timeout(function(){
                            $ionicSlideBoxDelegate.update();
                        }, 1);
                    });
            };

            if ( $stateParams.postId )
                $scope.loadPost( $stateParams.postId );

            $scope.showDelete = function(feed,index)
            {
                if($scope.lastDeleteEleClick >= 0 && $scope.feeds[$scope.lastDeleteEleClick].showExclude)
                {
                    $scope.feeds[$scope.lastDeleteEleClick].showExclude = false;
                }
                if(feed.showExclude)
                {
                    feed.showExclude = false;
                }
                else
                {
                    feed.showExclude = true;
                }
                $scope.lastDeleteEleClick = index;
            };

            $scope.deletePost = function(feed,index)
            {

                var confirmPopup = $ionicPopup.confirm({
                    title: 'Refriplay',
                    template: 'Você deseja excluir esta publicação?',
                    okType: 'button-balanced',
                    cancelType: 'button-balanced',
                    cancelText: 'Não',
                    okText: 'Sim'
                });

                confirmPopup.then(function (res) {
                    $ionicLoading.show({
                        content: 'Loading',
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showDelay: 0
                    });

                    if (res) {

                        deletepost.deletepost(feed.post_id).success(function (result) {
                            if(result.success == 1)
                            {
                                $scope.feeds.splice( index, 1 );
                            }
                            else
                            {
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Refriplay',
                                    okType: 'button-balanced',
                                    template: 'Não foi possível deletar esta publicação'
                                });
                            }
                        });
                    }
                    $ionicLoading.hide();
                });

            };

            $('video').bind('webkitfullscreenchange mozfullscreenchange fullscreenchange',function(e){
                var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
                var isFull = state ? 1 : 0;
                var orientation = window.screen.orientation;
                if(isFull == 1)
                {
                    orientation.lock('landscape');
                }
                else
                {
                    orientation.lock('portrait');
                }
            });

        })

        .controller('NewpostCtrl', function ($scope, $ionicModal, checkinplaces, addnewpost, $window, $cordovaFileTransfer, SERVER_URL, $state, $ionicPopup,$cordovaCamera,$ionicLoading,$rootScope,$ionicPlatform) {

            $scope.members = $rootScope.errorList;
            $scope.message =[];
            $scope.data = {
                'selplace': "Select",
                'post_text': "",
                'userid': ""
            };

            $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
                viewData.enableBack = true;
            });

            /*navigator.geolocation.getCurrentPosition(function (position) {

                var curr_lat = position.coords.latitude;
                var curr_lng = position.coords.longitude;
                checkinplaces.getnearbyplaces(curr_lat, curr_lng).success(function (placesdata) {
                    $scope.placetypes = placesdata.results;
                });

            });*/

            // For checkin modal
            $ionicModal.fromTemplateUrl('templates/checkin.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modal = modal;
            });
            $scope.openModal = function () {
                $scope.modal.show();
            }
            $scope.closeModal = function () {
                $scope.modal.hide();
            }
            // For checkin modal ends

            // To Add New Post
            var post_text = $scope.data.post_text;

            var location = $scope.data.selplace;
            $scope.profiledata = JSON.parse($window.localStorage['profile']);

            $ionicPlatform.ready(function() {
                $rootScope.analytics('Nova publicação em' + $scope.profiledata.feed_view);
            });


            var userid = $scope.profiledata.userid;
            var feed_view = $scope.profiledata.feed_view;
            $scope.addPost = function () {

                post_text = $scope.data.post_text;
                location = $scope.data.selplace;

                if(post_text == '' && ($scope.imagesuri.length == 0 && !$scope.video.name))
                {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Por favor escreva algo ,publique algumas fotos ou faça um vídeo!'
                    });
                    return false;
                }



                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });

                var flagged = 0;
                if($scope.video.name)
                {
                    flagged = 1;
                }

                addnewpost.addnewpost(post_text, location, userid,feed_view,flagged).success(function (newpostdata) {
                    var postid = newpostdata.postid;

                    // File upload to server
                    var successcount = 0;

                    if ($scope.imagesuri.length === 0) {
                        if($scope.video.name)
                        {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Publicado com sucesso! Assim que seu vídeo terminar de carregar em nossos servidores você será notificado, até lá sua publicação não pode ser vista por nenhum usuário.'
                            });
                        }
                        else
                        {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Publicado com sucesso'
                            });
                        }

                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        $ionicLoading.hide();
                        $state.go('tab.feeds');
                    }

                    if($scope.video.name)
                    {
                        var uri = $scope.video.fullPath;
                        var options = new FileUploadOptions();

                        options.fileName = $scope.video.name;
                        options.chunkedMode = false;
                        options.headers = {
                            Connection: "close"
                        };

                        var params = new Object();
                        params.userid = userid;
                        params.postid = postid;
                        params.type = 'video';
                        options.params = params;
                        $rootScope.videoProgress = 0;
                        console.log('params',params);
                        $cordovaFileTransfer.upload(SERVER_URL + "services/upload.php", uri, options, true).then(function (result) {
                            /*if (!$scope.$$phase) {
                                $scope.$apply();
                            }
                            $ionicLoading.hide();
                            $state.go('tab.feeds');*/
                            console.log('foi',result);
                        }, function (err) {
                            console.log('erro',err);
                            $ionicLoading.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Falha ao publicar'
                            });
                            return false;
                        }, function (progress) {
                            $rootScope.videoProgress = Math.ceil((progress.loaded/progress.total)*100);
                        });
                    }

                    for (var i = 0; i < $scope.imagesuri.length; i++) {
                        var uri = $scope.imagesuri[i];
                        var options = new FileUploadOptions();

                        options.fileName = randomString(20, "A") + ".jpg";
                        options.chunkedMode = false;
                        options.headers = {
                            Connection: "close"
                        };

                        var params = new Object();
                        params.userid = userid;
                        params.postid = postid;
                        options.params = params;

                        $cordovaFileTransfer.upload(SERVER_URL + "services/upload.php", uri, options, true).then(function (result) {
                            //alert("SUCCESS: " + JSON.stringify(result.response));
                            console.log('fez o upload');
                            console.log(result);

                            successcount = successcount + 1;

                            if (successcount === $scope.imagesuri.length) {
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Refriplay',
                                    okType: 'button-balanced',
                                    template: 'Publicado com sucesso'
                                });

                                $scope.images = [];
                                $scope.imagesuri = [];

                                if (!$scope.$$phase) {
                                    $scope.$apply();
                                }
                                $ionicLoading.hide();
                                $state.go('tab.feeds');
                            }
                        }, function (err) {
                            alert("ERROR: " + JSON.stringify(err));
                            $ionicLoading.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Falha ao publicar'
                            });
                            return false;
                        }, function (progress) {
                            // constant progress updates
                        });
                        // upload code ends here

                    } // end of for loop

                    // File upload to server end


                }); // end of function add new post ajax call





            }
            // To add new post ends

            // to open gallery
            $scope.photoType = '';
            $scope.images = [];
            $scope.imagesuri = [];
            $scope.video = [];
            $scope.opengallery = function () {
                //$scope.images = [];
                //$scope.imagesuri = [];
                if($scope.imagesuri.length >= 3)
                {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Você só pode anexar 3 imagens por publicação!'
                    });
                    return false;
                }

                var permissions = cordova.plugins.permissions;
                permissions.checkPermission(permissions.WRITE_EXTERNAL_STORAGE, function( status ){
                    if ( status.hasPermission ) {
                        $scope.imageGetter();
                    }
                    else
                    {
                        permissions.requestPermission(permissions.WRITE_EXTERNAL_STORAGE, function( status ){
                            if ( status.hasPermission ) {
                                $scope.imageGetter();
                            }
                        });

                    }
                });

            };
            // open gallery ends
            $scope.imageGetter = function(){

                var options = {
                    quality: 50,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                    mediaType: Camera.MediaType.ALLMEDIA,
                    limit:3,
                    correctOrientation: true,
                    allowEdit: true
                };


                /*if($scope.photoType == 'gallery')
                {
                    options.mediaType = Camera.MediaType.PICTURE;
                }
                else
                {
                    options.mediaType = Camera.MediaType.ALLMEDIA;
                }*/



                $cordovaCamera.getPicture(options).then(function(uri){
                    var imagesType = ["png","PNG","jpg","JPG","jpeg","JPEG","gif","GIF"];

                    if(imagesType.indexOf(uri.substring(uri.length-3, uri.length)) >= 0)
                    {
                        $scope.photoType = 'gallery';
                        $scope.imagesuri.push(uri);
                    }
                    else
                    {
                        $scope.imagesuri = [];
                        $scope.photoType = 'video';
                        $scope.video = {};
                        $scope.video.fullPath = uri;

                        var lastDot   = uri.lastIndexOf( "." ),
                            extension = uri.substr( lastDot, uri.length - lastDot + 1 );

                        $scope.video.name = 'video_refriplay' + extension.toLowerCase();
                    }


                })
            };

            //camera begin
           $scope.takeAPicture = function(){
               $scope.images = [];
               $scope.imagesuri = [];
               var permissions = cordova.plugins.permissions;
               permissions.checkPermission(permissions.WRITE_EXTERNAL_STORAGE, function( status ){
                   if ( status.hasPermission ) {
                       $scope.openCam();
                   }
                   else
                   {
                       permissions.requestPermission(permissions.WRITE_EXTERNAL_STORAGE, function( status ){
                           if ( status.hasPermission ) {
                               $scope.openCam();
                           }
                       });

                   }
               });

           };

           $scope.openCam = function(){
               var options = {
                   destinationType: Camera.DestinationType.FILE_URI,
                   sourceType: Camera.PictureSourceType.CAMERA,
                   correctOrientation: true,
                   allowEdit: true
               };
               $cordovaCamera.getPicture(options).then(function(imageURI) {
                   var srcobj = {src: imageURI};
                   $scope.images.push(srcobj);
                   $scope.imagesuri.push(imageURI);
                   if (!$scope.$$phase) {
                       $scope.$apply();
                   }
                   $scope.photoType = 'cam';
                   if (!$scope.$$phase) {
                       $scope.$apply();
                   }
               }, function(err) {
                   console.log('error: '+err);
               });
           }

            $scope.openVideoCam = function(){
                    window.plugins.videocaptureplus.captureVideo(
                    function(data){
                        console.log(data);
                        $scope.video = data[0];
                        $scope.photoType = 'video';
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }

                    },
                    function(err){
                        var alertPopup = $ionicPopup.alert({
                            title: 'Refriplay',
                            okType: 'button-balanced',
                            template: 'Falha ao gravar o vídeo'
                        });
                    },
                    {
                        //duration:60,
                        highquality: true,
                    }
                );

            };

            //camera end

            // Function to generate random string
            function randomString(len, an) {
                an = an && an.toLowerCase();
                var str = "", i = 0, min = an == "a" ? 10 : 0, max = an == "n" ? 10 : 62;
                for (; i++ < len; ) {
                    var r = Math.random() * (max - min) + min << 0;
                    str += String.fromCharCode(r += r > 9 ? r < 36 ? 55 : 61 : 48);
                }
                return str;
            }
            // function to generate random string ends here
        })

        .controller('ProfileCtrl', function (S3_URL, $scope, SERVER_URL, getfeedprofile, $window, getselffeed, addpostcomment, addpostlike, $ionicModal, $state, deletepost, $ionicPopup,$ionicPlatform) {
            $scope.showgallery = true;
            //$scope.pofilepic_baseurl = S3_URL + "/profile_imgs/";
            //$scope.photo_baseurl = S3_URL + "/posts/";
            $scope.photo_baseurl = SERVER_URL;
            $scope.profiledata = JSON.parse($window.localStorage['profile']);

            var userid = $scope.profiledata.userid;
            var username = $scope.profiledata.name;
            var profile_pic = $scope.profiledata.profile_pic;
            $scope.items = [];
            getfeedprofile.getfeedprofile(userid).success(function (profiledata) {
                //  console.log(profiledata);
                $scope.feedprofile = profiledata;
                $scope.items = profiledata.photos;
                //console.log("items===" + $scope.items);
            });

            var page = 0;
            $scope.feeds = [];

            $scope.loadMore = function (type) {
                getselffeed.getselffeed(userid, page).success(function (feedsdata) {
                    var success = feedsdata.success;
                    if (success === 1) {
                        if (type === 'iscroll') {
                            $scope.feeds = $scope.feeds.concat(feedsdata.feeds);
                        } else {
                            $scope.feeds = feedsdata.feeds;
                        }
                        page++;
                        $scope.pofilepic_baseurl = SERVER_URL + "/profile_imgs/"
                        //$scope.photo_baseurl = S3_URL + "/posts/";
                        $scope.photo_baseurl = SERVER_URL;
                    }
                    else {
                        $scope.noMoreItemsAvailable = true;
                    }
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                });
            };

            $scope.likeFeed = function (postid, feedindex) {
                addpostlike.addpostlike(userid, postid).success(function (likedata) {
                    var new_count = likedata.post_likescount;
                    $scope.feeds[feedindex].post_likescount = new_count;
                    $scope.feeds[feedindex].selflike = !$scope.feeds[feedindex].selflike;
                });
            }

            $scope.showcomments = function (commentsArray, feedid, feedindex) {
                $scope.cur_feedid = feedid;
                $scope.feedindex = feedindex;
                $scope.commentsArray = commentsArray;
                $scope.oModal1.show();
            }

            $scope.showPhotos = function () {

                // $scope.oModal2.show();
                //console.log($scope.showgallery);
                $scope.showgallery = !$scope.showgallery;
            }

            // For comments modal
            $ionicModal.fromTemplateUrl('templates/comments.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.oModal1 = modal;
            });
            $scope.openModal = function (index) {

                $scope.oModal1.show();
            }
            $scope.closeModal = function (index) {
                $scope.oModal1.hide();
            }
            // For comments modal ends



            $scope.comment_text = '';
            $scope.addComment = function (comment_text) {

                if (!comment_text) {
                    return false;
                }
                var postid = $scope.cur_feedid;
                addpostcomment.addpostcomment(userid, postid, comment_text).success(function (postcommentdata) {
                    if (postcommentdata.message.trim().toString() === 'success') {

                        var curr_comments = {"comment_text": comment_text, "user_id": userid, "user_name": username, "profile_pic": profile_pic};
                        $scope.commentsArray.push(curr_comments);
                        var new_count = postcommentdata.post_commentscount;
                        $scope.feeds[$scope.feedindex].post_commentscount = new_count;

                    }
                });
            }

            $scope.deletePost = function (postid) {

                var confirmPopup = $ionicPopup.confirm({
                    title: 'Refriplay',
                    template: 'Are you sure you want to Delete?',
                    okType: 'button-balanced',
                    cancelType: 'button-balanced',
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        // yes
                        deletepost.deletepost(postid).success(function (postcommentdata) {
                            if (postcommentdata.message.trim().toString() === 'success') {
                                page = 0;
                                $scope.feeds = [];
                                $scope.loadMore('iscroll');

                                if (!$scope.$$phase) {
                                    $scope.$apply();
                                }

                            }
                        });
                        // yes end
                    } else {

                    }
                });

            }
        })

        .controller('FriendsCtrl', function ($scope, getfriends, SERVER_URL, S3_URL, $window, addfollowing, $ionicLoading, unfollow, $ionicPopup) {



            $scope.profiledata = JSON.parse($window.localStorage['profile']);
            var userid = $scope.profiledata.userid;

            $scope.pofilepic_baseurl = S3_URL + "profile_imgs/";
            $scope.btnitems = [
                {id: "followers", name: "Followers", changed: false},
                {id: "following", name: "Following", changed: true},
                {id: "suggestions", name: "Others", changed: true}

            ];

            $scope.changeFriends = function (id) {
                for (var i = 0; i < 3; i++) {
                    var curr = $scope.btnitems[i];
                    if (curr.id !== id) {
                        curr.changed = true;
                    }
                }
                $scope.getFriends(userid, id);
            };

            $scope.getFriends = function (userid, type) {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                getfriends.getfriends(userid, type).success(function (friendsdata) {
                    $scope.friends = friendsdata.users;
                    $ionicLoading.hide();
                });
            }

            // By default call followers
            $scope.getFriends(userid, "followers");

            //addfollowing
            $scope.addfollowing = function (followingid) {
                addfollowing.addfollowing(userid, followingid).success(function (followingdata) {

                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }

                    $scope.getFriends(userid, "suggestions");

                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Successfully Following!'
                    });
                });
            }

            //unfollow user
            $scope.unfollow = function (followingid) {
                unfollow.unfollow(userid, followingid).success(function (unfollowdata) {

                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }

                    $scope.getFriends(userid, "following");

                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Unfollowed User!'
                    });
                });
            }


        })



        .controller('PhotoGalleryCtrl', function ($scope, getuserphotos, $window, $stateParams) {
            // console.log("Hey");
            var fromprofile = $stateParams.fromprofile;
            var userid = $stateParams.userid;

            // console.log("userid------ "+userid);
            if (fromprofile === "yes") {
                $scope.showpback = true;
            } else {
                $scope.showpback = false;
            }
            //$scope.profiledata = JSON.parse($window.localStorage['profile']);
            // var userid = $scope.profiledata.userid;

            $scope.items = [];
            getuserphotos.getuserphotos(userid).success(function (photodata) {
                //  alert('hey');
                if (photodata.message.trim().toString() === 'success') {
                    $scope.items = photodata.photos;
                }
            });


        })

        .controller('ProfilePageCtrl', function ($scope, S3_URL, $window, $stateParams, getotheruserprofile, SERVER_URL, $state, addfollowing, unfollow, $ionicPopup) {
            var userid = $stateParams.userid;
            var currtab = $stateParams.currpage;

            $scope.profiledata = JSON.parse($window.localStorage['profile']);
            var loggedinuserid = $scope.profiledata.userid;

            if (parseInt(loggedinuserid) === parseInt(userid)) {
                $state.go('tab.profile');
            } else {
                $scope.pofilepic_baseurl = S3_URL + "profile_imgs/"
                $scope.photo_baseurl = S3_URL + "posts/";
                $scope.profiledata = JSON.parse($window.localStorage['profile']);

                $scope.items = [];
                getotheruserprofile.getotheruserprofile(userid, loggedinuserid).success(function (profiledata) {
                    //  console.log(profiledata);
                    $scope.feedprofile = profiledata;
                    $scope.items = profiledata.photos;
                    //console.log("items===" + $scope.items);
                });

                $scope.goBackFromProfile = function () {
                    if (currtab === 'feeds') {
                        $state.go('tab.feeds');
                    } else if (currtab === 'friends') {
                        $state.go('tab.friends');
                    }
                };
            }
            //addfollowing
            $scope.addfollowing = function () {
                addfollowing.addfollowing(loggedinuserid, userid).success(function (followingdata) {
                    $scope.feedprofile.showfollowbutton = false;
                    $scope.feedprofile.showunfollowbutton = true;

                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }


                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Successfully Following!'
                    });
                });
            }

            //unfollow user
            $scope.unfollow = function () {
                unfollow.unfollow(loggedinuserid, userid).success(function (unfollowdata) {
                    $scope.feedprofile.showunfollowbutton = false;
                    $scope.feedprofile.showfollowbutton = true;

                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Unfollowed User!'
                    });
                });
            }

        })

        .controller('NotificationsCtrl', function ($scope, $rootScope, S3_URL, $location, Notifications, $window, $ionicLoading, $ionicPopup,SERVER_URL) {
            $scope.notifications = [];
            $scope.noMoreItemsAvailable = false;
            $scope.profiledata = JSON.parse($window.localStorage['profile']);
            $scope.length = 1;
            $rootScope.qntNotifications = 0;
            var userid = $scope.profiledata.userid;
            $scope.pofilepic_baseurl = SERVER_URL + "profileuploads/";

            var page = 0;

            Notifications.readAll(userid);

            $scope.loadMore = function() {
                Notifications.getUserNotifications(userid, page).success(
                    function(result) {
                        if(result.success == 0) {
                            $scope.noMoreItemsAvailable = true;
                            $scope.length = 0;
                        } else {
                            if(result.length < 10) {
                                $scope.noMoreItemsAvailable = true;
                            } else {
                                $scope.noMoreItemsAvailable = false;
                            }
                            page++;
                            $scope.length = result.length;
                            $scope.notifications = $scope.notifications.concat(result.notifications);
                            console.log($scope.notifications);
                        }
                    }).error(function(error) {
                        console.log(error);
                    }).finally(function(){
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                    });
            }


            $scope.checkNotification  = function(notification) {
                if(notification.is_read == 0) {
                    Notifications.changeNotificationStatus(notification.id);
                }
                $location.path('post/' + notification.post_id);
            }

            $scope.doRefresh = function() {
                Notifications.getUserNotifications(userid, page)
                .success(
                    function(result) {
                        $scope.notifications = result.notifications;
                })
                .error(function(error) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Falha ao capturar suas notificações'
                    });
                    return false;
                })
                .finally(function(){
                    $scope.$broadcast('scroll.refreshComplete');
                });
            }

        })

        .controller('AccountCtrl', function ($scope, S3_URL, SERVER_URL, $ionicLoading, updateprofile, $window, $ionicPopup, $state, $cordovaFileTransfer, $ionicHistory,md5,$rootScope,$ionicPlatform,$stateParams,$filter) {
            $scope.profiledata = JSON.parse($window.localStorage['profile']);

            $ionicPlatform.ready(function() {
                $rootScope.analytics('Minha Conta');
            });

            $scope.$on('$ionicView.enter', function(e) {
                $rootScope.checkNotifications();
            });


            if($stateParams.field)
            {
                updateprofile.getFieldInfo($stateParams.field).success(function (result) {
                    if(result.success == 1)
                    {
                        $scope.editInfo = result.data;
                        $scope.editInfo.inputName = $stateParams.field;
                        if($stateParams.field == 'date_birthday')
                        {
                            $scope.profiledata[$scope.editInfo.inputName] = $filter('date')($scope.profiledata[$scope.editInfo.inputName], 'dd/MM/yyyy');
                        }

                        if($stateParams.field != 'password')
                        {
                            $scope.info = $scope.profiledata[$scope.editInfo.inputName];
                        }

                    }
                    else
                    {
                        $state.go('tab.account');
                    }
                });

                $scope.editInfoProfile = function(form)
                {
                    var inputMoney = document.getElementsByClassName('moneyInput');
                    angular.element(inputMoney).triggerHandler('blur');

                    if (form.$valid)
                    {
                        $ionicLoading.show({
                            content: 'Loading',
                            animation: 'fade-in',
                            showBackdrop: true,
                            maxWidth: 200,
                            showDelay: 0
                        });

                        if($stateParams.field == 'password')
                        {
                            if(form.info.$viewValue != undefined && $scope.profiledata.password !== md5.createHash(form.info.$viewValue))
                            {
                                form.info.$error.incorrectPass = true;
                                $ionicLoading.hide();
                                return false;
                            }
                            else
                            {
                                form.info.$viewValue = md5.createHash(form.info.$viewValue);
                            }
                        }

                        if($stateParams.field == 'date_birthday')
                        {
                            var allowed_date = new Date(2005,12,31);
                            var day = form.info.$viewValue.substring(0,2);
                            var month = form.info.$viewValue.substring(3,5);
                            var year = form.info.$viewValue.substring(6,10);
                            var date = new Date(year,month,day);

                            if(date > allowed_date)
                            {
                                form.info.$error.invalidDate = true;
                                $ionicLoading.hide();
                                return false;
                            }
                            else
                            {
                                form.info.$viewValue = year+'-'+month+'-'+day;
                            }
                        }


                        updateprofile.updateprofileinfo($stateParams.field,form.info.$viewValue,$scope.profiledata.userid).success(function (result) {
                            if(result.success == 1)
                            {
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Refriplay',
                                    okType: 'button-balanced',
                                    template: 'Atualizado com sucesso'
                                });
                                $window.localStorage['profile'] = JSON.stringify(result.userdetails);
                                $ionicLoading.hide();
                                $state.go('tab.account');
                            }
                            else
                            {
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Refriplay',
                                    okType: 'button-balanced',
                                    template: 'Houve um erro ao tentar atualizar a sua informação :('
                                });
                                $ionicLoading.hide();
                            }
                        });
                    }
                }
                return false;
            }


            $scope.setHelper = function(value)
            {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                if(value == undefined)
                {
                    value = false;
                }
                updateprofile.updateprofileinfo('isHelper',value,$scope.profiledata.userid).success(function (result) {
                    $window.localStorage['profile'] = JSON.stringify(result.userdetails);
                    if(value == true)
                    {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Muito bem!',
                            okType: 'button-balanced',
                            template: 'Agora você será informado de todas as publicações do seu estado atual.'
                        });
                    }
                    else
                    {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Refriplay',
                            okType: 'button-balanced',
                            template: 'Agora você só será notificado em publicações que interagiu.'
                        });
                    }
                    $ionicLoading.hide();
                });
            }

            var userid = $scope.profiledata.userid;
            var username = $scope.profiledata.name;
            var password = $scope.profiledata.password;
            var gender = $scope.profiledata.gender;
            var feed_view = $scope.profiledata.feed_view;
            var cpf = $scope.profiledata.CPF;
            //  $scope.pofilepic_baseurl = SERVER_URL + "profileuploads/";



            //$scope.selprofilepic = SERVER_URL + "profileuploads/" + profile_pic;

            if($scope.profiledata.profile_pic)
            {
                $scope.selprofilepic = SERVER_URL + "profileuploads/" + $scope.profiledata.profile_pic;
            }
            else
            {
                $scope.selprofilepic = 'img/emptyavatar.jpg';
            }

            if($scope.profiledata.logo)
            {
                $scope.logo = SERVER_URL + "profileuploads/" + $scope.profiledata.logo;
            }
            else
            {
                $scope.logo = 'img/emptyavatar.jpg';
            }

            $scope.updateprofileflag = false;
            $scope.updateProfilePicture = function (type) {
                var permissions = cordova.plugins.permissions;

                permissions.checkPermission(permissions.WRITE_EXTERNAL_STORAGE, function( status ){
                    if ( status.hasPermission ) {
                        if(type=='logo')
                        {
                            $scope.getPickLogo();
                        }
                        else
                        {
                            $scope.getPick();
                        }
                    }
                    else
                    {
                        permissions.requestPermission(permissions.WRITE_EXTERNAL_STORAGE, function( status ){
                            if ( status.hasPermission ) {
                                if(type=='logo')
                                {
                                    $scope.getPickLogo();
                                }
                                else
                                {
                                    $scope.getPick();
                                }
                            }
                        });

                    }
                });

            };

            $scope.getPick = function(){
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                window.imagePicker.getPictures(
                    function (results) {
                        if(results.length >= 1){
                            $scope.updateprofileflag = true;
                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }

                            var options = {
                                fileName: userid + ".jpg",
                                chunkedMode: false
                            };
                            var params = new Object();
                            params.userid = userid;

                            options.params = params;

                            $cordovaFileTransfer.upload(SERVER_URL + "services/uploadProfilePicture.php", results[0], options, true).then(function (result) {
                                $scope.updateprofileflag = false;


                                //alert("SUCCESS: " + JSON.stringify(result.response));
                                if (!$scope.$$phase) {
                                    $scope.$apply();
                                }
                                $scope.selprofilepic = SERVERURL + "profileuploads/" +result.response;
                                $scope.selprofilepic = $scope.selprofilepic.replace(/\s/g, "");
                                $scope.profiledata.profile_pic = result.response;
                                var user_info = JSON.parse($window.localStorage['profile']);
                                user_info.profile_pic = result.response.replace('/','');
                                /****GORDINHO******/
                                //console.log($scope.selprofilepic);
                                //console.log($scope.profiledata.profile_pic);
                                $window.localStorage['profile'] = JSON.stringify(user_info);
                                $ionicLoading.hide();

                            }, function (err) {
                                //alert("ERROR: " + JSON.stringify(err));
                                $ionicLoading.hide();
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Refriplay',
                                    okType: 'button-balanced',
                                    template: 'Houve um erro :('
                                });
                                return false;
                            }, function (progress) {
                                // constant progress updates
                            });

                        }else{
                            $ionicLoading.hide();

                        }

                    }, function (error) {
                        //alert('Error: ' + error);
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Refriplay',
                            okType: 'button-balanced',
                            template: 'Houve algum erro, tente novamente'
                        });
                    }, {
                        maximumImagesCount: 1,
                        width: 800,
                        height: 600
                    }
                );
            };

            $scope.getPickLogo = function(){
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                window.imagePicker.getPictures(
                    function (results) {
                        if(results.length >= 1){
                            $scope.updateprofileflag = true;
                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }

                            var options = {
                                fileName: userid + ".jpg",
                                chunkedMode: false
                            };
                            var params = new Object();
                            params.userid = userid;

                            options.params = params;

                            $cordovaFileTransfer.upload(SERVER_URL + "services/uploadLogoPicture.php", results[0], options, true).then(function (result) {
                                $scope.updateprofileflag = false;


                                //alert("SUCCESS: " + JSON.stringify(result.response));


                                $scope.profiledata.logo = result.response;

                                var user_info = JSON.parse($window.localStorage['profile']);
                                user_info.logo = result.response.replace('/','');
                                $scope.logo = SERVER_URL + "profileuploads/" + user_info.logo;
                                $window.localStorage['profile'] = JSON.stringify(user_info);
                                if (!$scope.$$phase) {
                                    $scope.$apply();
                                }
                                $ionicLoading.hide();


                            }, function (err) {
                                //alert("ERROR: " + JSON.stringify(err));
                                $ionicLoading.hide();
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Refriplay',
                                    okType: 'button-balanced',
                                    template: 'Houve um erro :('
                                });
                                return false;
                            }, function (progress) {
                                // constant progress updates
                            });

                        }else{
                            $ionicLoading.hide();

                        }

                    }, function (error) {
                        //alert('Error: ' + error);
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Refriplay',
                            okType: 'button-balanced',
                            template: 'Houve algum erro, tente novamente'
                        });
                    }, {
                        maximumImagesCount: 1
                    }
                );
            };

            $scope.editInfo = function(param)
            {
                $state.go('tab.account-edit',{'field':param});
            }

            $scope.qtdSolutions = 0;
            $scope.qtdLikeSolution = 0;
            $scope.qtdPosts = 0;
            updateprofile.getProfileMetrics($scope.profiledata.userid).success(function (result) {
                if(result.success == 1)
                {
                    $scope.qtdSolutions = result.data[0].qtd_solution;
                    $scope.qtdLikeSolution = result.data[0].qtd_solution_like;
                    $scope.qtdPosts = result.data[0].qtd_post;
                }
            });


            $scope.confirmLogout = function () {

                var confirmPopup = $ionicPopup.confirm({
                    title: 'Refriplay',
                    template: 'Você deseja sair do aplicativo?',
                    okType: 'button-balanced',
                    cancelType: 'button-balanced',
                    cancelText: 'Não',
                    okText: 'Sim'
                });

                confirmPopup.then(function (res) {
                    if (res) {
                        $window.localStorage.removeItem('profile');
                        //$window.localStorage.clear();
                        $ionicHistory.clearCache();
                        $ionicHistory.clearHistory();
                        $state.go('login-intro');
                    } else {

                    }
                });

            }

            $scope.data = {
                'name': username,
                'feed_view': feed_view,
                'gender': gender,
                'profile_pic': $scope.profiledata.profile_pic,
                'cpf':cpf

            };

            $scope.updateprofile = function (form) {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                if (form.$valid){
                        if($scope.data.password != undefined && password !== md5.createHash($scope.data.password))
                        {
                            $ionicLoading.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'A senha atual está incorreta'
                            });
                            return false;
                        }
                        updateprofile.updateprofile($scope.data.name, $scope.data.new_password, $scope.data.feed_view, $scope.data.gender, userid,$scope.data.cpf).success(function (newpostdata) {
                         // Upload profile photo

                        if(newpostdata.success == 1)
                        {
                            password = $scope.data_new_passoword;
                            var userdata = newpostdata.userdetails;
                            $window.localStorage['profile'] = JSON.stringify(userdata);

                            $ionicLoading.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Atualizado com sucesso!'
                            });
                        }
                        else
                        {
                            $ionicLoading.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Houve um erro :('
                            });
                            return false;
                        }


                        /*if ($scope.updateprofileflag) {
                            var options = {
                                fileName: userid + ".jpg",
                                chunkedMode: false
                            };
                            var params = new Object();
                            params.userid = userid;

                            options.params = params;

                            $cordovaFileTransfer.upload(SERVER_URL + "services/uploadProfilePicture.php", $scope.selprofilepic, options, true).then(function (result) {
                                $scope.updateprofileflag = false;
                                console.log(JSON.stringify(result));
                                //alert("SUCCESS: " + JSON.stringify(result.response));
                                if (!$scope.$$phase) {
                                    $scope.$apply();
                                }
                                $ionicLoading.hide();
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Refriplay',
                                    okType: 'button-balanced',
                                    template: 'Atualizado com sucesso!'
                                });

                            }, function (err) {
                                //alert("ERROR: " + JSON.stringify(err));
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Refriplay',
                                    okType: 'button-balanced',
                                    template: 'Houve um erro :('
                                });
                                return false;
                            }, function (progress) {
                                // constant progress updates
                            });
                        } else {
                            $ionicLoading.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Atualizado com sucesso!'
                            });
                        }*/
                        // upload code ends here
                        /// upload profile photo code ends here

                    });
                }
                else
                {
                    $ionicLoading.hide();
                }
            }



            $scope.selectables = [
                1
              ];

              $scope.longList  = [];
              for(var i=0;i<1000; i++){
                $scope.longList.push(i);
              }

              $scope.selectableNames =  [
                { name : "Mauro", role : "black hat"},
                { name : "Silvia", role : "pineye"},
                { name : "Merlino", role : "little canaglia"},
              ];

              $scope.someSetModel = 'Mauro';

              $scope.getOpt = function(option){
                return option.name + ":" + option.role;
              };

              $scope.shoutLoud = function(newValuea, oldValue){
                alert("changed from " + JSON.stringify(oldValue) + " to " + JSON.stringify(newValuea));
              };

              $scope.shoutReset = function(){
                alert("value was reset!");
              };
        })

        .controller('TrendinghashtagsCtrl', function ($scope, SERVER_URL, $ionicLoading, $window, $ionicPopup, $state, gettrendinghashtags) {

            gettrendinghashtags.gettrendinghashtags().success(function (trendinghashtagsdata) {
                $scope.trendhashtags = trendinghashtagsdata;
            });

        })

.controller('estadosCtrl', function ($scope, SERVER_URL, $ionicLoading, $window, $ionicPopup, $state, getstates,checkinplaces,setFeedState,$rootScope,$ionicPlatform) {
    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
        viewData.enableBack = false;
    });

    $ionicPlatform.ready(function() {
        $rootScope.analytics('Selecionar estados');
    });

    $scope.lat = 0;
    $scope.lng = 0;

    $scope.noState = false;
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });



        navigator.geolocation.getCurrentPosition(function (position) {
            $scope.lat = position.coords.latitude;
            $scope.lng = position.coords.longitude;
        }, function(){
            //nothing here
        });

        getstates.getstates($scope.lat,$scope.lng).success(function (result) {
            if(result.success == 1)
            {
                $scope.states = result.data;
            }
            else
            {
                $scope.noState = true;
            }
        });
    $ionicLoading.hide();
   /*( if($window.cordova)
    {
        cordova.plugins.diagnostic.isLocationEnabled(function(enabled){
                if(enabled)
                {
                    $scope.getStates();
                }
                else
                {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Não foi possível encontrar a sua localização, por favor ative o GPS e abra o aplicativo novamente'
                    });
                    $ionicLoading.hide();
                    $state.go($rootScope.previousState.name);
                }
            },
            function(error){
                var alertPopup = $ionicPopup.alert({
                    title: 'Refriplay',
                    okType: 'button-balanced',
                    template: 'Não foi possível encontrar a sua localização, por favor ative o GPS e abra o aplicativo novamente'
                });
                $ionicLoading.hide();
                $state.go($rootScope.previousState.name);
            });
    }
    else
    {
        $scope.getStates();
        $ionicLoading.hide();
    }*/


            $scope.setFeedState = function(state){
                var user = JSON.parse($window.localStorage['profile']);

                setFeedState.setFeedState(state.short_name,user.userid).success(function (result) {
                    if(result.result == 1)
                    {
                        user.feed_view = state.short_name;
                        $window.localStorage['profile'] = JSON.stringify(user);
                    }
                    $state.go('tab.feeds');
                });
            }


        })

    .controller('ToolsCtrl', function ($scope, SERVER_URL, Tools,$window, $rootScope) {
        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = false;
            $rootScope.checkNotifications();

        });
        Tools.getTools().success(function (result) {
            $scope.tools = result;
        });

        $scope.goToTool = function(url)
        {
            $window.location.href = url;
        }

        $scope.isNew = function ( tool )
        {
            var _MS_PER_DAY = 1000 * 60 * 60 * 24;
            var pd  = new Date( tool.publish_date );
            var now = new Date();

            var newUntilDays = 25;

            var utc1 = Date.UTC( pd.getFullYear(), pd.getMonth(), pd.getDate() );
            var utc2 = Date.UTC( now.getFullYear(), now.getMonth(), now.getDate() );

            return Math.floor( ( utc2 - utc1 ) / _MS_PER_DAY ) <= 25;
        };
    })
    .controller('ErrorCategoryCtrl', function ($scope, SERVER_URL, ErrorCategory) {
        ErrorCategory.getErrorCategory().success(function (result) {
            $scope.server_url = SERVER_URL;
            $scope.cetegories = result;
        });
    })
    .controller('InsuranceCtrl', function($scope, Insurance) {
        Insurance.getText().success(function(result) {
            if(result.success) {
                $scope.text = result.text;
            }
        })
    })
    .controller('MarksCtrl', function ($scope, SERVER_URL, Marks,$ionicPopup) {
        Marks.getMarks().success(function (result) {
            $scope.server_url = SERVER_URL;
            $scope.marks = result;
        });
        $scope.is_disabled = function(status,event)
        {
            if(status == 0)
            {
                var alertPopup = $ionicPopup.alert({
                    title: 'Refriplay',
                    okType: 'button-balanced',
                    template: 'Ainda estamos recolhendo o material deste fabricante, assim que for concluído você será notificado'
                });
                event.preventDefault();
            }
            return false;
        }
    })
    .controller('ProductsCtrl', function ($scope, $stateParams,$state,$location,SERVER_URL, Products) {
            if($stateParams.mark_id >=1){
                Products.getProducts($stateParams.mark_id).success(function (result) {
                    $scope.server_url = SERVER_URL;
                    $scope.products = result;
                });
            }

            if($stateParams.products_id >=1){
                Products.getProductsErrorList($stateParams.products_id).success(function (result) {
                    $scope.server_url = SERVER_URL;
                    $scope.erros = result;
                });
            }

            $scope.go = function (url) {
                $state.go(url);
            }



    })
    .controller('ErrorCtrl', function ($scope, $stateParams,$window,$ionicModal,$ionicPopup,$ionicLoading,SERVER_URL, Error,Solutions,Likes,$rootScope,$ionicHistory,$state) {
        $backView = $ionicHistory.backView();

        if($backView && $backView.stateId == "tab.feeds")
        {
            $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
                viewData.enableBack = true;
            });
        }
        $rootScope.$ionicGoBack = function(backCount) {
            if($backView.stateId == "tab.feeds")
            {
                $state.go('tab.tools');
            }
            else
            {
                $ionicHistory.goBack();
            }
        };
        $scope.showExcluir=function(index) {
          $scope.solutions[index].excluir= true;
        }
        $scope.showExcluirBest=function() {
          $scope.bestSolution.excluir = true;
        }

        $scope.error_id = $stateParams.error_id;
        $scope.profiledata = JSON.parse($window.localStorage['profile']);
        $scope.user_id  = $scope.profiledata['userid'];
        Error.getError($stateParams.error_id).success(function (result) {
            $scope.server_url = SERVER_URL;
            $scope.error = result[0];
        });
        $scope.getSolutions = function () {
            Solutions.getSolutions($stateParams.error_id).success(function (result) {
                $scope.server_url = SERVER_URL;
                $scope.solutions = result;
                $scope.bestSolution = result[0];
                $scope.solutions.splice(0, 1);
            });
        }
        $scope.getSolutions();

        $scope.addSolution = function (form) {
            if(form.$valid)
            {
                Solutions.addSolution($scope.user_id,$scope.error.id,form.solution_text.$viewValue).success(function (result) {
                    $scope.getSolutions();
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Sua solução foi publicada com sucesso!'
                    });
                    form.solution_text.$viewValue ='';
                    $scope.closeModal();
                });
            }
            else
            {
                var alertPopup = $ionicPopup.alert({
                    title: 'Refriplay',
                    okType: 'button-balanced',
                    template: 'Você esqueceu de escrever a sua solução'
                });
            }

        }
        /*Desativa solucao de acordo com o ID dela*/
        $scope.desativaSolution = function (solution_id) {
          var confirmPopup = $ionicPopup.confirm({
              title: 'Refriplay',
              template: 'Você deseja excluir esta solução?',
              okType: 'button-balanced',
              cancelType: 'button-balanced',
              cancelText: 'Não',
              okText: 'Sim'
          });

          confirmPopup.then(function (res) {

              $ionicLoading.show({
                  content: 'Loading',
                  animation: 'fade-in',
                  showBackdrop: true,
                  maxWidth: 200,
                  showDelay: 0
              });

              if (res == true) {
                Solutions.disableSolution(solution_id).success(function (result) {
                  //console
                  $scope.getSolutions();
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Sua solução foi excluida  com sucesso!'
                    });
                        $ionicLoading.hide();

                });
              }else{
                $ionicLoading.hide();

              }
        }
      )}

        $scope.addLike = function (like,solution_id,index) {
            Likes.addLike($scope.user_id,like,solution_id).success(function (result) {
                if(result.error)
                {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Você já curtiu essa avaliação!'
                    });
                }
                else
                {
                    if(index == -1)
                    {
                        if(like == 0)
                        {
                            if($scope.bestSolution.likes > 0)
                            {
                                $scope.bestSolution.likes = parseInt($scope.bestSolution.likes)-parseInt(1);
                            }

                            if(result.success == 2)
                            {
                                $scope.bestSolution.deslikes = parseInt($scope.bestSolution.deslikes)+parseInt(1);
                            }
                        }
                        else
                        {
                            $scope.bestSolution.likes = parseInt($scope.bestSolution.likes)+parseInt(1);
                            if(result.success == 2)
                            {
                                if($scope.bestSolution.deslikes > 0)
                                {
                                    $scope.bestSolution.deslikes = parseInt($scope.bestSolution.deslikes)-parseInt(1);
                                }
                            }
                        }
                    }
                    else
                    {
                        if(like == 0)
                        {
                            $scope.solutions[index].deslikes = parseInt($scope.solutions[index].deslikes)+parseInt(1);
                            if(result.success == 2)
                            {
                                if($scope.solutions[index].likes > 0)
                                {
                                    $scope.solutions[index].likes = parseInt($scope.solutions[index].likes)-parseInt(1);
                                }
                            }
                        }
                        else
                        {
                            $scope.solutions[index].likes = parseInt($scope.solutions[index].likes)+parseInt(1);
                            if(result.success == 2)
                            {
                                if($scope.solutions[index].deslikes > 0)
                                {
                                    $scope.solutions[index].deslikes = parseInt($scope.solutions[index].deslikes)-parseInt(1);
                                }
                            }
                        }

                    }
                }
            })
        }

        $ionicModal.fromTemplateUrl('templates/modais/modal-solution.html', {
            scope: $scope,
            animation: 'slide-in-up',
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.openModal = function() {
            $scope.modal.show();
        };

        $scope.closeModal = function() {
            $scope.modal.hide();
        };

        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.modal.remove();
        });

        // Execute action on hide modal
        $scope.$on('modal.hidden', function() {
            // Execute action
        });

        // Execute action on remove modal
        $scope.$on('modal.removed', function() {
            // Execute action
        });
    })
    .controller('BudgetCtrl', function ($scope,$rootScope, $stateParams, SERVER_URL, $http, $window, $filter, $ionicPopup,$state) {
        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });
        var oldSoftBack = $rootScope.$ionicGoBack;
        $rootScope.$ionicGoBack = function(backCount) {
            $state.go('tab.tools');
            oldSoftBack();
        };

        $scope.pdfView = function(id){
          $state.go('tab.budget-pdf',{'budgetId':id});
        };

        $scope.editBudget = function(id){
            $state.go('tab.budget-edit',{'budgetId':id});
        };

        var userdata = JSON.parse( $window.localStorage['profile'] );

        var loading = $ionicPopup.show({
            // title: 'Carregando...',
            template: '<h1>Carregando...</h1>',
            cssClass: 'popup-default',
            // okType: 'button-popup-default',
            // cancelType: 'button-popup-default-cancel',
            // cancelText: 'Sair',
            // okText: 'Continuar'
        });

        $scope.groups = [
            { id: 1, "name":"Pendentes", "opened": true},
            { id: 2, "name":"Aprovados" },
            { id: 3, "name":"Cancelados" }
        ];

        $http({
            methot: "GET",
            url: SERVER_URL + "services/budgets.php?type=byOwner&technician_id=" + userdata.userid,
            cache: false,
        }).then(
            function ( response )
            {
                if ( !response.data.success )
                    return loading.close();

                $scope.budgets = response.data.data;

                loading.close();
            }
        );


        $scope.getTotal = function ( budget )
        {
            var t = parseFloat( budget.total_labors, 10 ) + parseFloat( budget.total_products, 10 );

            return "R$ " + t.toFixed( 2 ).replace( ".", "," );
        };

        $scope.getDate = function ( date )
        {
            return $filter( "date" )( new Date( date ), 'dd/MM/yyyy' );
        };

        angular.element(document).off("click.status");

        angular.element( document ).on( "click.status", function ( evt ) {
            var btn = angular.element( evt.target );

            if ( btn.parents( ".change-status" ).length )
                btn = btn.parents( ".change-status" );

            if ( !btn.is( ".change-status" ) ) {
                angular.element( ".status-budget" ).removeClass( "opened" );
                return;
            }

            if ( btn.parents( ".status-budget" ).length ) {
                return;
            } else {
                var bid = btn.attr( "data-budget-id" );
                var t   = angular.element( ".status-budget[data-budget-id='"+ bid +"']" );

                t.toggleClass( "opened" )
                    .css({
                        top: btn.offset().top - btn.parents( ".budget-group" ).offset().top,
                        left: btn.offset().left - 100,
                        right: "1em"
                    });

            }
        });


        $scope.changeStatus = function ( budget, status )
        {

            if(status == 4)
            {
                var confirmPopup = $ionicPopup.confirm({
                    title: '<h3>Refriplay</h3>',
                    template: 'Você deseja deletar o seu orçamento?',
                    cssClass: 'popup-default',
                    okType: 'button-popup-default',
                    okText: 'Não',
                    cancelType: 'button-popup-default-cancel',
                    cancelText: 'Sim',
                });

                confirmPopup.then( function ( result ) {
                    if ( !result ){
                        budget.status = status;
                        $http({
                            method: "PUT",
                            url: SERVER_URL + "services/budgets.php?id=" + budget.id,
                            data: {
                                status: status
                            }
                        });
                    }
                });
            }
            else
            {
                budget.status = status;
                $http({
                    method: "PUT",
                    url: SERVER_URL + "services/budgets.php?id=" + budget.id,
                    data: {
                        status: status
                    }
                });
            }
        };
    })
    .controller('ProdutosCtrl', function ($scope,$rootScope,$stateParams,SERVER_URL, $http, $window, $ionicPopup, $stateParams, $state, $filter) {

        // carrega dados iniciais
        var oldSoftBack = $rootScope.$ionicGoBack;
        $rootScope.$ionicGoBack = function(backCount) {
            $state.go('tab.produtos');
            oldSoftBack();
        };

        var userdata = JSON.parse( $window.localStorage['profile'] );


        if($stateParams.produtoId)
        {
            var loading = $ionicPopup.show({
                template: '<h1>Carregando...</h1>',
                cssClass: 'popup-default',
            });

            $http({
                method: "GET",
                url: SERVER_URL + "services/budgetUserProducts.php?id=" + $stateParams.produtoId,
                cache: false
            }).then(
                function ( response )
                {
                    if ( !response.data.success )
                        return loading.close();

                    $scope.produto = response.data.data;
                    $scope.produto.price = $scope.produto.price.replace('.', ',');
                    $scope.produto.price = "R$ " + $scope.produto.price;

                    loading.close();
                }
            );
        } else {
            var loading = $ionicPopup.show({
                template: '<h1>Carregando...</h1>',
                cssClass: 'popup-default',
            });

            $http({
                method: "GET",
                url: SERVER_URL + "services/budgetUserProducts.php?user_id=" + userdata.userid,
                cache: false
            }).then(
                function ( response )
                {
                    if ( !response.data.success )
                        return loading.close();

                    $scope.produtos = response.data.data;

                    loading.close();
                }
            );
        }

        $scope.deletarProduto = function(index, produtoId) {

            var confirmPopup = $ionicPopup.show({
                template: '',
                title: 'Tem certeza que deseja deletar esse produto?',
                cssClass: 'popup-default',
                scope: $scope,
                buttons: [
                { text: 'Cancelar' },
                {
                    text: '<b>Ok</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        $http({
                            method: 'DELETE',
                            url: SERVER_URL + "services/budgetUserProducts.php?id=" + produtoId,
                            data: $scope.produto
                        }).then(function(response) {
                            if ( !response.data.success ) {
                                $ionicPopup.alert({
                                    title: '<h3>Algo deu errado :(</h3>',
                                    template: 'Não foi possível deletar o produto, tente novamente por favor',
                                    cssClass: 'popup-default',
                                    okType: 'button-popup-default',
                                    okText: 'Ok'
                                });
                                return;
                            } else
                            {
                                $scope.produtos.splice(index, 1);
                            }
                        })
                    }
                }
                ]

            });

            confirmPopup.then(function(res) {
                if(res) {
                    $scope.produtos.splice(index, 1);
                } else {
                console.log('You are not sure');
                }
            });
        }

        $scope.getPrice = function ( price ) {
            if ( !price )
                return "";

            var p = price;
            p = p.replace( "R$", "" );
            p = p.replace( ".", "" );
            p = p.replace( ",", "." );
            p = p.replace( /\s/g, "" );
            p = parseFloat( p, 10 );
            return p
        };

        $scope.getHighPrice = function ( price ) {
            if ( !price )
                return "";

            var p = price;
            p = p.replace( "R$", "" );
            p = p.replace( ".", "" );
            p = p.replace( ",", "." );
            p = p.replace( /\s/g, "" );
            p = parseFloat( p, 10 )/100;
            return p
        };

        $scope.produto = $scope.produto || {};

        $scope.saveProduto = function (form) {

            if ( form.$invalid )
                return;

            $scope.produto.price = $scope.getPrice($scope.produto.price);

            var loading = $ionicPopup.show({
                template: '<h1>Salvando...</h1>',
                cssClass: 'popup-default',
            });

            var method = $stateParams.produtoId ? "PUT" : "POST";
            var query  = $stateParams.produtoId ? "?id=" + $stateParams.produtoId : "";

            $scope.produto.user_id = userdata.userid;

            $http({
                method: method,
                url: SERVER_URL + "services/budgetUserProducts.php" + query,
                data: $scope.produto
            }).then(
                function ( response ) {

                    loading.close();

                    if ( !response.data.success ) {
                        $ionicPopup.alert({
                            title: '<h3>Algo deu errado :(</h3>',
                            template: 'Não foi possível salvar sua informação, tente novamente por favor',
                            cssClass: 'popup-default',
                            okType: 'button-popup-default',
                            okText: 'Ok'
                        });
                        return;
                    }

                    $state.go( "tab.produtos" );

                    $scope.produto = response.data.data;

                }
            );
        }
    })
    .controller('ServicosCtrl', function ($scope,$rootScope,$stateParams,SERVER_URL, $http, $window, $ionicPopup, $stateParams, $state, $filter) {

        // carrega dados iniciais
        var oldSoftBack = $rootScope.$ionicGoBack;
        $rootScope.$ionicGoBack = function(backCount) {
            $state.go('tab.servicos');
            oldSoftBack();
        };

        var userdata = JSON.parse( $window.localStorage['profile'] );


        if($stateParams.servicoId)
        {
            var loading = $ionicPopup.show({
                template: '<h1>Carregando...</h1>',
                cssClass: 'popup-default',
            });

            $http({
                method: "GET",
                url: SERVER_URL + "services/budgetUserServices.php?id=" + $stateParams.servicoId,
                cache: false
            }).then(
                function ( response )
                {
                    if ( !response.data.success )
                        return loading.close();

                    $scope.servico = response.data.data;
                    $scope.servico.price = $scope.servico.price.replace('.', ',');
                    $scope.servico.price = "R$ " + $scope.servico.price;

                    loading.close();
                }
            );
        } else {
            var loading = $ionicPopup.show({
                template: '<h1>Carregando...</h1>',
                cssClass: 'popup-default',
            });

            $http({
                method: "GET",
                url: SERVER_URL + "services/budgetUserServices.php?user_id=" + userdata.userid,
                cache: false
            }).then(
                function ( response )
                {
                    if ( !response.data.success )
                        return loading.close();

                    $scope.servicos = response.data.data;

                    loading.close();
                }
            );
        }

        $scope.getPrice = function ( price ) {
            if ( !price )
                return "";

            var p = price;
            p = p.replace( "R$", "" );
            p = p.replace( ".", "" );
            p = p.replace( ",", "." );
            p = p.replace( /\s/g, "" );
            p = parseFloat( p, 10 );
            return p
        };

        $scope.getHighPrice = function ( price ) {
            if ( !price )
                return "";

            var p = price;
            p = p.replace( "R$", "" );
            p = p.replace( ".", "" );
            p = p.replace( ",", "." );
            p = p.replace( /\s/g, "" );
            p = parseFloat( p, 10 )/100;
            return p
        };

        $scope.deletarServico = function(index, servicoId) {

            var confirmPopup = $ionicPopup.show({
                template: '',
                title: 'Tem certeza que deseja deletar esse serviço?',
                cssClass: 'popup-default',
                scope: $scope,
                buttons: [
                { text: 'Cancelar' },
                {
                    text: '<b>Ok</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        $http({
                            method: 'DELETE',
                            url: SERVER_URL + "services/budgetUserServices.php?id=" + servicoId,
                            data: $scope.produto
                        }).then(function(response) {
                            if ( !response.data.success ) {
                                $ionicPopup.alert({
                                    title: '<h3>Algo deu errado :(</h3>',
                                    template: 'Não foi possível deletar o serviço, tente novamente por favor',
                                    cssClass: 'popup-default',
                                    okType: 'button-popup-default',
                                    okText: 'Ok'
                                });
                                return;
                            } else
                            {
                                $scope.servicos.splice(index, 1);
                            }
                        })
                    }
                }
                ]

            });

            confirmPopup.then(function(res) {
                if(res) {
                    $scope.servicos.splice(index, 1);
                } else {
                console.log('You are not sure');
                }
            });
        }

        $scope.servico = $scope.servico || {};

        $scope.saveServico = function (form) {

            if ( form.$invalid )
                return;

                $scope.servico.price = $scope.getPrice($scope.servico.price);

            var loading = $ionicPopup.show({
                template: '<h1>Salvando...</h1>',
                cssClass: 'popup-default',
            });

            var method = $stateParams.servicoId ? "PUT" : "POST";
            var query  = $stateParams.servicoId ? "?id=" + $stateParams.servicoId : "";

            $scope.servico.user_id = userdata.userid;

            $http({
                method: method,
                url: SERVER_URL + "services/budgetUserServices.php" + query,
                data: $scope.servico
            }).then(
                function ( response ) {

                    loading.close();

                    if ( !response.data.success ) {
                        $ionicPopup.alert({
                            title: '<h3>Algo deu errado :(</h3>',
                            template: 'Não foi possível salvar sua informação, tente novamente por favor',
                            cssClass: 'popup-default',
                            okType: 'button-popup-default',
                            okText: 'Ok'
                        });
                        return;
                    }

                    $state.go( "tab.servicos" );

                    $scope.servico = response.data.data;

                }
            );
        }
    })
    .controller('ClientsCtrl', function ($scope,$rootScope,$stateParams,SERVER_URL, $http, $window, $ionicPopup, $stateParams, $state, $filter) {
        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });
        var userdata = JSON.parse( $window.localStorage['profile'] );

        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });


        $scope.client = {'type':'cpf'};
        $scope.cliente = $scope.cliente || {};

        var oldSoftBack = $rootScope.$ionicGoBack;

        // carrega dados iniciais
        if ( $stateParams.clientId && !$scope.cliente.id ) {
            var oldSoftBack = $rootScope.$ionicGoBack;
            $rootScope.$ionicGoBack = function(backCount) {
                $state.go('tab.clientes');
                oldSoftBack();
            };
            // editar
            var loading = $ionicPopup.show({
                template: '<h1>Carregando...</h1>',
                cssClass: 'popup-default',
            });

            $http({
                method: "GET",
                url: SERVER_URL + "services/clients.php?id=" + $stateParams.clientId,
                cache: false
            }).then(
                function ( response )
                {
                    if ( !response.data.success )
                        return loading.close();

                    $scope.cliente = response.data.data;
                    if($scope.cliente.cnpj.length > 0)
                    {
                        $scope.client.type = 'cnpj';
                    }
                    loading.close();
                }
            );
        } else {

            if ( $state.is( "tab.clientes" ) ) {

                var oldSoftBack = $rootScope.$ionicGoBack;
                $rootScope.$ionicGoBack = function(backCount) {
                    $state.go('tab.tools');
                    oldSoftBack();
                };
                var loading = $ionicPopup.show({
                    template: '<h1>Carregando...</h1>',
                    cssClass: 'popup-default',
                });

                $http({
                    method: "GET",
                    url: SERVER_URL + "services/clients.php?type=byTechnicial&technician_id=" + userdata.userid,
                    cache: false,
                }).then(
                    function ( response )
                    {
                        if ( !response.data.success )
                            return loading.close();

                        $scope.clients = $filter( "orderBy" )( response.data.data, "+full_name" );

                        loading.close();
                    }
                );

            }
            else
            {
                $rootScope.$ionicGoBack = function(backCount) {
                    $state.go('tab.clientes');
                    oldSoftBack();
                };
            }
        }


        $scope.saveClient = function ( form,index ) {

            if ( form.$invalid )
                return;

            var loading = $ionicPopup.show({
                template: '<h1>Salvando...</h1>',
                cssClass: 'popup-default',
            });

            var method = $stateParams.clientId ? "PUT" : "POST";
            var query  = $stateParams.clientId ? "?id=" + $stateParams.clientId : "";

            $scope.cliente.technician_id = userdata.userid;
            if($scope.client.type == 'cnpj')
            {
                delete $scope.cliente.cpf;
            }

            if($scope.client.type == 'cpf')
            {
                delete $scope.cliente.cnpj;
            }

            $http({
                method: method,
                url: SERVER_URL + "services/clients.php" + query,
                data: $scope.cliente
            }).then(
                function ( response ) {

                    loading.close();

                    if ( !response.data.success ) {
                        $ionicPopup.alert({
                            title: '<h3>Algo deu errado :(</h3>',
                            template: 'Não foi possível salvar sua informação, tente novamente por favor',
                            cssClass: 'popup-default',
                            okType: 'button-popup-default',
                            okText: 'Ok'
                        });
                        return;
                    }

                    $state.go( "tab.clientes" );

                    $scope.cliente = response.data.data;

                }
            );
        }

    })
    .controller('NewBudgetCtrl', function ($rootScope, $scope, $stateParams,SERVER_URL, $ionicPopup, $ionicModal, $window, $http, $filter, $state) {
        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });
        var oldSoftBack = $rootScope.$ionicGoBack;
        if($state.current.name == 'tab.budget-review')
        {
            $rootScope.$ionicGoBack = function(backCount) {
                $state.go('tab.budget-new');
                oldSoftBack();
            };
        }
        else
        {
            $rootScope.$ionicGoBack = function(backCount) {
                $state.go('tab.budget');
                oldSoftBack();
            };
        }


        var userdata = JSON.parse( $window.localStorage['profile'] );

        $scope.client = {'type':'cpf'};

        $scope.cliente = {};
        $scope.user    = userdata;

        $scope.clientsModal   = null;
        $scope.newClientModal = null;
        $scope.produtosModal = null;
        $scope.servicosModal = null;
        $scope.produtosSelecionados = [];
        $scope.servicosSelecionados = [];


        $rootScope.budget = $rootScope.budget || {
            products: [ {} ],
            labors  : [ {} ]
        };

        $scope.baction = function($scope,$stateParams){
            $ionicModal.fromTemplateUrl( "templates/modais/budget-client-list.html", {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.clientsModal = modal;
            });

            $ionicModal.fromTemplateUrl( "templates/modais/budget-client-new.html", {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.newClientModal = modal;
            });

            $ionicModal.fromTemplateUrl( "templates/modais/budget-pdf.html", {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.pdfModal = modal;
            });


            $ionicModal.fromTemplateUrl( "templates/modais/budget-produtos.html", {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.produtosModal = modal;
            });

            $ionicModal.fromTemplateUrl( "templates/modais/budget-servicos.html", {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.servicosModal = modal;
            });

            $scope.openServicosModal = function ()
            {
                $scope.servicosModal.show();

                var loading = $ionicPopup.show({
                    template: '<h1>Carregando...</h1>',
                    cssClass: 'popup-default',
                });

                $http({
                    method: "GET",
                    url: SERVER_URL + "services/budgetUserServices.php?user_id=" + userdata.userid,
                    cache: false,
                }).then(
                    function ( response )
                    {
                        if ( !response.data.success )
                            return loading.close();

                        $scope.labors = $filter( "orderBy" )( response.data.data, "+name" );

                        loading.close();
                    }
                );
            };

            $scope.updateServicosSelecionados = function(servico){
                if(servico.selecionado){
                    $scope.servicosSelecionados.push(servico);
                } else{
                    $scope.servicosSelecionados.splice($scope.servicosSelecionados.indexOf(servico.id), 1);
                }
            };

            $scope.selecionarServicos = function()
            {
                $scope.budget.labors = $scope.servicosSelecionados;
                $scope.closeServicosModal();
                let index = $scope.budget.labors.length;
                $(function() {
                    $('.p_price_service').maskMoney('mask',{
                        decimal: ',',
                        thousands: '.',
                        precision: 2,
                        prefix: 'R$ ',
                        allowZero: true
                    });
                    $('.p_price_service').change();
                });

                //$scope.addNextRow( $scope.budget.labors[index - 1], index, $scope.budget.labors )
            }

            $scope.openProdutosModal = function ()
            {
                $scope.produtosModal.show();

                var loading = $ionicPopup.show({
                    template: '<h1>Carregando...</h1>',
                    cssClass: 'popup-default',
                });

                $http({
                    method: "GET",
                    url: SERVER_URL + "services/budgetUserProducts.php?user_id=" + userdata.userid,
                    cache: false,
                }).then(
                    function ( response )
                    {
                        if ( !response.data.success )
                            return loading.close();

                        $scope.produtos = $filter( "orderBy" )( response.data.data, "+name" );
                        loading.close();
                    }
                );
            };


            $scope.updateProdutosSelecionados = function(produto){
                if(produto.selecionado){
                    $scope.produtosSelecionados.push(produto);
                } else{
                    $scope.produtosSelecionados.splice($scope.produtosSelecionados.indexOf(produto.id), 1);
                }

            };

            $scope.selecionarProdutos = function()
            {
                $scope.budget.products = $scope.produtosSelecionados;
                $scope.closeProdutosModal();
                let index = $scope.budget.products.length;

                $(function() {
                    $('.p_price').maskMoney('mask',{
                        decimal: ',',
                        thousands: '.',
                        precision: 2,
                        prefix: 'R$ ',
                        allowZero: true
                    });
                    $('.p_price').change();
                });
                //$scope.addNextRow( $scope.budget.products[index - 1], index, $scope.budget.products )
            }

            $scope.openClientsModal = function ()
            {
                $scope.clientsModal.show();

                var loading = $ionicPopup.show({
                    template: '<h1>Carregando...</h1>',
                    cssClass: 'popup-default',
                });

                $http({
                    method: "GET",
                    url: SERVER_URL + "services/clients.php?type=byTechnicial&technician_id=" + userdata.userid,
                    cache: false,
                }).then(
                    function ( response )
                    {
                        if ( !response.data.success )
                            return loading.close();

                        $scope.clients = $filter( "orderBy" )( response.data.data, "+full_name" );

                        loading.close();
                    }
                );
            };


            $scope.closeClientsModal = function ()
            {
                $scope.clientsModal.hide();
            };

            $scope.closeProdutosModal = function ()
            {
                $scope.produtosModal.hide();
            };


            $scope.closeServicosModal = function ()
            {
                $scope.servicosModal.hide();
            };

            $scope.openNewClientModal = function ()
            {
                $scope.newClientModal.show();
            };

            $scope.closeNewClientModal = function ()
            {
                $scope.newClientModal.hide();
            };

            $scope.selectClient = function ( client )
            {
                $rootScope.budget.client = client;
                $rootScope.budget.client_id = client.id;
                $scope.clientsModal.hide();
            };

            $scope.saveClient = function ( form, client )
            {
                if ( form.$invalid )
                    return;

                $rootScope.budget.client = client;
                $rootScope.budget.client_id = [client];
                $scope.newClientModal.hide();
            };

            $scope.addNextRow = function ( obj, index, model )
            {
                var add = false;

                for (var p in obj)
                    if ( obj[p] != "" )
                        add = true;

                if ( !add )
                    return;

                if ( model.length - 1 > index )
                    return;


                model.push({});
                //model[index+1] = {};

            };

            $scope.hideRemoveRow = function (model) {
                return model.length <= 1;
            }

            $scope.removeRow = function(index, model)
            {
                model.splice(index, 1);
            };

            $scope.getPrice = function ( price )
            {
                if ( !price )
                    return "";

                var p = price;
                p = p.replace( "R$", "" );
                p = p.replace( ".", "" );
                p = p.replace( ",", "." );
                p = p.replace( /\s/g, "" );
                p = parseFloat( p, 10 );
                return p
            };

            $scope.getTotal = function ( collection )
            {
                var t = 0;
                var cl = collection.length;
                if(typeof collection === 'object')
                {
                    cl = Object.keys(collection).length;
                }

                for (var i = 0; i < cl; i++) {
                    if (!collection[i] || !collection[i].price )
                        continue;

                    var p = $scope.getPrice( collection[i].price );
                    p *= collection[i].qty;

                    t += p;
                }
                return t;
            };

            $scope.alertLeaving = function ()
            {
                var confirmPopup = $ionicPopup.confirm({
                    title: '<h3>Aviso Importante</h3>',
                    template: 'Você está saindo antes de finalizar o orçamento, seus dados serão perdidos.<br><br>Deseja mesmo sair?',
                    cssClass: 'popup-default',
                    okType: 'button-popup-default',
                    okText: 'Ficar',
                    cancelType: 'button-popup-default-cancel',
                    cancelText: 'Sair',
                });

                confirmPopup.then( function ( result ) {
                    if ( !result ){
                        $rootScope.budget = {
                            products: [ {} ],
                            labors  : [ {} ]
                        };
                        $state.go( "tab.account" );
                    }
                });
            };

            $scope.reviewBudget = function ( form )
            {
                var b = angular.copy( $rootScope.budget );
                var error = false;

                for (var i = b.products.length - 1; i > 0; i--) {
                    var item = b.products[i];

                    if (   ( typeof item.name  == "undefined" || !item.name )
                        && ( typeof item.qty   == "undefined" || !item.qty )
                        && ( typeof item.price == "undefined" || !item.price )
                        && ( typeof item.un    == "undefined" || !item.un ) )
                        b.products.splice( i, 1 );
                }

                for (var i = b.labors.length - 1; i > 0; i--) {
                    var item = b.labors[i];

                    if (   ( typeof item.name  == "undefined" || !item.name )
                        && ( typeof item.qty   == "undefined" || !item.qty )
                        && ( typeof item.price == "undefined" || !item.price ) )
                        b.labors.splice( i, 1 );
                }

                // if ( b.products.length == 1 && b.labors.length == 1 )
                //     error = true;

                // var removeRow = function(value) {
                //     return value.price == "R$ 0,00" && value.name == null && value.qty == null;
                // }

                // var filterLaborsInvalid = b.labors.filter(removeRow);
                // var filterProductsInvalid = b.products.filter(removeRow);

                if ( form.$invalid || error )
                    return $ionicPopup.alert({
                        title: "<h3>Oops...</h3>",
                        template: "Confira se todos os campos estão corretos e tente novamente.",
                        cssClass: "popup-default",
                        okText: "Ok",
                        okType: "button-popup-default"
                    });


                if($stateParams.budgetId)
                {
                    $state.go("tab.budget-review-edit",{'budgetId':$stateParams.budgetId});
                }
                else
                {
                    $state.go( "tab.budget-review");
                }

            };


            $scope.createBudget = function ()
            {
                var loading = $ionicPopup.show({
                    template: '<h1>Criar PDF...</h1>',
                    cssClass: 'popup-default',
                });

                $rootScope.budget.technician_id = userdata.userid;

                var budget = angular.copy( $rootScope.budget );

                for (var i = budget.products.length - 1; i > 0; i--) {
                    var item = budget.products[i];

                    if (   ( typeof item.name  == "undefined" || !item.name )
                        && ( typeof item.qty   == "undefined" || !item.qty )
                        && ( typeof item.price == "undefined" || !item.price )
                        && ( typeof item.un    == "undefined" || !item.un ) )
                        budget.products.splice( i, 1 );
                }

                for (var i = budget.labors.length - 1; i > 0; i--) {
                    var item = budget.labors[i];

                    if (   ( typeof item.name  == "undefined" || !item.name )
                        && ( typeof item.qty   == "undefined" || !item.qty )
                        && ( typeof item.price == "undefined" || !item.price ) )
                        budget.labors.splice( i, 1 );
                }

                for (var i = 0; i < budget.products.length; i++)
                    budget.products[i].price = $scope.getPrice( budget.products[i].price );

                for (var i = 0; i < budget.labors.length; i++)
                    budget.labors[i].price = $scope.getPrice( budget.labors[i].price );

                $http({
                    method: "POST",
                    url: SERVER_URL + "services/budgets.php",
                    data: budget
                }).then(
                    function ( response ) {

                        loading.close();

                        if ( !response.data.success ) {
                            return $ionicPopup.alert({
                                title: "<h3>Oops...</h3>",
                                template: "Houve um erro ao criar o seu orçamento. Tente novamente por favor. Caso o erro persista entre em contato com nossa equipe.",
                                cssClass: "popup-default",
                                okText: "Ok",
                                okType: "button-popup-default"
                            });
                        }
                        else
                        {
                            $rootScope.budget = {
                                products: [ {} ],
                                labors  : [ {} ]
                            };
                            $state.go('tab.budget-pdf',{'budgetId':response.data.data});
                        }

                    }
                );
            };

            $scope.editBudget = function (budgetId)
            {
                var loading = $ionicPopup.show({
                    template: '<h1>Editando...</h1>',
                    cssClass: 'popup-default',
                });

                $rootScope.budget.technician_id = userdata.userid;

                var budget = angular.copy( $rootScope.budget );

                budget.products.pop();
                budget.labors.pop();

                for (var i = 0; i < budget.products.length; i++)
                    budget.products[i].price = $scope.getPrice( budget.products[i].price );

                for (var i = 0; i < budget.labors.length; i++)
                    budget.labors[i].price = $scope.getPrice( budget.labors[i].price );

                $http({
                    method: "PUT",
                    url: SERVER_URL + "services/budgets.php?id="+budgetId,
                    data: budget
                }).then(
                    function ( response ) {

                        loading.close();

                        if ( !response.data.success ) {
                            return $ionicPopup.alert({
                                title: "<h3>Oops...</h3>",
                                template: "Houve um erro ao editar o seu orçamento. Tente novamente por favor. Caso o erro persista entre em contato com nossa equipe.",
                                cssClass: "popup-default",
                                okText: "Ok",
                                okType: "button-popup-default"
                            });
                        }
                        else
                        {
                            $rootScope.budget = $rootScope.budget || {
                                products: [ {} ],
                                labors  : [ {} ]
                            };
                            $state.go('tab.budget-pdf',{'budgetId':response.data.data});
                        }

                    }
                );
            };

        };

        $scope.edit = false;
        if($state.current.name == 'tab.budget-review' || $state.current.name == 'tab.budget-review-edit')
        {
            if ( $stateParams.budgetId) {
                $scope.edit = true;
                $scope.budgetId = $stateParams.budgetId;
            }
            $scope.baction($scope,$stateParams);
        }
        else
        {
            if ( $stateParams.budgetId)
            {
                $scope.edit = true;
                // editar
                var loading = $ionicPopup.show({
                    template: '<h1>Carregando...</h1>',
                    cssClass: 'popup-default',
                });

                $http({
                    method: "GET",
                    url: SERVER_URL + "services/budgets.php?id=" + $stateParams.budgetId,
                    cache: false
                }).then(
                    function ( response )
                    {

                        if ( !response.data.success )
                            return loading.close();


                        $rootScope.budget = response.data.data || {
                            products: [ {} ],
                            labors  : [ {} ]
                        };

                        $(function() {
                            $('.p_price').maskMoney('mask',{
                                decimal: ',',
                                thousands: '.',
                                precision: 2,
                                prefix: 'R$ ',
                                allowZero: true
                            });
                        });
                        $scope.baction($scope,$stateParams);

                        //$('.p_price').maskMoney({ decimal: ',', thousands: '.', precision: 2 ,prefix:'R$ ',allowZero:true}).maskMoney('mask');
                        loading.close();
                    }
                );
            }
            else
            {
                $scope.baction($scope,$stateParams);
                //loading.close();
            }
        }


    })
    .controller('budgetPdfCtr', function ($rootScope, $scope,$stateParams,SERVER_URL,$state,$http,$ionicPopup,$ionicHistory) {
        $scope.budgetid=0;

        var loading = $ionicPopup.show({
            template: '<h1>Carregando PDF...</h1>',
            cssClass: 'popup-default',
        });

        $scope.windowWidth = $( window ).width();
        $scope.budgetid = $stateParams.budgetId;

        if ( $rootScope.isIOS )
            $http.get( SERVER_URL+'services/budgetPDF.php?id='+$scope.budgetid+'&return=filepath' )
                .then(function (resp) {
                    $scope.pdf = SERVER_URL+'services/' + resp.data.trim();
                });
        else
            $scope.pdf = SERVER_URL+'services/budgetPDF.php?id='+$scope.budgetid+'&debug';

        $scope.msgShare = 'Olá, segue o orçamento solicitado, criado em http://refriplay.com.br';
        $scope.subjectShare = 'Orçamento Refriplay '+$scope.budgetid;

        $scope.back = function(){
            $state.go('tab.budget', {}, { reload: true });
        };

        $scope.openShare = function()
        {
            setTimeout(function(){
                window.plugins.socialsharing.share($scope.msgShare, $scope.subjectShare, $scope.pdf);
			}, 0);
        }

        setTimeout(function(){
            loading.close();
        },1500);

        /*$scope.pdfData = null; // this is loaded async

        $http.get($scope.pdf+'&debug', {
            responseType: 'arraybuffer'
        }).then(function (response) {
            $scope.pdfData = new Uint8Array(response.data);
        });*/
    })
