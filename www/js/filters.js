angular.module('fortinsocial.filters', [])
.filter('randomSrc', function () {
    return function (input) {
        if (input)
            return input + '?r=' + Math.round(Math.random() * 999999);
    }
}).filter('trusted', ['$sce', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}]).filter('notification_img', function() {
    return function (data) {
        if(data == '' || data == undefined)
            return 'img/emptyavatar.jpg';
        else return data;
    }
}).filter('notification_type', function() {
    return function (type) {
        if(type == 'curtida')
            return 'curtiu a sua publicação';
        else if(type == 'comentario') {
            return 'também comentou sua publicação';
        }
    }
});