"use strict";angular.module("ngTextcomplete",[]).factory("utils",[function(){function lock(func){var free,locked,queuedArgsToReplay;free=function(){locked=false};return function(){var args=toArray(arguments);if(locked){queuedArgsToReplay=args;return}locked=true;var that=this;args.unshift(function replayOrFree(){if(queuedArgsToReplay){var replayArgs=queuedArgsToReplay;queuedArgsToReplay=undefined;replayArgs.unshift(replayOrFree);func.apply(that,replayArgs)}else{locked=false}});func.apply(this,args)}}function toArray(args){return Array.prototype.slice.call(args)}var getStyles=function(){var color;color=$("<div></div>").css(["color"]).color;if(typeof color!=="undefined"){return function($el,properties){return $el.css(properties)}}else{return function($el,properties){var styles;styles={};angular.forEach(properties,function(property,i){styles[property]=$el.css(property)});return styles}}}();function memoize(func){var memo={};return function(term,callback){if(memo[term]){callback(memo[term])}else{func.call(this,term,function(data){memo[term]=(memo[term]||[]).concat(data);callback.apply(null,arguments)})}}}function include(array,value){var i,l;if(array.indexOf)return array.indexOf(value)!=-1;for(i=0,l=array.length;i<l;i++){if(array[i]===value)return true}return false}return{lock:lock,toArray:toArray,getStyles:getStyles,memoize:memoize,include:include}}]).factory("Completer",["ListView","utils",function(ListView,utils){var html,css,$baseWrapper,$baseList,_id;html={wrapper:'<div class="textcomplete-wrapper"></div>',list:'<ul class="dropdown-menu"></ul>'};css={wrapper:{position:"relative"},list:{position:"absolute",left:0,zIndex:"100",display:"none"}};$baseWrapper=$(html.wrapper).css(css.wrapper);$baseList=$(html.list).css(css.list);_id=0;function Completer($el,strategies){var focus;this.el=$el.get(0);focus=this.el===document.activeElement;this.$el=wrapElement($el);this.id="textComplete"+_id++;this.strategies=[];if(focus){this.initialize();this.$el.focus()}else{this.$el.one("focus.textComplete",$.proxy(this.initialize,this))}}angular.extend(Completer.prototype,{initialize:function(){var $list,globalEvents;$list=$baseList.clone();this.listView=new ListView($list,this);this.$el.before($list).on({"keyup.textComplete":$.proxy(this.onKeyup,this),"keydown.textComplete":$.proxy(this.listView.onKeydown,this.listView)});globalEvents={};globalEvents["click."+this.id]=$.proxy(this.onClickDocument,this);globalEvents["keyup."+this.id]=$.proxy(this.onKeyupDocument,this);$(document).on(globalEvents)},register:function(strategies){this.strategies=this.strategies.concat(strategies)},renderList:function(data){if(this.clearAtNext){this.listView.clear();this.clearAtNext=false}if(data.length){if(!this.listView.shown){this.listView.setPosition(this.getCaretPosition()).clear().activate();this.listView.strategy=this.strategy}data=data.slice(0,this.strategy.maxCount);this.listView.render(data)}if(!this.listView.data.length&&this.listView.shown){this.listView.deactivate()}},searchCallbackFactory:function(free){var self=this;return function(data,keep){self.renderList(data);if(!keep){free();self.clearAtNext=true}}},onKeyup:function(e){var searchQuery,term;if(this.skipSearch(e)){return}searchQuery=this.extractSearchQuery(this.getTextFromHeadToCaret());if(searchQuery.length){term=searchQuery[1];if(this.term===term)return;this.term=term;this.search(searchQuery)}else{this.term=null;this.listView.deactivate()}},skipSearch:function(e){switch(e.keyCode){case 40:case 38:return true}if(e.ctrlKey)switch(e.keyCode){case 78:case 80:return true}},onSelect:function(value){var pre,post,newSubStr;pre=this.getTextFromHeadToCaret();post=this.el.value.substring(this.el.selectionEnd);newSubStr=this.strategy.replace(value);if(angular.isArray(newSubStr)){post=newSubStr[1]+post;newSubStr=newSubStr[0]}pre=pre.replace(this.strategy.match,newSubStr);this.$el.val(pre+post);$(this).trigger("change").trigger("textComplete:select",this.$el.val());this.el.focus();this.el.selectionStart=this.el.selectionEnd=pre.length},onClickDocument:function(e){if(e.originalEvent&&!e.originalEvent.keepTextCompleteDropdown){this.listView.deactivate()}},onKeyupDocument:function(e){if(this.listView.shown&&e.keyCode===27){this.listView.deactivate();this.$el.focus()}},destroy:function(){var $wrapper;this.$el.off(".textComplete");$(document).off("."+this.id);if(this.listView){this.listView.destroy()}$wrapper=this.$el.parent();$wrapper.after(this.$el).remove();this.$el.data("textComplete",void 0);this.$el=null},getCaretPosition:function(){var properties,css,$div,$span,position,dir,scrollbar;dir=this.$el.attr("dir")||this.$el.css("direction");properties=["border-width","font-family","font-size","font-style","font-variant","font-weight","height","letter-spacing","word-spacing","line-height","text-decoration","text-align","width","padding-top","padding-right","padding-bottom","padding-left","margin-top","margin-right","margin-bottom","margin-left","border-style","box-sizing"];scrollbar=this.$el[0].scrollHeight>this.$el[0].offsetHeight;css=$.extend({position:"absolute",overflow:scrollbar?"scroll":"auto","white-space":"pre-wrap",top:0,left:-9999,direction:dir},utils.getStyles(this.$el,properties));$div=$("<div></div>").css(css).text(this.getTextFromHeadToCaret());$span=$("<span></span>").text(".").appendTo($div);this.$el.before($div);position=$span.position();position.top+=$span.height()-this.$el.scrollTop();if(dir=="rtl"){position.left-=this.listView.$el.width()}$div.remove();return position},getTextFromHeadToCaret:function(){var text,selectionEnd,range;selectionEnd=this.el.selectionEnd;if(typeof selectionEnd==="number"){text=this.el.value.substring(0,selectionEnd)}else if(document.selection){range=this.el.createTextRange();range.moveStart("character",0);range.moveEnd("textedit");text=range.text}return text},extractSearchQuery:function(text){var i,l,strategy,match;for(i=0,l=this.strategies.length;i<l;i++){strategy=this.strategies[i];match=text.match(strategy.match);if(match){return[strategy,match[strategy.index]]}}return[]},search:utils.lock(function(free,searchQuery){var term;this.strategy=searchQuery[0];term=searchQuery[1];this.strategy.search(term,this.searchCallbackFactory(free))})});var wrapElement=function($el){return $el.wrap($baseWrapper.clone().css("display",$el.css("display")))};return Completer}]).factory("ListView",["utils",function(utils){function ListView($el,completer){this.data=[];this.$el=$el;this.index=0;this.completer=completer;this.$el.on("click.textComplete","li.textcomplete-item",$.proxy(this.onClick,this))}angular.extend(ListView.prototype,{shown:false,render:function(data){var html,i,l,index,val;html="";for(i=0,l=data.length;i<l;i++){val=data[i];if(utils.include(this.data,val))continue;index=this.data.length;this.data.push(val);html+='<li class="textcomplete-item" data-index="'+index+'"><a>';html+=this.strategy.template(val);html+="</a></li>";if(this.data.length===this.strategy.maxCount)break}this.$el.append(html);if(!this.data.length){this.deactivate()}else{this.activateIndexedItem()}},clear:function(){this.data=[];this.$el.html("");this.index=0;return this},activateIndexedItem:function(){this.$el.find(".active").removeClass("active");this.getActiveItem().addClass("active")},getActiveItem:function(){return $(this.$el.children().get(this.index))},activate:function(){if(!this.shown){this.$el.show();$(this).trigger("textComplete:show");this.shown=true}return this},deactivate:function(){if(this.shown){this.$el.hide();$(this).trigger("textComplete:show");this.shown=false;this.data=[];this.index=null}return this},setPosition:function(position){var fontSize;if(this.completer.strategy.placement==="top"){fontSize=parseInt(this.$el.css("font-size"));position={top:"auto",bottom:this.$el.parent().height()-position.top+fontSize}}else{position.bottom="auto"}if(this.$el.parent().offset().left+position.left+this.$el.width()>$(window).width()){position.right=this.$el.parent().width()-position.left;position.left="auto"}else{position.right="auto"}this.$el.css(position);return this},select:function(index){var self=this;this.completer.onSelect(this.data[index]);setTimeout(function(){self.deactivate()},0)},onKeydown:function(e){if(!this.shown)return;if(e.keyCode===27){this.deactivate()}else if(e.keyCode===38||e.ctrlKey&&e.keyCode===80){e.preventDefault();if(this.index===0){this.index=this.data.length-1}else{this.index-=1}this.activateIndexedItem()}else if(e.keyCode===40||e.ctrlKey&&e.keyCode===78){e.preventDefault();if(this.index===this.data.length-1){this.index=0}else{this.index+=1}this.activateIndexedItem()}else if(e.keyCode===13||e.keyCode===9){e.preventDefault();this.select(parseInt(this.getActiveItem().data("index"),10))}},onClick:function(e){var $e=$(e.target);e.originalEvent.keepTextCompleteDropdown=true;if(!$e.hasClass("textcomplete-item")){$e=$e.parents("li.textcomplete-item")}this.select(parseInt($e.data("index"),10))},destroy:function(){this.deactivate();this.$el.off("click.textComplete").remove();this.$el=null}});return ListView}]).factory("Textcomplete",["utils","Completer",function(utils,Completer){function identity(obj){return obj}function Textcomplete(element,strategies){var i,l,strategy,dataKey;dataKey="textComplete";if(strategies==="destroy"){return this.each(function(){var completer=$(this).data(dataKey);if(completer){completer.destroy()}})}for(i=0,l=strategies.length;i<l;i++){strategy=strategies[i];if(!strategy.template){strategy.template=identity}if(strategy.index==null){strategy.index=2}if(strategy.cache){strategy.search=memoize(strategy.search)}strategy.maxCount||(strategy.maxCount=10)}var completer;completer=element.data(dataKey);if(!completer){completer=new Completer(element);element.data(dataKey,completer)}completer.register(strategies);return $(completer)}return Textcomplete}]);

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	__webpack_require__(1);

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	compile.$inject = ["$compile"];
	modalSelect.$inject = ["$ionicModal", "$timeout", "$filter", "$parse", "$templateCache"];
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
	
	/*!
	 * Copyright 2015 Inmagik SRL.
	 * http://www.inmagik.com/
	 *
	 * ionic-modal-select, v1.3.2
	 * Modal select directive for Ionic framework.
	 *
	 * By @bianchimro
	 *
	 * Licensed under the MIT license. Please see LICENSE for more information.
	 *
	 */
	
	angular.module('ionic-modal-select', []).directive('compile', compile).directive('modalSelect', modalSelect);
	
	function compile($compile) {
		return function (scope, iElement, iAttrs) {
			var x = scope.$watch(function (scope) {
				// watch the 'compile' expression for changes
				return scope.$eval(iAttrs.compile);
			}, function (value) {
				// when the 'compile' expression changes
				// assign it into the current DOM
				iElement.html(value);
	
				// compile the new DOM and link it to the current
				// scope.
				// NOTE: we only compile .childNodes so that
				// we don't get into infinite loop compiling ourselves
				$compile(iElement.contents())(scope);
	
				//deactivate watch if "compile-once" is set to "true"
				if (iAttrs.compileOnce === 'true') {
					x();
				}
			});
		};
	}
	
	function modalSelect($ionicModal, $timeout, $filter, $parse, $templateCache) {
	
		var modalTemplateMultiple = __webpack_require__(2);
		var modalTemplate = __webpack_require__(3);
	
		return {
			restrict: 'A',
			require: 'ngModel',
			scope: {
				initialOptions: "=options",
				optionGetter: "&",
				searchFilters: "=searchFilters",
				searchProperties: '=',
				onSelect: "&",
				onSearch: "&",
				onReset: "&",
				onClose: "&"
			},
			link: function link(scope, iElement, iAttrs, ngModelController, transclude) {
	
				var shortList = true;
				var shortListBreak = iAttrs.shortListBreak ? parseInt(iAttrs.shortListBreak) : 10;
				var setFromProperty = iAttrs.optionProperty;
				var onOptionSelect = iAttrs.optionGetter;
				var clearSearchOnSelect = iAttrs.clearSearchOnSelect !== "false" ? true : false;
				var searchProperties = scope.searchProperties ? scope.searchProperties : false;
	
				//multiple values settings.
				var multiple = iAttrs.multiple ? true : false;
				if (multiple) {
					scope.isChecked = {};
				}
				var multipleNullValue = iAttrs.multipleNullValue ? scope.$eval(iAttrs.multipleNullValue) : [];
	
				scope.ui = {
					modalTitle: iAttrs.modalTitle || 'Select an option',
					okButton: iAttrs.okButton || 'OK',
					hideReset: iAttrs.hideReset !== "true" ? false : true,
					resetButton: iAttrs.resetButton || 'Reset',
					cancelButton: iAttrs.cancelButton || 'Cancel',
					loadListMessage: iAttrs.loadListMessage || 'Loading',
					modalClass: iAttrs.modalClass || '',
					headerFooterClass: iAttrs.headerFooterClass || 'bar-stable',
					value: null,
					selectedClass: iAttrs.selectedClass || 'option-selected',
					itemClass: iAttrs.itemClass || 'item item-text-wrap',
					searchTemplate: iAttrs.searchTemplate || (multiple ? modalTemplateMultiple : modalTemplate),
	
					//search stuff
					hasSearch: iAttrs.hasSearch !== "true" ? false : true,
					searchValue: '',
					searchPlaceholder: iAttrs.searchPlaceholder || 'Search',
					subHeaderClass: iAttrs.subHeaderClass || 'bar-stable',
					cancelSearchButton: iAttrs.cancelSearchButton || 'Clear'
	
				};
	
				var allOptions = [];
				scope.options = [];
	
				if (iAttrs.optionsExpression) {
					var optionsExpression = iAttrs.optionsExpression;
					var match = optionsExpression.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?\s*$/);
					if (!match) {
						throw new Error("collection-repeat expected expression in form of '_item_ in " + "_collection_[ track by _id_]' but got '" + iAttrs.optionsExpression + "'.");
					}
					//var keyExpr = match[1];
					var listExpr = match[2];
					var listGetter = $parse(listExpr);
					var s = iElement.scope();
	
					scope.$watch(function () {
						return listGetter(s);
					}, function (nv, ov) {
						initialOptionsSetup(nv);
						updateListMode();
					}, true);
				} else {
					scope.$watchCollection('initialOptions', function (nv) {
						initialOptionsSetup(nv);
						updateListMode();
					});
				}
	
				//#TODO: this is due to different single vs multiple template
				//but adds lots of complexity here and in search
				function initialOptionsSetup(nv) {
					nv = nv || [];
					if (!multiple) {
						allOptions = angular.copy(nv);
						scope.options = angular.copy(nv);
					} else {
						allOptions = nv.map(function (item, idx) {
							return [idx, angular.copy(item)];
						});
						scope.options = angular.copy(allOptions);
					}
				}
	
				// getting options template
				var opt = iElement[0].querySelector('.option');
				if (!opt) {
					throw new Error({
						name: 'modalSelectError:noOptionTemplate',
						message: 'When using modalSelect directive you must include an element with class "option"\n\t\t\t\t\t\t to provide a template for your select options.',
						toString: function toString() {
							return this.name + " " + this.message;
						}
					});
				}
				scope.inner = angular.element(opt).html();
	
				//add support for .remove for older devices
				if (!('remove' in Element.prototype)) {
					Element.prototype.remove = function () {
						this.parentNode.removeChild(this);
					};
				}
	
				angular.element(opt).remove();
	
				var notFound = iElement[0].querySelector('.not-found');
				if (notFound) {
					scope.notFound = angular.element(notFound).html();
					angular.element(notFound).remove();
				}
	
				function updateListMode() {
					//shortList controls wether using ng-repeat instead of collection-repeat
					if (iAttrs.useCollectionRepeat === "true") {
						shortList = false;
					} else if (iAttrs.useCollectionRepeat === "false") {
						shortList = true;
					} else {
						if (typeof scope.options !== "undefined") {
							shortList = !!(scope.options.length < shortListBreak);
						}
					}
	
					scope.ui.shortList = shortList;
				}
	
				ngModelController.$render = function () {
					scope.ui.value = ngModelController.$viewValue;
				};
	
				var getSelectedValue = scope.getSelectedValue = function (option) {
					var val = null;
					if (option === null || option === undefined) {
						return option;
					}
					if (onOptionSelect) {
						return scope.optionGetter({ option: option });
					}
					if (setFromProperty) {
						val = option[setFromProperty];
					} else {
						val = option;
					}
					return val;
				};
	
				scope.setOption = function (option) {
					var oldValue = ngModelController.$viewValue;
					var val = getSelectedValue(option);
					ngModelController.$setViewValue(val);
					ngModelController.$render();
	
					if (scope.onSelect) {
						scope.onSelect({ newValue: val, oldValue: oldValue });
					}
					scope.modal.hide().then(function () {
						scope.showList = false;
						if (scope.ui.hasSearch) {
							if (clearSearchOnSelect) {
								scope.ui.searchValue = '';
							}
						}
					});
				};
	
				// Filter object {id: <filterId>, active: <boolean>}
				// Used as auxiliary query params when querying server for search results
				scope.setFilter = function (filterId) {
					angular.forEach(scope.searchFilters, function (filter) {
						if (filter.id == filterId) {
							filter.active = !filter.active;
						} else {
							filter.active = false;
						}
					});
	
					// Trigger another search when the search filters change
					if (scope.onSearch) {
						scope.onSearch({ query: scope.ui.searchValue });
					}
				};
	
				scope.unsetValue = function () {
					$timeout(function () {
						ngModelController.$setViewValue("");
						ngModelController.$render();
						scope.modal.hide();
						scope.showList = false;
						if (scope.onReset && angular.isFunction(scope.onReset)) {
							scope.onReset();
						}
					});
				};
	
				scope.setValues = function () {
					var checkedItems = [];
					angular.forEach(scope.isChecked, function (v, k) {
						if (v) {
							checkedItems.push(allOptions[k][1]);
						}
					});
					var oldValues = ngModelController.$viewValue;
					var vals = checkedItems.map(function (item) {
						return getSelectedValue(item);
					});
					ngModelController.$setViewValue(vals);
					ngModelController.$render();
	
					if (scope.onSelect) {
						scope.onSelect({ newValue: vals, oldValue: oldValues });
					}
					scope.modal.hide().then(function () {
						scope.showList = false;
						if (scope.ui.hasSearch) {
							if (clearSearchOnSelect) {
								scope.ui.searchValue = '';
							}
						}
					});
				};
	
				scope.unsetValues = function () {
					$timeout(function () {
						ngModelController.$setViewValue(multipleNullValue);
						ngModelController.$render();
						scope.isChecked = {};
						scope.modal.hide();
						scope.showList = false;
						if (scope.onReset && angular.isFunction(scope.onReset)) {
							scope.onReset();
						}
					});
				};
	
				scope.closeModal = function () {
					scope.modal.hide().then(function () {
						scope.showList = false;
					});
				};
	
				scope.compareValues = function (a, b) {
					return angular.equals(a, b);
				};
	
				//loading the modal
				var modalTpl = null;
				if (iAttrs.searchTemplate) {
					scope.modal = $ionicModal.fromTemplate($templateCache.get(iAttrs.searchTemplate), { scope: scope });
				} else {
					modalTpl = multiple ? modalTemplateMultiple : modalTemplate;
					scope.modal = $ionicModal.fromTemplate(modalTpl, { scope: scope });
				}
	
				var hiddenCb = null;
				scope.$on('$destroy', function () {
					if (hiddenCb) {
						hiddenCb();
						hiddenCb = null;
					}
					scope.modal.remove();
				});
	
				if (scope.onClose && angular.isFunction(scope.onClose)) {
					hiddenCb = scope.$on('modal.hidden', function () {
						scope.onClose();
					});
				}
	
				iElement.on('click', function () {
					if (shortList) {
						scope.showList = true;
						scope.modal.show();
					} else {
						scope.modal.show().then(function () {
							scope.showList = true;
							scope.ui.shortList = shortList;
						});
					}
				});
	
				//filter function
				if (scope.ui.hasSearch) {
					scope.$watch('ui.searchValue', function (nv) {
						var whatToSearch;
						if (!multiple) {
							whatToSearch = allOptions;
						} else {
							whatToSearch = allOptions.map(function (item) {
								return item[1];
							});
						}
	
						if (iAttrs.onSearch) {
							scope.onSearch({ query: nv });
						} else {
							var filteredOpts = $filter('filter')(whatToSearch, nv, function (actual, expected) {
								if (!actual) {
									// if actual is an empty string, empty object, null, or undefined
									return false;
								}
								if (searchProperties) {
									if ((typeof actual === 'undefined' ? 'undefined' : _typeof(actual)) == 'object') {
										for (var i = 0; i < searchProperties.length; i++) {
											if (actual[searchProperties[i]] && actual[searchProperties[i]].toLowerCase().indexOf(expected.toLowerCase()) >= 0) {
												return true;
											}
										}
									}
									return false;
								} else {
									if (actual.toString().toLowerCase().indexOf(expected.toLowerCase()) >= 0) {
										return true;
									}
								}
								return false;
							});
	
							var oldLen = scope.options.length;
							if (!multiple) {
								scope.options = filteredOpts;
							} else {
								//#TODO: lots of loops here!
								var newOpts = [];
								angular.forEach(filteredOpts, function (item) {
									var originalItem = allOptions.find(function (it) {
										return it[1] == item;
									});
									if (originalItem) {
										newOpts.push(originalItem);
									}
								});
								scope.options = newOpts;
							}
							if (oldLen != scope.options.length) {
								//#todo: should resize scroll or scroll up here
							}
						}
					});
					scope.clearSearch = function () {
						scope.ui.searchValue = '';
					};
				}
	
				scope.copyOpt = function (option) {
					return angular.copy(option);
				};
	
				//#TODO ?: WRAP INTO $timeout?
				ngModelController.$render();
			}
		};
	}

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = " <ion-modal-view class=\"ionic-select-modal\" ng-class=\"::ui.modalClass\">\n\n    <ion-header-bar ng-class=\"::ui.headerFooterClass\">\n      <h1 class=\"title\">{{::ui.modalTitle}}</h1>\n    </ion-header-bar>\n\n    <div class=\"bar bar-subheader item-input-inset\" ng-class=\"::ui.subHeaderClass\" ng-if=\"ui.hasSearch\">\n      <label class=\"item-input-wrapper\">\n        <i class=\"icon ion-ios-search placeholder-icon\"></i>\n        <input type=\"search\" placeholder=\"{{::ui.searchPlaceholder}}\" ng-model=\"ui.searchValue\">\n      </label>\n      <button type=\"button\" class=\"button button-clear\" ng-click=\"clearSearch()\">\n        {{ ui.cancelSearchButton }}\n      </button>\n    </div>\n\n    <ion-content class=\"has-header\" ng-class=\"{'has-subheader':ui.hasSearch}\">\n    <div class=\"text-center\" ng-if=\"!ui.shortList && !showList\" style=\"padding-top:40px;\">\n        <h4 class=\"muted\">{{::ui.loadListMessage}}</h4>\n        <p>\n            <ion-spinner></ion-spinner>\n        </p>\n    </div>\n\n    <div ng-if=\"showList\">\n        <!--collection-repeat mode -->\n        <!-- not working right now -->\n        <!--\n        <div ng-if=\"!ui.shortList\" >\n            <div class=\"list\" class=\"animate-if\" >\n                <div\n                    class=\"item item-checkbox\" ng-class=\"ui.itemClass\"\n                     collection-repeat=\"optionm in options track by $index\">\n                    <label class=\"checkbox\">\n                        <input type=\"checkbox\" ng-model=\"isChecked[optionm[0]]\">\n                    </label>\n\n                    <div compile=\"inner\" ng-init=\"option=optionm[1]\" compile-once=\"false\"></div>\n\n                </div>\n            </div>\n        </div>\n        -->\n\n        <!-- ng-repeat mode -->\n        <div ng-if=\"ui.shortList || true\">\n            <div class=\"list\">\n                <div\n                  class=\"item item-checkbox\" ng-class=\"ui.itemClass\"\n                  ng-repeat=\"optionm in options track by optionm[0]\">\n                    <label class=\"checkbox\">\n                        <input type=\"checkbox\" ng-model=\"isChecked[optionm[0]]\">\n                    </label>\n                    <div ng-init=\"option=optionm[1]\" compile=\"inner\" compile-once=\"true\"></div>\n                </div>\n            </div>\n        </div>\n    </div>\n    </ion-content>\n    <ion-footer-bar ng-class=\"::ui.headerFooterClass\">\n        <button class=\"button button-stable\" ng-click=\"closeModal()\">{{ui.cancelButton}}</button>\n        <div class=\"title\" style=\"padding-top:6px\">\n            <button class=\"button button-navbar\" ng-click=\"setValues()\">OK</button>\n        </div>\n        <button ng-if=\"::!ui.hideReset\" class=\"button button-stable\" ng-click=\"unsetValues()\">{{ui.resetButton}}</button>\n    </ion-footer-bar>\n</ion-modal-view>\n"

/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = " <ion-modal-view class=\"ionic-select-modal\" ng-class=\"::ui.modalClass\">\n\n    <ion-header-bar ng-class=\"::ui.headerFooterClass\">\n      <h1 class=\"title\">{{::ui.modalTitle}}</h1>\n    </ion-header-bar>\n\n    <div class=\"bar bar-subheader item-input-inset\" ng-class=\"::ui.subHeaderClass\" ng-if=\"ui.hasSearch\">\n      <label class=\"item-input-wrapper\">\n        <i class=\"icon ion-ios-search placeholder-icon\"></i>\n        <input type=\"search\" placeholder=\"{{::ui.searchPlaceholder}}\" ng-model=\"ui.searchValue\">\n      </label>\n      <button type=\"button\" class=\"button button-clear\" ng-click=\"clearSearch()\">\n        {{ ui.cancelSearchButton }}\n      </button>\n    </div>\n\n    <ion-content class=\"has-header\" ng-class=\"{'has-subheader':ui.hasSearch}\">\n        <div class=\"text-center\" ng-if=\"!ui.shortList && !showList\" style=\"padding-top:40px;\">\n            <h4 class=\"muted\">{{::ui.loadListMessage}}</h4>\n            <p>\n                <ion-spinner></ion-spinner>\n            </p>\n        </div>\n        <div ng-if=\"showList\">\n            <div ng-if=\"!ui.shortList\">\n                <div class=\"list\" ng-if=\"showList\" class=\"animate-if\">\n                    <div\n                      ng-class=\"{ '{{::ui.itemClass}}' : true, '{{::ui.selectedClass}}': compareValues(getSelectedValue(option), ui.value) }\"\n                      collection-repeat=\"option in options track by $index\"\n                      ng-click=\"setOption(option)\"\n                      ng-class=\"{'{{::ui.selectedClass}}': compareValues(getSelectedValue(option), ui.value) }\">\n                        <div compile=\"inner\" compile-once=\"true\"></div>\n                    </div>\n                </div>\n            </div>\n            <div ng-if=\"ui.shortList\">\n                <div class=\"list\">\n                    <div\n                      ng-repeat=\"option in options track by $index\"\n                      ng-click=\"setOption(option)\"\n                      ng-class=\"{ '{{::ui.itemClass}}' : true, '{{::ui.selectedClass}}': compareValues(getSelectedValue(option), ui.value) }\">\n                        <div compile=\"inner\" compile-once=\"true\"></div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div ng-if=\"notFound && options.length == 0\">\n            <div compile=\"notFound\" compile-once=\"true\" ng-click=\"closeModal()\"></div>\n        </div>\n\n    </ion-content>\n\n    <ion-footer-bar ng-class=\"::ui.headerFooterClass\">\n        <button type=\"button\" class=\"button button-stable modal-select-close-button\" ng-click=\"closeModal()\">{{ui.cancelButton}}</button>\n        <button type=\"button\" ng-if=\"::!ui.hideReset\" class=\"button button-stable\" ng-click=\"unsetValue()\">{{ui.resetButton}}</button>\n    </ion-footer-bar>\n\n</ion-modal-view>\n"

/***/ }
/******/ ]);
//# sourceMappingURL=ionic-modal-select.js.map
!function(){"use strict";angular.module("ngMask",[])}(),function(){"use strict";angular.module("ngMask").directive("mask",["$log","$timeout","MaskService",function(a,b,c){return{restrict:"A",require:"ngModel",compile:function(d,e){function f(a){"number"==typeof a&&(b.cancel(g),g=b(function(){var b=a+1,c=d[0];if(c.setSelectionRange)c.focus(),c.setSelectionRange(a,b);else if(c.createTextRange){var e=c.createTextRange();e.collapse(!0),e.moveEnd("character",b),e.moveStart("character",a),e.select()}}))}if(!e.mask||!e.ngModel)return void a.info("Mask and ng-model attributes are required!");var g,h,i=c.create();return{pre:function(a,b,c){h=i.generateRegex({mask:c.mask,repeat:c.repeat||c.maskRepeat,clean:"true"===(c.clean||c.maskClean),limit:"true"===(c.limit||c.maskLimit||"true"),restrict:c.restrict||c.maskRestrict||"select",validate:"true"===(c.validate||c.maskValidate||"true"),model:c.ngModel,value:c.ngValue})},post:function(c,d,e,g){h.then(function(){function h(b){var c=b;b=b||"";var d=i.getViewValue(b),e=k.maskWithoutOptionals||"",h=d.withDivisors(!0),j=d.withoutDivisors(!0);try{var l=i.getRegex(h.length-1),m=i.getRegex(e.length-1),n=l.test(h)||m.test(h),o=b.length-h.length===1,p=e.length-h.length>0;if("accept"!==k.restrict)if("select"!==k.restrict||n&&!o)"reject"!==k.restrict||n||(d=i.removeWrongPositions(h),h=d.withDivisors(!0),j=d.withoutDivisors(!0));else{var q=b[b.length-1],r=h[h.length-1];q!==r&&p&&(h+=q);var s=i.getFirstWrongPosition(h);angular.isDefined(s)&&f(s)}k.limit||(h=d.withDivisors(!1),j=d.withoutDivisors(!1)),k.validate&&g.$dirty&&(m.test(h)||g.$isEmpty(c)?g.$setValidity("mask",!0):g.$setValidity("mask",!1)),b!==h&&(g.$setViewValue(angular.copy(h),"input"),g.$render())}catch(t){throw a.error("[mask - parseViewValue]"),t}return k.clean?j:h}var j,k=i.getOptions();g.$parsers.push(h),d.on("click input paste keyup",function(){j=b(function(){b.cancel(j),h(d.val()),c.$apply()},100)});var l=c.$watch(e.ngModel,function(a){angular.isDefined(a)&&(h(a),l())});k.value&&c.$evalAsync(function(){g.$setViewValue(angular.copy(k.value),"input"),g.$render()})})}}}}}])}(),function(){"use strict";angular.module("ngMask").factory("MaskService",["$q","OptionalService","UtilService",function(a,b,c){function d(){function d(a,b){var c;try{var d=t[a],e=C[d],f=h(a);e?c="("+e.source+")":(i(a)||(z.push(a),A[a]=d),c="(\\"+d+")")}catch(g){throw g}return(f||b)&&(c+="?"),new RegExp(c)}function e(a,b){var c,f;try{var g=d(a,b);c=g;var i=h(a),j=g.source;if(i&&u>a+1){var k=e(a+1,!0).elementOptionalRegex();j+=k.source}f=new RegExp(j)}catch(l){throw l}return{elementRegex:function(){return c},elementOptionalRegex:function(){return f}}}function f(c){var d=a.defer();s=c;try{var f=c.mask,g=c.repeat;if(!f)return;g&&(f=Array(parseInt(g)+1).join(f)),w=b.getOptionals(f).fromMaskWithoutOptionals(),s.maskWithoutOptionals=t=b.removeOptionals(f),u=t.length;for(var h,i=0;u>i;i++){var l=e(i),m=l.elementRegex(),n=l.elementOptionalRegex(),o=h?h.source+n.source:n.source;o=new RegExp(o),h=h?h.source+m.source:m.source,h=new RegExp(h),B.push(o)}j(),v=k(t).length,d.resolve({options:s,divisors:z,divisorElements:A,optionalIndexes:w,optionalDivisors:x,optionalDivisorsCombinations:y})}catch(p){throw d.reject(p),p}return d.promise}function g(a){var b;try{b=B[a]?B[a].source:""}catch(c){throw c}return new RegExp("^"+b+"$")}function h(a){return c.inArray(a,w)}function i(a){return c.inArray(a,z)}function j(){function a(a,b){return a-b}for(var b=z.sort(a),c=w.sort(a),d=0;d<b.length;d++)for(var e=b[d],f=1;f<=c.length;f++){var g=c[f-1];if(g>=e)break;x[e]=x[e]?x[e].concat(e-f):[e-f],A[e-f]=A[e]}}function k(a){a=a.toString();try{if(z.length>0&&a){for(var b=Object.keys(A),d=[],e=b.length-1;e>=0;e--){var f=A[b[e]];f&&d.push(f)}d=c.uniqueArray(d);var g=new RegExp("[\\"+d.join("\\")+"]","g");return a.replace(g,"")}return a}catch(h){throw h}}function l(a,b){function d(a,b){for(var c=b,d=0;d<a.length;d++){var e=a[d];e<c.length&&c.splice(e,0,A[e])}return c}var e=a,f=z.filter(function(a){var d=Object.keys(x).map(function(a){return parseInt(a)});return!c.inArray(a,b)&&!c.inArray(a,d)});return angular.isArray(a)&&angular.isArray(b)?(e=d(f,e),e=d(b,e)):e}function m(a){var b=a.split(""),d=!0;if(w.length>0){for(var e=[],f=Object.keys(x),h=0;h<f.length;h++){var i=x[f[h]];e.push(i)}0===y.length&&c.lazyProduct(e,function(){y.push(Array.prototype.slice.call(arguments))});for(var h=y.length-1;h>=0;h--){var j=angular.copy(b);j=l(j,y[h]);var k=j.join(""),m=g(t.length-1);if(m.test(k)){d=!1,b=j;break}}}return d&&(b=l(b,z)),b.join("")}function n(){return s}function o(a){try{var b=k(a),c=m(b);return{withDivisors:function(a){return a?c.substr(0,u):c},withoutDivisors:function(a){return a?b.substr(0,v):b}}}catch(d){throw d}}function p(a,b){var c=[];if(!a)return 0;for(var d=0;d<a.length;d++){var e=g(d),f=a.substr(0,d+1);if(e&&!e.test(f)&&(c.push(d),b))break}return c}function q(a){return p(a,!0)[0]}function r(a){for(var b=p(a,!1),c=a,d=0;d<b.length;d++){var e=b[d],f=a.split("");f.splice(e,1),c=f.join("")}return o(c)}var s,t,u=0,v=0,w=[],x={},y=[],z=[],A={},B=[],C={9:/[0-9]/,8:/[0-8]/,7:/[0-7]/,6:/[0-6]/,5:/[0-5]/,4:/[0-4]/,3:/[0-3]/,2:/[0-2]/,1:/[0-1]/,0:/[0]/,"*":/./,w:/\w/,W:/\W/,d:/\d/,D:/\D/,s:/\s/,S:/\S/,b:/\b/,A:/[A-Z]/,a:/[a-z]/,Z:/[A-ZÇÀÁÂÃÈÉÊẼÌÍÎĨÒÓÔÕÙÚÛŨ]/,z:/[a-zçáàãâéèêẽíìĩîóòôõúùũüû]/,"@":/[a-zA-Z]/,"#":/[a-zA-ZçáàãâéèêẽíìĩîóòôõúùũüûÇÀÁÂÃÈÉÊẼÌÍÎĨÒÓÔÕÙÚÛŨ]/,"%":/[0-9a-zA-ZçáàãâéèêẽíìĩîóòôõúùũüûÇÀÁÂÃÈÉÊẼÌÍÎĨÒÓÔÕÙÚÛŨ]/};return{getViewValue:o,generateRegex:f,getRegex:g,getOptions:n,removeDivisors:k,getFirstWrongPosition:q,removeWrongPositions:r}}return{create:d}}])}(),function(){"use strict";angular.module("ngMask").factory("OptionalService",[function(){function a(a){var c=[];try{for(var d=/\?/g,e=[];null!=(e=d.exec(a));)c.push(e.index-1)}catch(f){throw f}return{fromMask:function(){return c},fromMaskWithoutOptionals:function(){return b(c)}}}function b(a){for(var b=[],c=0;c<a.length;c++)b.push(a[c]-c);return b}function c(a){var b;try{b=a.replace(/\?/g,"")}catch(c){throw c}return b}return{removeOptionals:c,getOptionals:a}}])}(),function(){"use strict";angular.module("ngMask").factory("UtilService",[function(){function a(a,b,c){function d(h){var i=a[h],j=g[h];if(h===f)for(var k=0;j>k;++k)e[h]=i[k],b.apply(c,e);else for(var k=0;j>k;++k)e[h]=i[k],d(h+1);e.pop()}c||(c=this);for(var e=[],f=a.length-1,g=[],h=a.length;h--;)g[h]=a[h].length;d(0)}function b(a,b){var c;try{c=b.indexOf(a)>-1}catch(d){throw d}return c}function c(a){for(var b={},c=[],d=0,e=a.length;e>d;++d)b.hasOwnProperty(a[d])||(c.push(a[d]),b[a[d]]=1);return c}return{lazyProduct:a,inArray:b,uniqueArray:c}}])}();
//# sourceMappingURL=ngMask.min.map
/*
 *  jquery-maskmoney - v3.0.2
 *  jQuery plugin to mask data entry in the input text in the form of money (currency)
 *  https://github.com/plentz/jquery-maskmoney
 *
 *  Made by Diego Plentz
 *  Under MIT License (https://raw.github.com/plentz/jquery-maskmoney/master/LICENSE)
 */
!function($){"use strict";$.browser||($.browser={},$.browser.mozilla=/mozilla/.test(navigator.userAgent.toLowerCase())&&!/webkit/.test(navigator.userAgent.toLowerCase()),$.browser.webkit=/webkit/.test(navigator.userAgent.toLowerCase()),$.browser.opera=/opera/.test(navigator.userAgent.toLowerCase()),$.browser.msie=/msie/.test(navigator.userAgent.toLowerCase()));var a={destroy:function(){return $(this).unbind(".maskMoney"),$.browser.msie&&(this.onpaste=null),this},mask:function(a){return this.each(function(){var b,c=$(this);return"number"==typeof a&&(c.trigger("mask"),b=$(c.val().split(/\D/)).last()[0].length,a=a.toFixed(b),c.val(a)),c.trigger("mask")})},unmasked:function(){return this.map(function(){var a,b=$(this).val()||"0",c=-1!==b.indexOf("-");return $(b.split(/\D/).reverse()).each(function(b,c){return c?(a=c,!1):void 0}),b=b.replace(/\D/g,""),b=b.replace(new RegExp(a+"$"),"."+a),c&&(b="-"+b),parseFloat(b)})},init:function(a){return a=$.extend({prefix:"",suffix:"",affixesStay:!0,thousands:",",decimal:".",precision:2,allowZero:!1,allowNegative:!1},a),this.each(function(){function b(){var a,b,c,d,e,f=s.get(0),g=0,h=0;return"number"==typeof f.selectionStart&&"number"==typeof f.selectionEnd?(g=f.selectionStart,h=f.selectionEnd):(b=document.selection.createRange(),b&&b.parentElement()===f&&(d=f.value.length,a=f.value.replace(/\r\n/g,"\n"),c=f.createTextRange(),c.moveToBookmark(b.getBookmark()),e=f.createTextRange(),e.collapse(!1),c.compareEndPoints("StartToEnd",e)>-1?g=h=d:(g=-c.moveStart("character",-d),g+=a.slice(0,g).split("\n").length-1,c.compareEndPoints("EndToEnd",e)>-1?h=d:(h=-c.moveEnd("character",-d),h+=a.slice(0,h).split("\n").length-1)))),{start:g,end:h}}function c(){var a=!(s.val().length>=s.attr("maxlength")&&s.attr("maxlength")>=0),c=b(),d=c.start,e=c.end,f=c.start!==c.end&&s.val().substring(d,e).match(/\d/)?!0:!1,g="0"===s.val().substring(0,1);return a||f||g}function d(a){s.each(function(b,c){if(c.setSelectionRange)c.focus(),c.setSelectionRange(a,a);else if(c.createTextRange){var d=c.createTextRange();d.collapse(!0),d.moveEnd("character",a),d.moveStart("character",a),d.select()}})}function e(b){var c="";return b.indexOf("-")>-1&&(b=b.replace("-",""),c="-"),c+a.prefix+b+a.suffix}function f(b){var c,d,f,g=b.indexOf("-")>-1&&a.allowNegative?"-":"",h=b.replace(/[^0-9]/g,""),i=h.slice(0,h.length-a.precision);return i=i.replace(/^0*/g,""),i=i.replace(/\B(?=(\d{3})+(?!\d))/g,a.thousands),""===i&&(i="0"),c=g+i,a.precision>0&&(d=h.slice(h.length-a.precision),f=new Array(a.precision+1-d.length).join(0),c+=a.decimal+f+d),e(c)}function g(a){var b,c=s.val().length;s.val(f(s.val())),b=s.val().length,a-=c-b,d(a)}function h(){var a=s.val();s.val(f(a))}function i(){var b=s.val();return a.allowNegative?""!==b&&"-"===b.charAt(0)?b.replace("-",""):"-"+b:b}function j(a){a.preventDefault?a.preventDefault():a.returnValue=!1}function k(a){a=a||window.event;var d,e,f,h,k,l=a.which||a.charCode||a.keyCode;return void 0===l?!1:48>l||l>57?45===l?(s.val(i()),!1):43===l?(s.val(s.val().replace("-","")),!1):13===l||9===l?!0:!$.browser.mozilla||37!==l&&39!==l||0!==a.charCode?(j(a),!0):!0:c()?(j(a),d=String.fromCharCode(l),e=b(),f=e.start,h=e.end,k=s.val(),s.val(k.substring(0,f)+d+k.substring(h,k.length)),g(f+1),!1):!1}function l(c){c=c||window.event;var d,e,f,h,i,k=c.which||c.charCode||c.keyCode;return void 0===k?!1:(d=b(),e=d.start,f=d.end,8===k||46===k||63272===k?(j(c),h=s.val(),e===f&&(8===k?""===a.suffix?e-=1:(i=h.split("").reverse().join("").search(/\d/),e=h.length-i-1,f=e+1):f+=1),s.val(h.substring(0,e)+h.substring(f,h.length)),g(e),!1):9===k?!0:!0)}function m(){r=s.val(),h();var a,b=s.get(0);b.createTextRange&&(a=b.createTextRange(),a.collapse(!1),a.select())}function n(){setTimeout(function(){h()},0)}function o(){var b=parseFloat("0")/Math.pow(10,a.precision);return b.toFixed(a.precision).replace(new RegExp("\\.","g"),a.decimal)}function p(b){if($.browser.msie&&k(b),""===s.val()||s.val()===e(o()))a.allowZero?a.affixesStay?s.val(e(o())):s.val(o()):s.val("");else if(!a.affixesStay){var c=s.val().replace(a.prefix,"").replace(a.suffix,"");s.val(c)}s.val()!==r&&s.change()}function q(){var a,b=s.get(0);b.setSelectionRange?(a=s.val().length,b.setSelectionRange(a,a)):s.val(s.val())}var r,s=$(this);a=$.extend(a,s.data()),s.unbind(".maskMoney"),s.bind("keypress.maskMoney",k),s.bind("keydown.maskMoney",l),s.bind("blur.maskMoney",p),s.bind("focus.maskMoney",m),s.bind("click.maskMoney",q),s.bind("cut.maskMoney",n),s.bind("paste.maskMoney",n),s.bind("mask.maskMoney",h)})}};$.fn.maskMoney=function(b){return a[b]?a[b].apply(this,Array.prototype.slice.call(arguments,1)):"object"!=typeof b&&b?($.error("Method "+b+" does not exist on jQuery.maskMoney"),void 0):a.init.apply(this,arguments)}}(window.jQuery||window.Zepto);