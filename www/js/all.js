
angular.module(
        'fortinsocial',   
        [
            'ionic',
            'fortinsocial.controllers',
            'fortinsocial.config',
            'fortinsocial.services',
            'ngMessages',
            'ion-gallery',
            'ngCordova',
            'fortinsocial.filters',
            'fortinsocial.directives',
            'angular-md5',
            'ngCookies',
            'pascalprecht.translate',
            'ngAnimate',
            'ngTextcomplete',
            'ionic-modal-select',
            'ngMask',
            'pdf-viewer',
        ]
    )
    .run(function ($ionicPlatform,$http, $translate, $location, $state, $rootScope,$window,$ionicHistory) {
            $ionicPlatform.ready(function () {

                $rootScope.platformName = 'browser';
                if($ionicPlatform.is('android'))
                {
                    $rootScope.platformName = 'android';
                }

                if($ionicPlatform.is('ios'))
                {
                    $rootScope.platformName = 'ios';
                }


                $ionicPlatform.registerBackButtonAction(function(e){
                    if($location.path()=='/tab/feeds'){
                        $('.fancybox-button--close').click();
                        e.preventDefault();
                    }else{
                        $ionicHistory.goBack();
                    }
                }, 100);

                var pushid = "f86625e9-5a69-4157-b696-640fa6dfdc62"; 
                var googleprojectno = "refriplay-167920"; 

                if(window.plugins && window.plugins.OneSignal)
                {

                    var notificationOpenedCallback = function (jsonData) {
                        var redirectTo = jsonData.notification.payload.additionalData.redirectTo;
                        $rootScope.$broadcast( 'app:notification', jsonData.notification.payload.additionalData );
                    };

                    window.plugins.OneSignal
                        .startInit(pushid)
                        .handleNotificationOpened(notificationOpenedCallback)
                        .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
                        .endInit();

                    window.plugins.OneSignal.getIds(function (ids) {
                        localStorage.setItem("playerid", ids.userId);

                    });


                }



                if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                    cordova.plugins.Keyboard.disableScroll(true);

                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }

                if (window.ga) {
                    window.ga.startTrackerWithId('UA-106020434-1'); 
                    window.ga.trackView('Iniciou o app');
                    if($window.localStorage['profile'])
                    {
                        var profiledata = JSON.parse($window.localStorage['profile']);
                        window.ga.startTrackerWithId(profiledata.userid); 
                    }

                }




            });


            document.addEventListener("deviceready", function(){

                navigator.globalization.getPreferredLanguage(function (language) {
                    var lang = language.value.substr(0, 2);
                    $translate.use(lang);
                });

            }, false);


        })

        .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, ionGalleryConfigProvider,$httpProvider, $translateProvider,$sceDelegateProvider) {
            $ionicConfigProvider.tabs.position('bottom');

            $ionicConfigProvider.backButton.previousTitleText(false);
            $ionicConfigProvider.backButton.icon('ion-chevron-left');
            $ionicConfigProvider.backButton.text('');




            $translateProvider.useMessageFormatInterpolation();
            $translateProvider.preferredLanguage('pt');
            $translateProvider.useStaticFilesLoader({
                prefix: 'languages/',
                suffix: '.json'
            });


            $stateProvider


                    .state('landing', {
                        url: '/landing',
                        templateUrl: 'templates/landing.html',
                        controller: 'LandingCtrl'
                    })

                    .state('tab', {
                        cache:false,
                        url: '/tab',
                        abstract: true,
                        templateUrl: 'templates/tabs.html',
                        controller: 'TabsCtrl',
                    })

                    .state('tab.feeds', {
                        url: '/feeds',
                        views: {
                            'tab-feeds': {
                                templateUrl: 'templates/tab-feeds.html',
                                controller: 'FeedsCtrl',

                            }
                        },
                        params: {
                                    hashtagid: 0,
                                    hashtag: null
                                }
                    })
                    .state('tab.feeds-brasil', {
                        url: '/feeds/{brasil}',
                        views: {
                            'tab-feeds': {
                                templateUrl: 'templates/tab-feeds.html',
                                controller: 'FeedsCtrl',

                            }
                        },
                        params: {
                                    hashtagid: 0,
                                    hashtag: null
                                }
                    })

                    .state('tab.newpost', {
                        url: '/newpost',
                        views: {
                            'tab-newpost': {
                                templateUrl: 'templates/tab-newpost.html',
                                controller: 'NewpostCtrl'
                            }
                        }
                    })

                    .state('post', {
                        url: '/post/{postId:int}',
                        templateUrl: 'templates/post.html',
                        controller: 'FeedsCtrl'
                    })

                    .state('tab.friends', {
                        url: '/friends',
                        views: {
                            'tab-friends': {
                                templateUrl: 'templates/tab-friends.html',
                                controller: 'FriendsCtrl'
                            }
                        }
                    })
                    .state('tab.profile', {
                        url: '/profile',
                        views: {
                            'tab-profile': {
                                templateUrl: 'templates/tab-profile.html',
                                controller: 'ProfileCtrl'
                            }
                        }
                    })
                    .state('tab.chat-detail', {
                        url: '/chats/:chatId',
                        views: {
                            'tab-chats': {
                                templateUrl: 'templates/chat-detail.html',
                                controller: 'ChatDetailCtrl'
                            }
                        }
                    })

                    .state('tab.account', {
                        url: '/account',
                        views: {
                            'tab-account': {
                                templateUrl: 'templates/tab-account.html',
                                controller: 'AccountCtrl'
                            }
                        }
                    })

                    .state('tab.notifications', {
                        url: '/notifications',
                        views: {
                            'tab-notifications' : {
                                templateUrl: 'templates/tab-notifications.html',
                                controller: 'NotificationsCtrl'
                            }
                        }
                    })

                    .state('tab.account-edit', {
                        url: '/account/edit/{field}',
                        views: {
                            'tab-account': {
                                templateUrl: 'templates/tab-account-edit.html',
                                controller: 'AccountCtrl'
                            }
                        }
                    })

                    .state('tab.account-modal', {
                        url: '/account/modal',
                        views: {
                            'tab-account': {
                                templateUrl: 'templates/tab-account-modal.html',
                                controller: 'AccountCtrl'
                            }
                        }
                    })


                    .state('tab.budget', {
                        url: '/budget',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-budget.html',
                                controller: 'BudgetCtrl'
                            }
                        }
                    })

                    .state('tab.produtos', {
                        url: '/produtos',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-produtos.html',
                                controller: 'ProdutosCtrl'
                            }
                        }
                    })

                                        .state('tab.produtos-new', {
                        url: '/produtos-new',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-produtos-form.html',
                                controller: 'ProdutosCtrl'
                            }
                        }
                    })

                    .state('tab.produtos-edit', {
                        url: '/produtos-edit/{produtoId}',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-produtos-form.html',
                                controller: 'ProdutosCtrl'
                            }
                        }
                    })


                    .state('tab.servicos', {
                        url: '/servicos',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-servicos.html',
                                controller: 'ServicosCtrl'
                            }
                        }
                    })

                                        .state('tab.servicos-new', {
                        url: '/servicos-new',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-servicos-form.html',
                                controller: 'ServicosCtrl'
                            }
                        }
                    })

                    .state('tab.servicos-edit', {
                        url: '/servicos-edit/{servicoId}',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-servicos-form.html',
                                controller: 'ServicosCtrl'
                            }
                        }
                    })

                    .state('tab.clientes', {
                        url: '/clients',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-clientes.html',
                                controller: 'ClientsCtrl'
                            }
                        }
                    })

                    .state('tab.clientes-new', {
                        url: '/clients-new',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-clientes-edit.html',
                                controller: 'ClientsCtrl'
                            }
                        }
                    })


                                        .state('tab.clientes-edit', {
                        url: '/clients-edit/{clientId}',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-clientes-edit.html',
                                controller: 'ClientsCtrl'
                            }
                        }
                    })

                    .state('tab.budget-new', {
                        url: '/budget-new',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-budget-new.html',
                                controller: 'NewBudgetCtrl'
                            }
                        }
                    })
                    .state('tab.budget-edit', {
                        url: '/budget-new/{budgetId}',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-budget-new.html',
                                controller: 'NewBudgetCtrl'
                            }
                        }
                    })
                    .state('tab.budget-review', {
                        url: '/budget-review',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-budget-review.html',
                                controller: 'NewBudgetCtrl'
                            }
                        }
                    })
                .state('tab.budget-review-edit', {
                    url: '/budget-review/{budgetId}',
                    views: {
                        'tab-tools': {
                            templateUrl: 'templates/tab-budget-budget-review.html',
                            controller: 'NewBudgetCtrl'
                        }
                    }
                })
                .state('tab.budget-pdf', {
                    url: '/budget-pdf/{budgetId}',
                    views: {
                        'tab-tools': {
                            templateUrl: 'templates/tab-budget-pdf.html',
                            controller: 'budgetPdfCtr'
                        }
                    }
                })

                    .state('tabauth', {
                        url: '/tabauth',
                        abstract: true,
                        templateUrl: 'templates/tabs-auth.html'
                    })
                    .state('login-intro', {
                        url: '/login-intro',
                        templateUrl: 'templates/login-intro.html',
                        controller: 'LoginCtrl'
                    })
                    .state('login', {
                        url: '/login',
                        templateUrl: 'templates/login.html',
                        controller: 'LoginCtrl'
                    })
                    .state('criar-conta', {
                        url: '/criar-conta',
                        templateUrl: 'templates/criar-conta.html',
                        controller: 'SignupCtrl'
                    })
                    .state('esqueceu-a-senha', {
                        url: '/esqueceu-a-senha',
                        templateUrl: 'templates/esqueceu-a-senha.html',
                        controller: 'SignupCtrl'
                    })
                    .state('senha-confirmacao', {
                        url: '/senha-confirmacao',
                        templateUrl: 'templates/senha-confirmacao.html',
                        controller: 'SignupCtrl'
                    })
                    .state('aguardando-aprovacao', {
                        url: '/aguardando-aprovacao',
                        templateUrl: 'templates/aguardando-aprovacao.html',
                        controller: 'SignupCtrl'
                    })
                    .state('conta-reprovada', {
                        url: '/conta-reprovada',
                        templateUrl: 'templates/conta-reprovada.html',
                        controller: 'SignupCtrl'
                    })
                    .state('photogallery', {
                        url: '/photogallery',
                        templateUrl: 'templates/photo-gallery.html',
                        controller: 'PhotoGalleryCtrl',
                        params: {
                            fromprofile: null,
                            userid:0
                        },
                    })
                    .state('profilepage', {
                        url: '/profilepage',
                        params: {
                            userid: 0,
                            currpage: null
                        },
                        templateUrl: 'templates/profile-page.html',
                        controller: 'ProfilePageCtrl'
                    })
                    .state('trendinghashtags', {
                        url: '/trendinghashtags',
                        templateUrl: 'templates/trendinghashtags.html',
                        controller: 'TrendinghashtagsCtrl'
                    })
                    .state('selecionar-estado', {
                        url: '/selecionar-estado',
                        templateUrl: 'templates/selecionar-estado.html',
                        controller: 'estadosCtrl'
                    })
                    .state('tab.tools', {
                        cache: false,
                        url: '/tools',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-tools.html',
                                controller: 'ToolsCtrl'
                            }
                        }
                    })
                    .state('tab.tools-category', {
                        cache: false,
                        url: '/tools/category',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-tools-category.html',
                                controller: 'ErrorCategoryCtrl'
                            }
                        }
                    })
                    .state('tab.tools-insurance', {
                        cache: false,
                        url: '/tools/insurance',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-insurance.html',
                                controller: 'InsuranceCtrl'
                            }
                        }
                    })
                    .state('tab.marks', {
                        cache: false,
                        url: '/marks',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/marks.html',
                                controller: 'MarksCtrl'
                            }
                        }
                    })
                    .state('tab.products-list', {
                        cache: false,
                        url: '/products/list/:mark_id',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/products-list.html',
                                controller: 'ProductsCtrl'
                            }
                        }
                    })
                    .state('tab.products-error-list', {
                        cache: false,
                        url: '/products/list/error/:products_id',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/products-error-list.html',
                                controller: 'ProductsCtrl'
                            }
                        }
                    })
                    .state('tab.products-error-detail', {
                        cache: false,
                        url: '/products/list/error/detail/:error_id',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/products-error-detail.html',
                                controller: 'ErrorCtrl'
                            }
                        }
                    })
                    ;
            ionGalleryConfigProvider.setGalleryConfig({
                action_label: 'Done',
                toggle: false,
                row_size: 3,
                fixed_row_size: false
            });

            $urlRouterProvider.otherwise('login-intro');

        });

angular.module('fortinsocial.config', [])
.constant('GOOGLE_KEY', 'AIzaSyBeHGG3PThk8aZu_nmERcCki_phXRT13Rk')
    .constant('S3_URL', 'https://s3.amazonaws.com/refriplay-dev/')
.constant('SERVER_URL', 'http://localhost/refriplayadmin/')   
;

angular.module('fortinsocial.directives', [])
.directive('fallbackSrc', function () {
            var fallbackSrc = {
                link: function postLink(scope, iElement, iAttrs) {
                    iElement.bind('error', function () {
                        angular.element(this).attr("src", iAttrs.fallbackSrc);
                    });
                }
            }
            return fallbackSrc;
})

    .directive(
        "fpFloatingLabelInput",
        [
            function ()
            {
                return {
                    restrict: "A",
                    require: "^ngModel",
                    link: function ( scope, elm, attrs, ctrl )
                    {
                        scope.$watch(
                            function ()
                            {
                                if(ctrl.$viewValue)
                                {
                                    return ctrl.$viewValue.trim();
                                }
                                else
                                {
                                    return ctrl.$viewValue;
                                }
                            },
                            function ( value )
                            {
                                if( value && value.length )
                                    angular.element( elm ).addClass( "fp-filled" );
                                else
                                    angular.element( elm ).removeClass( "fp-filled" );
                            }
                        );
                    }
                };
            }
        ]
    )

    .directive(
        "fpCompareTo",
        [
            function ()
            {
                return {
                    require: "ngModel",
                    scope: {
                        otherModelValue: "=fpCompareTo"
                    },
                    link: function(scope, element, attributes, ngModel) {
                        ngModel.$validators.fpCompareTo = function(modelValue) {
                            return modelValue == scope.otherModelValue;
                        };

                        scope.$watch("otherModelValue", function() {
                            ngModel.$validate();
                        });
                    }
                };
            }
        ]
    )
    .directive(
        "fpCompareToNew",
        [
            function ()
            {
                return {
                    require: "ngModel",
                    scope: {
                        otherModelValue: "=fpCompareTo"
                    },
                    link: function(scope, element, attributes, ngModel) {
                        ngModel.$validators.fpCompareTo = function(modelValue) {
                            return modelValue == scope.otherModelValue.$viewValue;
                        };

                        scope.$watch("otherModelValue", function() {
                            ngModel.$validate();
                        });
                    }
                };
            }
        ]
    )
.directive('swifthash', function($compile, $parse) {
    return {
      restrict: 'E',
      link: function(scope, element, attr) {
        scope.$watch(attr.content, function() {
          element.html($parse(attr.content)(scope));
          $compile(element.contents())(scope);
        }, true);
      }
    }
  }).directive('textcomplete', ['Textcomplete', function(Textcomplete) {

          return {
              restrict: 'EA',
              scope: {
                  members: '=',
                  message: '='
              },
              template: '<textarea ng-model=\'message\' type=\'text\'></textarea>',
              link: function(scope, iElement, iAttrs) {
                  var mentions = scope.members;
                  var ta = iElement.find('textarea');
                  var textcomplete = new Textcomplete(ta, [
                      {
                          match: /(^|\s)!(\w*)$/,
                          search: function(term, callback) {
                              callback($.map(mentions, function(mention) {
                                  return mention.toLowerCase().indexOf(term.toLowerCase()) === 0 ? mention : null;
                              }));
                          },
                          maxCount:3,
                          index: 2,
                          replace: function(mention) {
                              return '$1!' + mention + '';
                          }
                      }
                  ]);

                  $(textcomplete).on({
                      'textComplete:select': function (e, value) {
                          scope.$apply(function() {
                              scope.message = value
                          })
                      },
                      'textComplete:show': function (e) {
                          $(this).data('autocompleting', true);
                      },
                      'textComplete:hide': function (e) {
                          $(this).data('autocompleting', false);
                      }
                  });
              }
          }
      }]).filter("BR_money", function(){
    return function(n){
        if(n > 0)
        {
            return n.replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
        }
        else
        {
            return n;
        }
    }
}).directive("fpMaskMoney",function(){
    return function(scope,element,attr){
        return element.maskMoney({ decimal: ',', thousands: '.', precision: 2 ,prefix:'R$ ',allowZero:true},scope.info);
    }
})

.directive(
    "fpResizeOnChange",
    function ()
    {
        return {
            restrict: "A",
            link: function ( scope, elm )
            {
                elm.on( "keyup", function () {
                    window.setTimeout( function () {
                        elm[0].style.height = "auto";
                        elm[0].style.height = elm[0].scrollHeight + "px";
                    }, 1);
                });
            }
        }
    }
).directive('pdfonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {

            $(element).on('change',function(){
                alert('oi');
            })

        }
    };
});
;

angular.module('fortinsocial.filters', [])
.filter('randomSrc', function () {
    return function (input) {
        if (input)
            return input + '?r=' + Math.round(Math.random() * 999999);
    }
}).filter('trusted', ['$sce', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}]).filter('notification_img', function() {
    return function (data) {
        if(data == '' || data == undefined)
            return 'img/emptyavatar.jpg';
        else return data;
    }
}).filter('notification_type', function() {
    return function (type) {
        if(type == 'curtida')
            return 'curtiu a sua publicação';
        else if(type == 'comentario') {
            return 'também comentou sua publicação';
        }
    }
});
angular.module('fortinsocial.controllers', [])


        .run(function($rootScope,$http, $location, $timeout,$ionicPlatform) {
            $http.defaults.headers.common['Authorization'] = 'Basic cmVmcmlwbGF5c2VydmljZTpiZjlhNDIyZWUwMTgwZjI5ZWEzN2Q5NTFkNDkxNzdjZg==';
            $http.defaults.headers.get = {};

            $rootScope.$on('app:notification', function ( evt, data ) {
                if ( data.redirectTo )
                    $timeout(function(){
                        $location.path( data.redirectTo );
                    }, 1);
            });

            $rootScope.$on('$stateChangeSuccess', function(event, to, toParams, from, fromParams) {
                $rootScope.previousState = from;
            });

            $rootScope.analytics = function(title)
            {
                    if(window.ga)
                    {
                        window.ga.startTrackerWithId('UA-106020434-1');
                        window.ga.trackView(title);
                    }
            };

            $rootScope.isIOS = $ionicPlatform.is( 'ios' );

        })
        .controller('LandingCtrl', function ($scope, $state, $ionicPopup, $window, $ionicLoading) {

        })
        .controller('TabsCtrl', function($rootScope, $scope, Notifications, $window, $ionicTabsDelegate){
            $scope.profiledata = JSON.parse($window.localStorage['profile']);
            var userid = $scope.profiledata.userid;
            $rootScope.qntNotifications = 0;

            $rootScope.checkNotifications = function() {
                Notifications.getQntNotifications(userid).success(
                function(result) {
                    if(result.length > 0)
                        $rootScope.qntNotifications = result.length;

                }).error(function(error) {
                    console.log(error);
                });
            }

        })
        .controller('LoginCtrl', function ($scope, userauth,usersignupfb, $state, $ionicPopup, $window, $ionicLoading, updatepushid,$ionicNavBarDelegate,$ionicLoading,$rootScope,$ionicPlatform) {
            $ionicPlatform.ready(function() {
                $rootScope.analytics('Login');
            });


            $ionicLoading.hide();
            if ($window.localStorage['profile']) {
              var userdata = JSON.parse($window.localStorage['profile']);
                if (localStorage.getItem("playerid") && userdata.pushid.length < 1) {
                    var pushid = localStorage.getItem("playerid");

                    updatepushid.updatepushid(pushid, userdata.userid).success(function (successdata) {});
                }
              userauth.dologinSemMd5(userdata.email, userdata.password).success(function (successdata) {
                  $ionicLoading.hide();
                  if (successdata.message.trim().toString() === 'LoginSuccess') {
                      userdata = successdata.userdetails;
                      $window.localStorage['profile'] = JSON.stringify(userdata);
                  }
              });
                if(userdata.feed_view)
                {
                    $state.go('tab.feeds');
                }
                else
                {
                    $state.go('selecionar-estado');
                }
            }

            $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
                viewData.enableBack = true;
            });

            $scope.data = {
                'email': "",
                'password': "",
                'name': ""
            };

            $scope.facebookAuth = function()
            {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });

                facebookConnectPlugin.logout(function(){},function(){});

                var fbLoginSuccess = function (userData) {
                    console.log('login_success',userData);
                    var userId = userData['authResponse']['userID'];
                    OnGetFacebookInfo(userId);
                };

                facebookConnectPlugin.login(["email","public_profile"], fbLoginSuccess,
                    function loginError (error) {
                        console.log('get_face_data',error);
                        $ionicLoading.hide();
                    }
                );

                function OnGetFacebookInfo(fb_user_id)
                {
                   return facebookConnectPlugin.api(fb_user_id+"/?fields=id,name,email", ["public_profile"],
                        function (result) {
                            $ionicLoading.hide();
                            usersignupfb.signup(result['email'], fb_user_id, result['name']).success(function (successdata) {
                                if (successdata.message.trim().toString() === 'success' || successdata.message.trim().toString() === 'exists') {
                                    var userdata = successdata.userdetails;
                                    if (localStorage.getItem("playerid")) {
                                        var pushid = localStorage.getItem("playerid");

                                        updatepushid.updatepushid(pushid, userdata.userid).success(function (successdata) {});
                                    }
                                    if(userdata.flagged == 'yes')
                                    {
                                         $state.go('conta-reprovada');
                                    }
                                    else if (userdata.user_active != 'yes')
                                    {
                                         $state.go('aguardando-aprovacao');
                                    }
                                    else
                                    {
                                        $window.localStorage['profile'] = JSON.stringify(userdata);
                                        if(userdata.feed_view)
                                        {
                                            $state.go('tab.feeds');
                                        }
                                        else
                                        {
                                            $state.go('selecionar-estado');
                                        }
                                    }
                                } else {
                                    var alertPopup = $ionicPopup.alert({
                                        title: 'Refriplay',
                                        okType: 'button-balanced',
                                        template: 'Houve algum erro no cadastro'
                                    });
                                    return false;
                                }
                            });
                        },
                        function (error){
                            console.log('connect_face_api',error);
                            $ionicLoading.hide();
                            return error;
                        });

                }
            };

            $scope.login = function () {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });

                userauth.dologin($scope.data.email, $scope.data.password).success(function (successdata) {
                    $ionicLoading.hide();
                    if (successdata.message.trim().toString() === 'LoginSuccess') {
                        var userdata = successdata.userdetails;
                        $window.localStorage['profile'] = JSON.stringify(userdata);
                        $scope.profiledata = JSON.parse($window.localStorage['profile']);
                        var userid = $scope.profiledata.userid;
                        if (localStorage.getItem("playerid")) {
                            var pushid = localStorage.getItem("playerid");

                            updatepushid.updatepushid(pushid, userid).success(function (successdata) {
                                if(userdata.feed_view)
                                {
                                    $state.go('tab.feeds');
                                }
                                else
                                {
                                    $state.go('selecionar-estado');
                                }
                            });
                        } else {
                            if(userdata.feed_view)
                            {
                                $state.go('tab.feeds');
                            }
                            else
                            {
                                $state.go('selecionar-estado');
                            }
                        }

                    }
                    else if (successdata.message.trim().toString() == 'UserBan')
                    {
                        $state.go('conta-reprovada');
                        return false;
                    }
                    else if (successdata.message.trim().toString() == 'InactiveUser')
                    {
                        $state.go('aguardando-aprovacao');
                        return false;
                    }
                    else {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Refriplay',
                            okType: 'button-balanced',
                            template: 'Login ou Senha inválido'
                        });
                        return false;
                    }
                });
            };

        })
        .controller('SignupCtrl', function ($scope, $state, $ionicPopup, usersignup, $window, $ionicLoading,userpass,userChangePass,$rootScope,$ionicPlatform,updatepushid) {

            $ionicPlatform.ready(function() {
                $rootScope.analytics('Cadastro');
            });


            $ionicLoading.hide();
            $scope.data = {
                email: '',
                password: '',
                name: ''
            };

            $scope.signUp = function (form) {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });

                if (form.$valid) {

                    usersignup.signup($scope.data.email, $scope.data.password, $scope.data.name).success(function (successdata) {
                        $ionicLoading.hide();
                        if (successdata.message.trim().toString() === 'exists') {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Este email já existe, tente fazer login'
                            });
                            return false;
                        } else if (successdata.message.trim().toString() === 'success') {
                            var userdata = successdata.userdetails;
                            if (localStorage.getItem("playerid")) {
                                var pushid = localStorage.getItem("playerid");

                                updatepushid.updatepushid(pushid, userdata.userid).success(function (successdata) {});
                            }
                            if(userdata.flagged == 'yes')
                            {
                                $state.go('conta-reprovada');
                            }
                            else if (userdata.user_active == 'no')
                            {
                                $state.go('aguardando-aprovacao');
                            }
                            else
                            {
                                $window.localStorage['profile'] = JSON.stringify(userdata);
                                if(userdata.feed_view)
                                {
                                    $state.go('tab.feeds');
                                }
                                else
                                {
                                    $state.go('selecionar-estado');
                                }
                            }
                        } else {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Falha ao registrar'
                            });
                            return false;
                        }
                    });

                } else {
                    $ionicLoading.hide();
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Por favor preencha o formulário'
                    });
                }
            };

            $scope.forgotMyPass = function(form){
                if (form.$valid)
                {
                    $ionicLoading.show({
                        content: 'Loading',
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showDelay: 0
                    });
                    userpass.forgotPass($scope.data.email).success(function(data){
                        if(data.result == 1)
                        {
                            $state.go('senha-confirmacao');
                        }
                        else if(data.result == -1)
                        {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Email não existe',
                                okType: 'button-balanced',
                                template: 'Este email não está cadastrado!'
                            });
                        }
                        else
                        {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Erro ao enviar',
                                okType: 'button-balanced',
                                template: 'Tente novamente ou mande um email para suporte@refriplay.com.br'
                            });
                        }
                        $ionicLoading.hide();
                    });
                }
            };

            $scope.changePass = function(form)
            {
                if (form.$valid)
                {
                    $ionicLoading.show({
                        content: 'Loading',
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showDelay: 0
                    });
                    userChangePass.changePass($scope.data.code,$scope.data.password).success(function(data){
                        if(data.result == 1)
                        {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay Senha',
                                okType: 'button-balanced',
                                template: 'Senha alterada com sucesso!'
                            });
                            $state.go('login-intro');
                        }
                        else
                        {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay Senha',
                                okType: 'button-balanced',
                                template: 'A senha não foi alterada, seu código está inválido, tente gerar outro!'
                            });
                        }
                        $ionicLoading.hide();
                    });
                }
            };

        })

        .controller('FeedsCtrl', function ($scope, Comments, getuserfeeds, S3_URL, SERVER_URL, $ionicModal, addpostlike, addpostcomment, $window, $sce, $stateParams, $compile,$state,adv_report, $location, $rootScope, $timeout, $ionicSlideBoxDelegate,$ionicPlatform,deletepost,$ionicLoading,$ionicPopup,$document,setFeedState,Error,$ionicHistory,$ionicNavBarDelegate) {
            var postid = 0;
            if($stateParams.postId)
            {
                postid = $stateParams.postId;
                $scope.UPost = postid;
                $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
                    viewData.enableBack = true;
                });
            }

            $scope.profiledata = JSON.parse($window.localStorage['profile']);
            $scope.SERVER_URL = SERVER_URL;
            $scope.youtubeLink = '';

            $scope.trustSrc = function(youtubelink) {
                return $sce.trustAsResourceUrl(youtubelink);
              }

            $scope.$on('$ionicView.enter', function(e) {
                $ionicNavBarDelegate.showBar(true);
                    $rootScope.checkNotifications();
            });


            var userid = $scope.profiledata.userid;
            var username = $scope.profiledata.name;
            var profile_pic = $scope.profiledata.profile_pic;
            var page = 0;
            var hashtagid = $stateParams.hashtagid;
            $scope.feeds = [];
            $scope.noMoreItemsAvailable = false;
            $scope.advid = 0;

            $scope.photo_baseurl = SERVER_URL;

            $scope.currhashtag = $stateParams.hashtag;

            $scope.loadMore = function (type) {
                $rootScope.videoProgress = 0;
                if (type === 'iscroll')
                {
                    adv_report.count($scope.advid,userid, $scope.profiledata.feed_view).success(function (feedsdata) {});
                }

                if($scope.profiledata.feed_view == 'all'){

                    $scope.profiledata.feed_view = "Brasil";

                    getuserfeeds.getuserfeedsAll(userid, page).then(
                        function (feedsdata) {
                            var success = feedsdata.data.success;
                            $scope.advid = feedsdata.data.has_adv;

                            if (success === 1) {
                                if (type === 'iscroll') {
                                    $scope.feeds = $scope.feeds.concat(feedsdata.data.feeds);
                                } else {
                                    $scope.feeds = feedsdata.data.feeds;
                                }
                                page++;
                        }
                        else {
                            $scope.noMoreItemsAvailable = true;
                        }

                        $scope.$broadcast('scroll.infiniteScrollComplete');
                    });

                } else {

                    getuserfeeds.getuserfeeds(userid, page, hashtagid).then(function (feedsdata) {
                        var success = feedsdata.data.success;
                        $scope.advid = feedsdata.data.has_adv;
                        if (success === 1) {
                            if (type === 'iscroll') {
                                $scope.feeds = $scope.feeds.concat(feedsdata.data.feeds);
                            } else {
                                $scope.feeds = feedsdata.data.feeds;
                            }
                            page++;
                        }
                        else {
                            $scope.noMoreItemsAvailable = true;
                        }
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                    });
                }

            };

            $rootScope.videoProgress = 0;

            $scope.profileAdmin = 0;
            $scope.lastDeleteEleClick = -1;
            $document.on('click', function (event) {
                var isMoreOption = $(event.target).hasClass('ion-android-more-vertical');
                if($scope.lastDeleteEleClick >= 0 && $scope.feeds[$scope.lastDeleteEleClick].showExclude && !isMoreOption)
                {
                    $scope.feeds[$scope.lastDeleteEleClick].showExclude = false;
                    $scope.lastDeleteEleClick = -1;
                    $scope.$apply();
                }
            });

            $scope.finalScript = false;
            if($window.localStorage['profile'] === undefined)
            {
                $state.go('login-intro');
            }

            $scope.profiledata = JSON.parse($window.localStorage['profile']);
            $ionicPlatform.ready(function() {
                $rootScope.analytics('Publicações em ' + $scope.profiledata.feed_view);
            });


            if($scope.profiledata.flagged == 'yes')
            {
                $state.go('conta-reprovada');
            }
            else if ($scope.profiledata.user_active != 'yes')
            {
                $state.go('aguardando-aprovacao');
            }

            $scope.showAddComment = false;
            $scope.showAddCommentFunction = function(){
                $scope.showAddComment = true;
            };

            $scope.TrustDangerousSnippet = function (p) {
                if (!$scope.$$phase) {
                    $scope.$apply();
                }

                return $sce.trustAsHtml(p);

            };

            $scope.doRefresh = function () {
                $scope.feeds = [];
                page = 0;
                $scope.noMoreItemsAvailable = false;
                $scope.loadMore('refresh');
                $scope.$broadcast('scroll.refreshComplete');

            };
            $scope.galleryOpened = 0;
            $scope.openGallery = function(){
                $scope.galleryOpened = 1;
            }

            $scope.likeFeed = function (postid, feedindex) {
                $scope.feeds[feedindex].selflike = !$scope.feeds[feedindex].selflike;
                if($scope.feeds[feedindex].selflike)
                {
                    $scope.feeds[feedindex].post_likescount = parseInt($scope.feeds[feedindex].post_likescount)+parseInt(1);
                }
                else
                {
                    $scope.feeds[feedindex].post_likescount = parseInt($scope.feeds[feedindex].post_likescount)-parseInt(1);
                }
                addpostlike.addpostlike(userid, postid).success(function (likedata) {
                    var new_count = likedata.post_likescount;
                });
            }

            $scope.likeFeedPost = function (feed) {
                if(!feed.selflike)
                {
                    feed.post_likescount = parseInt(feed.post_likescount)+parseInt(1);
                    feed.selflike = true;
                }
                else
                {
                    feed.post_likescount = parseInt(feed.post_likescount)-parseInt(1);
                    feed.selflike = false;
                }
                addpostlike.addpostlike(userid, feed.post_id).success(function (likedata) {
                    var new_count = likedata.post_likescount;
                });
            }

            Error.getErrorAll().then(function (result) {
                $rootScope.errorList = result.data;
            });

            $scope.showcomments = function (feedid, feedindex,create) {
                $scope.cur_feedid = feedid;
                $scope.feedindex = feedindex;
                $scope.commentsArray = [];


                Comments.getCommentsByPost(feedid).success(function(result){
                    $scope.commentsArray = result;
                    if(!create)
                    $ionicLoading.hide();
                });


                $scope.modal.show();
            }


            $scope.setFeedState = function(state){

                setFeedState.setFeedState(state.short_name,$scope.profiledata.userid).success(function (result) {
                    if(result.result == 1)
                    {
                        $scope.profiledata.feed_view = state.short_name;
                        $window.localStorage['profile'] = JSON.stringify($scope.profiledata);
                    }
                    $state.go('tab.feeds',{},{reload:true});
                });
            }

            $ionicModal.fromTemplateUrl('templates/comments.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.iframeHeight = $window.innerHeight-100;
                $scope.modal = modal;
            });
            $scope.openModal = function () {
                $scope.modal.show();
            }
            $scope.closeModal = function () {
                $scope.modal.hide();
            }
            $scope.commentFocus = function(){
                $timeout(function() {
                    $('#comment').focus();
                },750);
            }

            $scope.comment_text = '';
            $scope.addComment = function ( txt ) {
                $scope.comment_text = txt;
                if (!$scope.comment_text) {
                    return false;
                }
                if($scope.UPost)
                {
                    addpostcomment.addpostcomment(userid, $scope.UPost, $scope.comment_text).success(function (postcommentdata) {
                        if (postcommentdata.message.trim().toString() === 'success') {

                            var curr_comments = {"comment_text": txt, "user_id": $scope.profiledata.userid, "user_name": $scope.profiledata.name, "profile_pic": $scope.profiledata.profile_pic,"comment_date":"Agora mesmo"};
                            $scope.feed.comments.push(curr_comments);
                            var new_count = postcommentdata.post_commentscount;

                        }
                    });
                }
                else
                {

                    var postid = $scope.cur_feedid;
                    addpostcomment.addpostcomment(userid, postid, $scope.comment_text).success(function (postcommentdata) {
                        if (postcommentdata.message.trim().toString() === 'success') {

                            var curr_comments = {"comment_text": $scope.comment_text, "user_id": userid, "user_name": username, "profile_pic": $scope.profiledata.profile_pic,"comment_date":"Agora mesmo"};
                            $scope.commentsArray.push(curr_comments);
                            var new_count = postcommentdata.post_commentscount;
                            $scope.feeds[$scope.feedindex].post_commentscount = new_count;

                        }
                    });
                }

            }

            $scope.loadPost = function( postId )
            {
                getuserfeeds
                    .getpostdetails( postId, userid )
                    .then( function ( resp ) {
                        $scope.feed = resp.data.feeds[0];
                        $timeout(function(){
                            $ionicSlideBoxDelegate.update();
                        }, 1);
                    });
            };

            if ( $stateParams.postId )
                $scope.loadPost( $stateParams.postId );

            $scope.showDelete = function(feed,index)
            {
                if($scope.lastDeleteEleClick >= 0 && $scope.feeds[$scope.lastDeleteEleClick].showExclude)
                {
                    $scope.feeds[$scope.lastDeleteEleClick].showExclude = false;
                }
                if(feed.showExclude)
                {
                    feed.showExclude = false;
                }
                else
                {
                    feed.showExclude = true;
                }
                $scope.lastDeleteEleClick = index;
            };

            $scope.deletePost = function(feed,index)
            {

                var confirmPopup = $ionicPopup.confirm({
                    title: 'Refriplay',
                    template: 'Você deseja excluir esta publicação?',
                    okType: 'button-balanced',
                    cancelType: 'button-balanced',
                    cancelText: 'Não',
                    okText: 'Sim'
                });

                confirmPopup.then(function (res) {
                    $ionicLoading.show({
                        content: 'Loading',
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showDelay: 0
                    });

                    if (res) {

                        deletepost.deletepost(feed.post_id).success(function (result) {
                            if(result.success == 1)
                            {
                                $scope.feeds.splice( index, 1 );
                            }
                            else
                            {
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Refriplay',
                                    okType: 'button-balanced',
                                    template: 'Não foi possível deletar esta publicação'
                                });
                            }
                        });
                    }
                    $ionicLoading.hide();
                });

            };

            $('video').bind('webkitfullscreenchange mozfullscreenchange fullscreenchange',function(e){
                var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
                var isFull = state ? 1 : 0;
                var orientation = window.screen.orientation;
                if(isFull == 1)
                {
                    orientation.lock('landscape');
                }
                else
                {
                    orientation.lock('portrait');
                }
            });

        })

        .controller('NewpostCtrl', function ($scope, $ionicModal, checkinplaces, addnewpost, $window, $cordovaFileTransfer, SERVER_URL, $state, $ionicPopup,$cordovaCamera,$ionicLoading,$rootScope,$ionicPlatform) {

            $scope.members = $rootScope.errorList;
            $scope.message =[];
            $scope.data = {
                'selplace': "Select",
                'post_text': "",
                'userid': ""
            };

            $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
                viewData.enableBack = true;
            });


            $ionicModal.fromTemplateUrl('templates/checkin.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modal = modal;
            });
            $scope.openModal = function () {
                $scope.modal.show();
            }
            $scope.closeModal = function () {
                $scope.modal.hide();
            }

            var post_text = $scope.data.post_text;

            var location = $scope.data.selplace;
            $scope.profiledata = JSON.parse($window.localStorage['profile']);

            $ionicPlatform.ready(function() {
                $rootScope.analytics('Nova publicação em' + $scope.profiledata.feed_view);
            });


            var userid = $scope.profiledata.userid;
            var feed_view = $scope.profiledata.feed_view;
            $scope.addPost = function () {

                post_text = $scope.data.post_text;
                location = $scope.data.selplace;

                if(post_text == '' && ($scope.imagesuri.length == 0 && !$scope.video.name))
                {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Por favor escreva algo ,publique algumas fotos ou faça um vídeo!'
                    });
                    return false;
                }



                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });

                var flagged = 0;
                if($scope.video.name)
                {
                    flagged = 1;
                }

                addnewpost.addnewpost(post_text, location, userid,feed_view,flagged).success(function (newpostdata) {
                    var postid = newpostdata.postid;

                    var successcount = 0;

                    if ($scope.imagesuri.length === 0) {
                        if($scope.video.name)
                        {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Publicado com sucesso! Assim que seu vídeo terminar de carregar em nossos servidores você será notificado, até lá sua publicação não pode ser vista por nenhum usuário.'
                            });
                        }
                        else
                        {
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Publicado com sucesso'
                            });
                        }

                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        $ionicLoading.hide();
                        $state.go('tab.feeds');
                    }

                    if($scope.video.name)
                    {
                        var uri = $scope.video.fullPath;
                        var options = new FileUploadOptions();

                        options.fileName = $scope.video.name;
                        options.chunkedMode = false;
                        options.headers = {
                            Connection: "close"
                        };

                        var params = new Object();
                        params.userid = userid;
                        params.postid = postid;
                        params.type = 'video';
                        options.params = params;
                        $rootScope.videoProgress = 0;
                        console.log('params',params);
                        $cordovaFileTransfer.upload(SERVER_URL + "services/upload.php", uri, options, true).then(function (result) {
                            console.log('foi',result);
                        }, function (err) {
                            console.log('erro',err);
                            $ionicLoading.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Falha ao publicar'
                            });
                            return false;
                        }, function (progress) {
                            $rootScope.videoProgress = Math.ceil((progress.loaded/progress.total)*100);
                        });
                    }

                    for (var i = 0; i < $scope.imagesuri.length; i++) {
                        var uri = $scope.imagesuri[i];
                        var options = new FileUploadOptions();

                        options.fileName = randomString(20, "A") + ".jpg";
                        options.chunkedMode = false;
                        options.headers = {
                            Connection: "close"
                        };

                        var params = new Object();
                        params.userid = userid;
                        params.postid = postid;
                        options.params = params;

                        $cordovaFileTransfer.upload(SERVER_URL + "services/upload.php", uri, options, true).then(function (result) {
                            console.log('fez o upload');
                            console.log(result);

                            successcount = successcount + 1;

                            if (successcount === $scope.imagesuri.length) {
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Refriplay',
                                    okType: 'button-balanced',
                                    template: 'Publicado com sucesso'
                                });

                                $scope.images = [];
                                $scope.imagesuri = [];

                                if (!$scope.$$phase) {
                                    $scope.$apply();
                                }
                                $ionicLoading.hide();
                                $state.go('tab.feeds');
                            }
                        }, function (err) {
                            alert("ERROR: " + JSON.stringify(err));
                            $ionicLoading.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Falha ao publicar'
                            });
                            return false;
                        }, function (progress) {
                        });

                    } 



                }); 





            }

            $scope.photoType = '';
            $scope.images = [];
            $scope.imagesuri = [];
            $scope.video = [];
            $scope.opengallery = function () {
                if($scope.imagesuri.length >= 3)
                {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Você só pode anexar 3 imagens por publicação!'
                    });
                    return false;
                }

                var permissions = cordova.plugins.permissions;
                permissions.checkPermission(permissions.WRITE_EXTERNAL_STORAGE, function( status ){
                    if ( status.hasPermission ) {
                        $scope.imageGetter();
                    }
                    else
                    {
                        permissions.requestPermission(permissions.WRITE_EXTERNAL_STORAGE, function( status ){
                            if ( status.hasPermission ) {
                                $scope.imageGetter();
                            }
                        });

                    }
                });

            };
            $scope.imageGetter = function(){

                var options = {
                    quality: 50,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                    mediaType: Camera.MediaType.ALLMEDIA,
                    limit:3,
                    correctOrientation: true,
                    allowEdit: true
                };





                $cordovaCamera.getPicture(options).then(function(uri){
                    var imagesType = ["png","PNG","jpg","JPG","jpeg","JPEG","gif","GIF"];

                    if(imagesType.indexOf(uri.substring(uri.length-3, uri.length)) >= 0)
                    {
                        $scope.photoType = 'gallery';
                        $scope.imagesuri.push(uri);
                    }
                    else
                    {
                        $scope.imagesuri = [];
                        $scope.photoType = 'video';
                        $scope.video = {};
                        $scope.video.fullPath = uri;

                        var lastDot   = uri.lastIndexOf( "." ),
                            extension = uri.substr( lastDot, uri.length - lastDot + 1 );

                        $scope.video.name = 'video_refriplay' + extension.toLowerCase();
                    }


                })
            };

           $scope.takeAPicture = function(){
               $scope.images = [];
               $scope.imagesuri = [];
               var permissions = cordova.plugins.permissions;
               permissions.checkPermission(permissions.WRITE_EXTERNAL_STORAGE, function( status ){
                   if ( status.hasPermission ) {
                       $scope.openCam();
                   }
                   else
                   {
                       permissions.requestPermission(permissions.WRITE_EXTERNAL_STORAGE, function( status ){
                           if ( status.hasPermission ) {
                               $scope.openCam();
                           }
                       });

                   }
               });

           };

           $scope.openCam = function(){
               var options = {
                   destinationType: Camera.DestinationType.FILE_URI,
                   sourceType: Camera.PictureSourceType.CAMERA,
                   correctOrientation: true,
                   allowEdit: true
               };
               $cordovaCamera.getPicture(options).then(function(imageURI) {
                   var srcobj = {src: imageURI};
                   $scope.images.push(srcobj);
                   $scope.imagesuri.push(imageURI);
                   if (!$scope.$$phase) {
                       $scope.$apply();
                   }
                   $scope.photoType = 'cam';
                   if (!$scope.$$phase) {
                       $scope.$apply();
                   }
               }, function(err) {
                   console.log('error: '+err);
               });
           }

            $scope.openVideoCam = function(){
                    window.plugins.videocaptureplus.captureVideo(
                    function(data){
                        console.log(data);
                        $scope.video = data[0];
                        $scope.photoType = 'video';
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }

                    },
                    function(err){
                        var alertPopup = $ionicPopup.alert({
                            title: 'Refriplay',
                            okType: 'button-balanced',
                            template: 'Falha ao gravar o vídeo'
                        });
                    },
                    {
                        highquality: true,
                    }
                );

            };


            function randomString(len, an) {
                an = an && an.toLowerCase();
                var str = "", i = 0, min = an == "a" ? 10 : 0, max = an == "n" ? 10 : 62;
                for (; i++ < len; ) {
                    var r = Math.random() * (max - min) + min << 0;
                    str += String.fromCharCode(r += r > 9 ? r < 36 ? 55 : 61 : 48);
                }
                return str;
            }
        })

        .controller('ProfileCtrl', function (S3_URL, $scope, SERVER_URL, getfeedprofile, $window, getselffeed, addpostcomment, addpostlike, $ionicModal, $state, deletepost, $ionicPopup,$ionicPlatform) {
            $scope.showgallery = true;
            $scope.photo_baseurl = SERVER_URL;
            $scope.profiledata = JSON.parse($window.localStorage['profile']);

            var userid = $scope.profiledata.userid;
            var username = $scope.profiledata.name;
            var profile_pic = $scope.profiledata.profile_pic;
            $scope.items = [];
            getfeedprofile.getfeedprofile(userid).success(function (profiledata) {
                $scope.feedprofile = profiledata;
                $scope.items = profiledata.photos;
            });

            var page = 0;
            $scope.feeds = [];

            $scope.loadMore = function (type) {
                getselffeed.getselffeed(userid, page).success(function (feedsdata) {
                    var success = feedsdata.success;
                    if (success === 1) {
                        if (type === 'iscroll') {
                            $scope.feeds = $scope.feeds.concat(feedsdata.feeds);
                        } else {
                            $scope.feeds = feedsdata.feeds;
                        }
                        page++;
                        $scope.pofilepic_baseurl = SERVER_URL + "/profile_imgs/"
                        $scope.photo_baseurl = SERVER_URL;
                    }
                    else {
                        $scope.noMoreItemsAvailable = true;
                    }
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                });
            };

            $scope.likeFeed = function (postid, feedindex) {
                addpostlike.addpostlike(userid, postid).success(function (likedata) {
                    var new_count = likedata.post_likescount;
                    $scope.feeds[feedindex].post_likescount = new_count;
                    $scope.feeds[feedindex].selflike = !$scope.feeds[feedindex].selflike;
                });
            }

            $scope.showcomments = function (commentsArray, feedid, feedindex) {
                $scope.cur_feedid = feedid;
                $scope.feedindex = feedindex;
                $scope.commentsArray = commentsArray;
                $scope.oModal1.show();
            }

            $scope.showPhotos = function () {

                $scope.showgallery = !$scope.showgallery;
            }

            $ionicModal.fromTemplateUrl('templates/comments.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.oModal1 = modal;
            });
            $scope.openModal = function (index) {

                $scope.oModal1.show();
            }
            $scope.closeModal = function (index) {
                $scope.oModal1.hide();
            }



            $scope.comment_text = '';
            $scope.addComment = function (comment_text) {

                if (!comment_text) {
                    return false;
                }
                var postid = $scope.cur_feedid;
                addpostcomment.addpostcomment(userid, postid, comment_text).success(function (postcommentdata) {
                    if (postcommentdata.message.trim().toString() === 'success') {

                        var curr_comments = {"comment_text": comment_text, "user_id": userid, "user_name": username, "profile_pic": profile_pic};
                        $scope.commentsArray.push(curr_comments);
                        var new_count = postcommentdata.post_commentscount;
                        $scope.feeds[$scope.feedindex].post_commentscount = new_count;

                    }
                });
            }

            $scope.deletePost = function (postid) {

                var confirmPopup = $ionicPopup.confirm({
                    title: 'Refriplay',
                    template: 'Are you sure you want to Delete?',
                    okType: 'button-balanced',
                    cancelType: 'button-balanced',
                });
                confirmPopup.then(function (res) {
                    if (res) {
                        deletepost.deletepost(postid).success(function (postcommentdata) {
                            if (postcommentdata.message.trim().toString() === 'success') {
                                page = 0;
                                $scope.feeds = [];
                                $scope.loadMore('iscroll');

                                if (!$scope.$$phase) {
                                    $scope.$apply();
                                }

                            }
                        });
                    } else {

                    }
                });

            }
        })

        .controller('FriendsCtrl', function ($scope, getfriends, SERVER_URL, S3_URL, $window, addfollowing, $ionicLoading, unfollow, $ionicPopup) {



            $scope.profiledata = JSON.parse($window.localStorage['profile']);
            var userid = $scope.profiledata.userid;

            $scope.pofilepic_baseurl = S3_URL + "profile_imgs/";
            $scope.btnitems = [
                {id: "followers", name: "Followers", changed: false},
                {id: "following", name: "Following", changed: true},
                {id: "suggestions", name: "Others", changed: true}

            ];

            $scope.changeFriends = function (id) {
                for (var i = 0; i < 3; i++) {
                    var curr = $scope.btnitems[i];
                    if (curr.id !== id) {
                        curr.changed = true;
                    }
                }
                $scope.getFriends(userid, id);
            };

            $scope.getFriends = function (userid, type) {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                getfriends.getfriends(userid, type).success(function (friendsdata) {
                    $scope.friends = friendsdata.users;
                    $ionicLoading.hide();
                });
            }

            $scope.getFriends(userid, "followers");

            $scope.addfollowing = function (followingid) {
                addfollowing.addfollowing(userid, followingid).success(function (followingdata) {

                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }

                    $scope.getFriends(userid, "suggestions");

                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Successfully Following!'
                    });
                });
            }

            $scope.unfollow = function (followingid) {
                unfollow.unfollow(userid, followingid).success(function (unfollowdata) {

                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }

                    $scope.getFriends(userid, "following");

                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Unfollowed User!'
                    });
                });
            }


        })



        .controller('PhotoGalleryCtrl', function ($scope, getuserphotos, $window, $stateParams) {
            var fromprofile = $stateParams.fromprofile;
            var userid = $stateParams.userid;

            if (fromprofile === "yes") {
                $scope.showpback = true;
            } else {
                $scope.showpback = false;
            }

            $scope.items = [];
            getuserphotos.getuserphotos(userid).success(function (photodata) {
                if (photodata.message.trim().toString() === 'success') {
                    $scope.items = photodata.photos;
                }
            });


        })

        .controller('ProfilePageCtrl', function ($scope, S3_URL, $window, $stateParams, getotheruserprofile, SERVER_URL, $state, addfollowing, unfollow, $ionicPopup) {
            var userid = $stateParams.userid;
            var currtab = $stateParams.currpage;

            $scope.profiledata = JSON.parse($window.localStorage['profile']);
            var loggedinuserid = $scope.profiledata.userid;

            if (parseInt(loggedinuserid) === parseInt(userid)) {
                $state.go('tab.profile');
            } else {
                $scope.pofilepic_baseurl = S3_URL + "profile_imgs/"
                $scope.photo_baseurl = S3_URL + "posts/";
                $scope.profiledata = JSON.parse($window.localStorage['profile']);

                $scope.items = [];
                getotheruserprofile.getotheruserprofile(userid, loggedinuserid).success(function (profiledata) {
                    $scope.feedprofile = profiledata;
                    $scope.items = profiledata.photos;
                });

                $scope.goBackFromProfile = function () {
                    if (currtab === 'feeds') {
                        $state.go('tab.feeds');
                    } else if (currtab === 'friends') {
                        $state.go('tab.friends');
                    }
                };
            }
            $scope.addfollowing = function () {
                addfollowing.addfollowing(loggedinuserid, userid).success(function (followingdata) {
                    $scope.feedprofile.showfollowbutton = false;
                    $scope.feedprofile.showunfollowbutton = true;

                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }


                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Successfully Following!'
                    });
                });
            }

            $scope.unfollow = function () {
                unfollow.unfollow(loggedinuserid, userid).success(function (unfollowdata) {
                    $scope.feedprofile.showunfollowbutton = false;
                    $scope.feedprofile.showfollowbutton = true;

                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Unfollowed User!'
                    });
                });
            }

        })

        .controller('NotificationsCtrl', function ($scope, $rootScope, S3_URL, $location, Notifications, $window, $ionicLoading, $ionicPopup,SERVER_URL) {
            $scope.notifications = [];
            $scope.noMoreItemsAvailable = false;
            $scope.profiledata = JSON.parse($window.localStorage['profile']);
            $scope.length = 1;
            $rootScope.qntNotifications = 0;
            var userid = $scope.profiledata.userid;
            $scope.pofilepic_baseurl = SERVER_URL + "profileuploads/";

            var page = 0;

            Notifications.readAll(userid);

            $scope.loadMore = function() {
                Notifications.getUserNotifications(userid, page).success(
                    function(result) {
                        if(result.success == 0) {
                            $scope.noMoreItemsAvailable = true;
                            $scope.length = 0;
                        } else {
                            if(result.length < 10) {
                                $scope.noMoreItemsAvailable = true;
                            } else {
                                $scope.noMoreItemsAvailable = false;
                            }
                            page++;
                            $scope.length = result.length;
                            $scope.notifications = $scope.notifications.concat(result.notifications);
                            console.log($scope.notifications);
                        }
                    }).error(function(error) {
                        console.log(error);
                    }).finally(function(){
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                    });
            }


            $scope.checkNotification  = function(notification) {
                if(notification.is_read == 0) {
                    Notifications.changeNotificationStatus(notification.id);
                }
                $location.path('post/' + notification.post_id);
            }

            $scope.doRefresh = function() {
                Notifications.getUserNotifications(userid, page)
                .success(
                    function(result) {
                        $scope.notifications = result.notifications;
                })
                .error(function(error) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Falha ao capturar suas notificações'
                    });
                    return false;
                })
                .finally(function(){
                    $scope.$broadcast('scroll.refreshComplete');
                });
            }

        })

        .controller('AccountCtrl', function ($scope, S3_URL, SERVER_URL, $ionicLoading, updateprofile, $window, $ionicPopup, $state, $cordovaFileTransfer, $ionicHistory,md5,$rootScope,$ionicPlatform,$stateParams,$filter) {
            $scope.profiledata = JSON.parse($window.localStorage['profile']);

            $ionicPlatform.ready(function() {
                $rootScope.analytics('Minha Conta');
            });

            $scope.$on('$ionicView.enter', function(e) {
                $rootScope.checkNotifications();
            });


            if($stateParams.field)
            {
                updateprofile.getFieldInfo($stateParams.field).success(function (result) {
                    if(result.success == 1)
                    {
                        $scope.editInfo = result.data;
                        $scope.editInfo.inputName = $stateParams.field;
                        if($stateParams.field == 'date_birthday')
                        {
                            $scope.profiledata[$scope.editInfo.inputName] = $filter('date')($scope.profiledata[$scope.editInfo.inputName], 'dd/MM/yyyy');
                        }

                        if($stateParams.field != 'password')
                        {
                            $scope.info = $scope.profiledata[$scope.editInfo.inputName];
                        }

                    }
                    else
                    {
                        $state.go('tab.account');
                    }
                });

                $scope.editInfoProfile = function(form)
                {
                    var inputMoney = document.getElementsByClassName('moneyInput');
                    angular.element(inputMoney).triggerHandler('blur');

                    if (form.$valid)
                    {
                        $ionicLoading.show({
                            content: 'Loading',
                            animation: 'fade-in',
                            showBackdrop: true,
                            maxWidth: 200,
                            showDelay: 0
                        });

                        if($stateParams.field == 'password')
                        {
                            if(form.info.$viewValue != undefined && $scope.profiledata.password !== md5.createHash(form.info.$viewValue))
                            {
                                form.info.$error.incorrectPass = true;
                                $ionicLoading.hide();
                                return false;
                            }
                            else
                            {
                                form.info.$viewValue = md5.createHash(form.info.$viewValue);
                            }
                        }

                        if($stateParams.field == 'date_birthday')
                        {
                            var allowed_date = new Date(2005,12,31);
                            var day = form.info.$viewValue.substring(0,2);
                            var month = form.info.$viewValue.substring(3,5);
                            var year = form.info.$viewValue.substring(6,10);
                            var date = new Date(year,month,day);

                            if(date > allowed_date)
                            {
                                form.info.$error.invalidDate = true;
                                $ionicLoading.hide();
                                return false;
                            }
                            else
                            {
                                form.info.$viewValue = year+'-'+month+'-'+day;
                            }
                        }


                        updateprofile.updateprofileinfo($stateParams.field,form.info.$viewValue,$scope.profiledata.userid).success(function (result) {
                            if(result.success == 1)
                            {
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Refriplay',
                                    okType: 'button-balanced',
                                    template: 'Atualizado com sucesso'
                                });
                                $window.localStorage['profile'] = JSON.stringify(result.userdetails);
                                $ionicLoading.hide();
                                $state.go('tab.account');
                            }
                            else
                            {
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Refriplay',
                                    okType: 'button-balanced',
                                    template: 'Houve um erro ao tentar atualizar a sua informação :('
                                });
                                $ionicLoading.hide();
                            }
                        });
                    }
                }
                return false;
            }


            $scope.setHelper = function(value)
            {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                if(value == undefined)
                {
                    value = false;
                }
                updateprofile.updateprofileinfo('isHelper',value,$scope.profiledata.userid).success(function (result) {
                    $window.localStorage['profile'] = JSON.stringify(result.userdetails);
                    if(value == true)
                    {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Muito bem!',
                            okType: 'button-balanced',
                            template: 'Agora você será informado de todas as publicações do seu estado atual.'
                        });
                    }
                    else
                    {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Refriplay',
                            okType: 'button-balanced',
                            template: 'Agora você só será notificado em publicações que interagiu.'
                        });
                    }
                    $ionicLoading.hide();
                });
            }

            var userid = $scope.profiledata.userid;
            var username = $scope.profiledata.name;
            var password = $scope.profiledata.password;
            var gender = $scope.profiledata.gender;
            var feed_view = $scope.profiledata.feed_view;
            var cpf = $scope.profiledata.CPF;




            if($scope.profiledata.profile_pic)
            {
                $scope.selprofilepic = SERVER_URL + "profileuploads/" + $scope.profiledata.profile_pic;
            }
            else
            {
                $scope.selprofilepic = 'img/emptyavatar.jpg';
            }

            if($scope.profiledata.logo)
            {
                $scope.logo = SERVER_URL + "profileuploads/" + $scope.profiledata.logo;
            }
            else
            {
                $scope.logo = 'img/emptyavatar.jpg';
            }

            $scope.updateprofileflag = false;
            $scope.updateProfilePicture = function (type) {
                var permissions = cordova.plugins.permissions;

                permissions.checkPermission(permissions.WRITE_EXTERNAL_STORAGE, function( status ){
                    if ( status.hasPermission ) {
                        if(type=='logo')
                        {
                            $scope.getPickLogo();
                        }
                        else
                        {
                            $scope.getPick();
                        }
                    }
                    else
                    {
                        permissions.requestPermission(permissions.WRITE_EXTERNAL_STORAGE, function( status ){
                            if ( status.hasPermission ) {
                                if(type=='logo')
                                {
                                    $scope.getPickLogo();
                                }
                                else
                                {
                                    $scope.getPick();
                                }
                            }
                        });

                    }
                });

            };

            $scope.getPick = function(){
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                window.imagePicker.getPictures(
                    function (results) {
                        if(results.length >= 1){
                            $scope.updateprofileflag = true;
                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }

                            var options = {
                                fileName: userid + ".jpg",
                                chunkedMode: false
                            };
                            var params = new Object();
                            params.userid = userid;

                            options.params = params;

                            $cordovaFileTransfer.upload(SERVER_URL + "services/uploadProfilePicture.php", results[0], options, true).then(function (result) {
                                $scope.updateprofileflag = false;


                                if (!$scope.$$phase) {
                                    $scope.$apply();
                                }
                                $scope.selprofilepic = SERVERURL + "profileuploads/" +result.response;
                                $scope.selprofilepic = $scope.selprofilepic.replace(/\s/g, "");
                                $scope.profiledata.profile_pic = result.response;
                                var user_info = JSON.parse($window.localStorage['profile']);
                                user_info.profile_pic = result.response.replace('/','');
                                $window.localStorage['profile'] = JSON.stringify(user_info);
                                $ionicLoading.hide();

                            }, function (err) {
                                $ionicLoading.hide();
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Refriplay',
                                    okType: 'button-balanced',
                                    template: 'Houve um erro :('
                                });
                                return false;
                            }, function (progress) {
                            });

                        }else{
                            $ionicLoading.hide();

                        }

                    }, function (error) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Refriplay',
                            okType: 'button-balanced',
                            template: 'Houve algum erro, tente novamente'
                        });
                    }, {
                        maximumImagesCount: 1,
                        width: 800,
                        height: 600
                    }
                );
            };

            $scope.getPickLogo = function(){
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                window.imagePicker.getPictures(
                    function (results) {
                        if(results.length >= 1){
                            $scope.updateprofileflag = true;
                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }

                            var options = {
                                fileName: userid + ".jpg",
                                chunkedMode: false
                            };
                            var params = new Object();
                            params.userid = userid;

                            options.params = params;

                            $cordovaFileTransfer.upload(SERVER_URL + "services/uploadLogoPicture.php", results[0], options, true).then(function (result) {
                                $scope.updateprofileflag = false;




                                $scope.profiledata.logo = result.response;

                                var user_info = JSON.parse($window.localStorage['profile']);
                                user_info.logo = result.response.replace('/','');
                                $scope.logo = SERVER_URL + "profileuploads/" + user_info.logo;
                                $window.localStorage['profile'] = JSON.stringify(user_info);
                                if (!$scope.$$phase) {
                                    $scope.$apply();
                                }
                                $ionicLoading.hide();


                            }, function (err) {
                                $ionicLoading.hide();
                                var alertPopup = $ionicPopup.alert({
                                    title: 'Refriplay',
                                    okType: 'button-balanced',
                                    template: 'Houve um erro :('
                                });
                                return false;
                            }, function (progress) {
                            });

                        }else{
                            $ionicLoading.hide();

                        }

                    }, function (error) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Refriplay',
                            okType: 'button-balanced',
                            template: 'Houve algum erro, tente novamente'
                        });
                    }, {
                        maximumImagesCount: 1
                    }
                );
            };

            $scope.editInfo = function(param)
            {
                $state.go('tab.account-edit',{'field':param});
            }

            $scope.qtdSolutions = 0;
            $scope.qtdLikeSolution = 0;
            $scope.qtdPosts = 0;
            updateprofile.getProfileMetrics($scope.profiledata.userid).success(function (result) {
                if(result.success == 1)
                {
                    $scope.qtdSolutions = result.data[0].qtd_solution;
                    $scope.qtdLikeSolution = result.data[0].qtd_solution_like;
                    $scope.qtdPosts = result.data[0].qtd_post;
                }
            });


            $scope.confirmLogout = function () {

                var confirmPopup = $ionicPopup.confirm({
                    title: 'Refriplay',
                    template: 'Você deseja sair do aplicativo?',
                    okType: 'button-balanced',
                    cancelType: 'button-balanced',
                    cancelText: 'Não',
                    okText: 'Sim'
                });

                confirmPopup.then(function (res) {
                    if (res) {
                        $window.localStorage.removeItem('profile');
                        $ionicHistory.clearCache();
                        $ionicHistory.clearHistory();
                        $state.go('login-intro');
                    } else {

                    }
                });

            }

            $scope.data = {
                'name': username,
                'feed_view': feed_view,
                'gender': gender,
                'profile_pic': $scope.profiledata.profile_pic,
                'cpf':cpf

            };

            $scope.updateprofile = function (form) {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                if (form.$valid){
                        if($scope.data.password != undefined && password !== md5.createHash($scope.data.password))
                        {
                            $ionicLoading.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'A senha atual está incorreta'
                            });
                            return false;
                        }
                        updateprofile.updateprofile($scope.data.name, $scope.data.new_password, $scope.data.feed_view, $scope.data.gender, userid,$scope.data.cpf).success(function (newpostdata) {

                        if(newpostdata.success == 1)
                        {
                            password = $scope.data_new_passoword;
                            var userdata = newpostdata.userdetails;
                            $window.localStorage['profile'] = JSON.stringify(userdata);

                            $ionicLoading.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Atualizado com sucesso!'
                            });
                        }
                        else
                        {
                            $ionicLoading.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Refriplay',
                                okType: 'button-balanced',
                                template: 'Houve um erro :('
                            });
                            return false;
                        }



                    });
                }
                else
                {
                    $ionicLoading.hide();
                }
            }



            $scope.selectables = [
                1
              ];

              $scope.longList  = [];
              for(var i=0;i<1000; i++){
                $scope.longList.push(i);
              }

              $scope.selectableNames =  [
                { name : "Mauro", role : "black hat"},
                { name : "Silvia", role : "pineye"},
                { name : "Merlino", role : "little canaglia"},
              ];

              $scope.someSetModel = 'Mauro';

              $scope.getOpt = function(option){
                return option.name + ":" + option.role;
              };

              $scope.shoutLoud = function(newValuea, oldValue){
                alert("changed from " + JSON.stringify(oldValue) + " to " + JSON.stringify(newValuea));
              };

              $scope.shoutReset = function(){
                alert("value was reset!");
              };
        })

        .controller('TrendinghashtagsCtrl', function ($scope, SERVER_URL, $ionicLoading, $window, $ionicPopup, $state, gettrendinghashtags) {

            gettrendinghashtags.gettrendinghashtags().success(function (trendinghashtagsdata) {
                $scope.trendhashtags = trendinghashtagsdata;
            });

        })

.controller('estadosCtrl', function ($scope, SERVER_URL, $ionicLoading, $window, $ionicPopup, $state, getstates,checkinplaces,setFeedState,$rootScope,$ionicPlatform) {
    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
        viewData.enableBack = false;
    });

    $ionicPlatform.ready(function() {
        $rootScope.analytics('Selecionar estados');
    });

    $scope.lat = 0;
    $scope.lng = 0;

    $scope.noState = false;
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });



        navigator.geolocation.getCurrentPosition(function (position) {
            $scope.lat = position.coords.latitude;
            $scope.lng = position.coords.longitude;
        }, function(){
        });

        getstates.getstates($scope.lat,$scope.lng).success(function (result) {
            if(result.success == 1)
            {
                $scope.states = result.data;
            }
            else
            {
                $scope.noState = true;
            }
        });
    $ionicLoading.hide();


            $scope.setFeedState = function(state){
                var user = JSON.parse($window.localStorage['profile']);

                setFeedState.setFeedState(state.short_name,user.userid).success(function (result) {
                    if(result.result == 1)
                    {
                        user.feed_view = state.short_name;
                        $window.localStorage['profile'] = JSON.stringify(user);
                    }
                    $state.go('tab.feeds');
                });
            }


        })

    .controller('ToolsCtrl', function ($scope, SERVER_URL, Tools,$window, $rootScope) {
        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = false;
            $rootScope.checkNotifications();

        });
        Tools.getTools().success(function (result) {
            $scope.tools = result;
        });

        $scope.goToTool = function(url)
        {
            $window.location.href = url;
        }

        $scope.isNew = function ( tool )
        {
            var _MS_PER_DAY = 1000 * 60 * 60 * 24;
            var pd  = new Date( tool.publish_date );
            var now = new Date();

            var newUntilDays = 25;

            var utc1 = Date.UTC( pd.getFullYear(), pd.getMonth(), pd.getDate() );
            var utc2 = Date.UTC( now.getFullYear(), now.getMonth(), now.getDate() );

            return Math.floor( ( utc2 - utc1 ) / _MS_PER_DAY ) <= 25;
        };
    })
    .controller('ErrorCategoryCtrl', function ($scope, SERVER_URL, ErrorCategory) {
        ErrorCategory.getErrorCategory().success(function (result) {
            $scope.server_url = SERVER_URL;
            $scope.cetegories = result;
        });
    })
    .controller('InsuranceCtrl', function($scope, Insurance) {
        Insurance.getText().success(function(result) {
            if(result.success) {
                $scope.text = result.text;
            }
        })
    })
    .controller('MarksCtrl', function ($scope, SERVER_URL, Marks,$ionicPopup) {
        Marks.getMarks().success(function (result) {
            $scope.server_url = SERVER_URL;
            $scope.marks = result;
        });
        $scope.is_disabled = function(status,event)
        {
            if(status == 0)
            {
                var alertPopup = $ionicPopup.alert({
                    title: 'Refriplay',
                    okType: 'button-balanced',
                    template: 'Ainda estamos recolhendo o material deste fabricante, assim que for concluído você será notificado'
                });
                event.preventDefault();
            }
            return false;
        }
    })
    .controller('ProductsCtrl', function ($scope, $stateParams,$state,$location,SERVER_URL, Products) {
            if($stateParams.mark_id >=1){
                Products.getProducts($stateParams.mark_id).success(function (result) {
                    $scope.server_url = SERVER_URL;
                    $scope.products = result;
                });
            }

            if($stateParams.products_id >=1){
                Products.getProductsErrorList($stateParams.products_id).success(function (result) {
                    $scope.server_url = SERVER_URL;
                    $scope.erros = result;
                });
            }

            $scope.go = function (url) {
                $state.go(url);
            }



    })
    .controller('ErrorCtrl', function ($scope, $stateParams,$window,$ionicModal,$ionicPopup,$ionicLoading,SERVER_URL, Error,Solutions,Likes,$rootScope,$ionicHistory,$state) {
        $backView = $ionicHistory.backView();

        if($backView && $backView.stateId == "tab.feeds")
        {
            $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
                viewData.enableBack = true;
            });
        }
        $rootScope.$ionicGoBack = function(backCount) {
            if($backView.stateId == "tab.feeds")
            {
                $state.go('tab.tools');
            }
            else
            {
                $ionicHistory.goBack();
            }
        };
        $scope.showExcluir=function(index) {
          $scope.solutions[index].excluir= true;
        }
        $scope.showExcluirBest=function() {
          $scope.bestSolution.excluir = true;
        }

        $scope.error_id = $stateParams.error_id;
        $scope.profiledata = JSON.parse($window.localStorage['profile']);
        $scope.user_id  = $scope.profiledata['userid'];
        Error.getError($stateParams.error_id).success(function (result) {
            $scope.server_url = SERVER_URL;
            $scope.error = result[0];
        });
        $scope.getSolutions = function () {
            Solutions.getSolutions($stateParams.error_id).success(function (result) {
                $scope.server_url = SERVER_URL;
                $scope.solutions = result;
                $scope.bestSolution = result[0];
                $scope.solutions.splice(0, 1);
            });
        }
        $scope.getSolutions();

        $scope.addSolution = function (form) {
            if(form.$valid)
            {
                Solutions.addSolution($scope.user_id,$scope.error.id,form.solution_text.$viewValue).success(function (result) {
                    $scope.getSolutions();
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Sua solução foi publicada com sucesso!'
                    });
                    form.solution_text.$viewValue ='';
                    $scope.closeModal();
                });
            }
            else
            {
                var alertPopup = $ionicPopup.alert({
                    title: 'Refriplay',
                    okType: 'button-balanced',
                    template: 'Você esqueceu de escrever a sua solução'
                });
            }

        }
        $scope.desativaSolution = function (solution_id) {
          var confirmPopup = $ionicPopup.confirm({
              title: 'Refriplay',
              template: 'Você deseja excluir esta solução?',
              okType: 'button-balanced',
              cancelType: 'button-balanced',
              cancelText: 'Não',
              okText: 'Sim'
          });

          confirmPopup.then(function (res) {

              $ionicLoading.show({
                  content: 'Loading',
                  animation: 'fade-in',
                  showBackdrop: true,
                  maxWidth: 200,
                  showDelay: 0
              });

              if (res == true) {
                Solutions.disableSolution(solution_id).success(function (result) {
                  $scope.getSolutions();
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Sua solução foi excluida  com sucesso!'
                    });
                        $ionicLoading.hide();

                });
              }else{
                $ionicLoading.hide();

              }
        }
      )}

        $scope.addLike = function (like,solution_id,index) {
            Likes.addLike($scope.user_id,like,solution_id).success(function (result) {
                if(result.error)
                {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Refriplay',
                        okType: 'button-balanced',
                        template: 'Você já curtiu essa avaliação!'
                    });
                }
                else
                {
                    if(index == -1)
                    {
                        if(like == 0)
                        {
                            if($scope.bestSolution.likes > 0)
                            {
                                $scope.bestSolution.likes = parseInt($scope.bestSolution.likes)-parseInt(1);
                            }

                            if(result.success == 2)
                            {
                                $scope.bestSolution.deslikes = parseInt($scope.bestSolution.deslikes)+parseInt(1);
                            }
                        }
                        else
                        {
                            $scope.bestSolution.likes = parseInt($scope.bestSolution.likes)+parseInt(1);
                            if(result.success == 2)
                            {
                                if($scope.bestSolution.deslikes > 0)
                                {
                                    $scope.bestSolution.deslikes = parseInt($scope.bestSolution.deslikes)-parseInt(1);
                                }
                            }
                        }
                    }
                    else
                    {
                        if(like == 0)
                        {
                            $scope.solutions[index].deslikes = parseInt($scope.solutions[index].deslikes)+parseInt(1);
                            if(result.success == 2)
                            {
                                if($scope.solutions[index].likes > 0)
                                {
                                    $scope.solutions[index].likes = parseInt($scope.solutions[index].likes)-parseInt(1);
                                }
                            }
                        }
                        else
                        {
                            $scope.solutions[index].likes = parseInt($scope.solutions[index].likes)+parseInt(1);
                            if(result.success == 2)
                            {
                                if($scope.solutions[index].deslikes > 0)
                                {
                                    $scope.solutions[index].deslikes = parseInt($scope.solutions[index].deslikes)-parseInt(1);
                                }
                            }
                        }

                    }
                }
            })
        }

        $ionicModal.fromTemplateUrl('templates/modais/modal-solution.html', {
            scope: $scope,
            animation: 'slide-in-up',
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.openModal = function() {
            $scope.modal.show();
        };

        $scope.closeModal = function() {
            $scope.modal.hide();
        };

        $scope.$on('$destroy', function() {
            $scope.modal.remove();
        });

        $scope.$on('modal.hidden', function() {
        });

        $scope.$on('modal.removed', function() {
        });
    })
    .controller('BudgetCtrl', function ($scope,$rootScope, $stateParams, SERVER_URL, $http, $window, $filter, $ionicPopup,$state) {
        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });
        var oldSoftBack = $rootScope.$ionicGoBack;
        $rootScope.$ionicGoBack = function(backCount) {
            $state.go('tab.tools');
            oldSoftBack();
        };

        $scope.pdfView = function(id){
          $state.go('tab.budget-pdf',{'budgetId':id});
        };

        $scope.editBudget = function(id){
            $state.go('tab.budget-edit',{'budgetId':id});
        };

        var userdata = JSON.parse( $window.localStorage['profile'] );

        var loading = $ionicPopup.show({
            template: '<h1>Carregando...</h1>',
            cssClass: 'popup-default',
        });

        $scope.groups = [
            { id: 1, "name":"Pendentes", "opened": true},
            { id: 2, "name":"Aprovados" },
            { id: 3, "name":"Cancelados" }
        ];

        $http({
            methot: "GET",
            url: SERVER_URL + "services/budgets.php?type=byOwner&technician_id=" + userdata.userid,
            cache: false,
        }).then(
            function ( response )
            {
                if ( !response.data.success )
                    return loading.close();

                $scope.budgets = response.data.data;

                loading.close();
            }
        );


        $scope.getTotal = function ( budget )
        {
            var t = parseFloat( budget.total_labors, 10 ) + parseFloat( budget.total_products, 10 );

            return "R$ " + t.toFixed( 2 ).replace( ".", "," );
        };

        $scope.getDate = function ( date )
        {
            return $filter( "date" )( new Date( date ), 'dd/MM/yyyy' );
        };

        angular.element(document).off("click.status");

        angular.element( document ).on( "click.status", function ( evt ) {
            var btn = angular.element( evt.target );

            if ( btn.parents( ".change-status" ).length )
                btn = btn.parents( ".change-status" );

            if ( !btn.is( ".change-status" ) ) {
                angular.element( ".status-budget" ).removeClass( "opened" );
                return;
            }

            if ( btn.parents( ".status-budget" ).length ) {
                return;
            } else {
                var bid = btn.attr( "data-budget-id" );
                var t   = angular.element( ".status-budget[data-budget-id='"+ bid +"']" );

                t.toggleClass( "opened" )
                    .css({
                        top: btn.offset().top - btn.parents( ".budget-group" ).offset().top,
                        left: btn.offset().left - 100,
                        right: "1em"
                    });

            }
        });


        $scope.changeStatus = function ( budget, status )
        {

            if(status == 4)
            {
                var confirmPopup = $ionicPopup.confirm({
                    title: '<h3>Refriplay</h3>',
                    template: 'Você deseja deletar o seu orçamento?',
                    cssClass: 'popup-default',
                    okType: 'button-popup-default',
                    okText: 'Não',
                    cancelType: 'button-popup-default-cancel',
                    cancelText: 'Sim',
                });

                confirmPopup.then( function ( result ) {
                    if ( !result ){
                        budget.status = status;
                        $http({
                            method: "PUT",
                            url: SERVER_URL + "services/budgets.php?id=" + budget.id,
                            data: {
                                status: status
                            }
                        });
                    }
                });
            }
            else
            {
                budget.status = status;
                $http({
                    method: "PUT",
                    url: SERVER_URL + "services/budgets.php?id=" + budget.id,
                    data: {
                        status: status
                    }
                });
            }
        };
    })
    .controller('ProdutosCtrl', function ($scope,$rootScope,$stateParams,SERVER_URL, $http, $window, $ionicPopup, $stateParams, $state, $filter) {

        var oldSoftBack = $rootScope.$ionicGoBack;
        $rootScope.$ionicGoBack = function(backCount) {
            $state.go('tab.produtos');
            oldSoftBack();
        };

        var userdata = JSON.parse( $window.localStorage['profile'] );


        if($stateParams.produtoId)
        {
            var loading = $ionicPopup.show({
                template: '<h1>Carregando...</h1>',
                cssClass: 'popup-default',
            });

            $http({
                method: "GET",
                url: SERVER_URL + "services/budgetUserProducts.php?id=" + $stateParams.produtoId,
                cache: false
            }).then(
                function ( response )
                {
                    if ( !response.data.success )
                        return loading.close();

                    $scope.produto = response.data.data;
                    $scope.produto.price = $scope.produto.price.replace('.', ',');
                    $scope.produto.price = "R$ " + $scope.produto.price;

                    loading.close();
                }
            );
        } else {
            var loading = $ionicPopup.show({
                template: '<h1>Carregando...</h1>',
                cssClass: 'popup-default',
            });

            $http({
                method: "GET",
                url: SERVER_URL + "services/budgetUserProducts.php?user_id=" + userdata.userid,
                cache: false
            }).then(
                function ( response )
                {
                    if ( !response.data.success )
                        return loading.close();

                    $scope.produtos = response.data.data;

                    loading.close();
                }
            );
        }

        $scope.deletarProduto = function(index, produtoId) {

            var confirmPopup = $ionicPopup.show({
                template: '',
                title: 'Tem certeza que deseja deletar esse produto?',
                cssClass: 'popup-default',
                scope: $scope,
                buttons: [
                { text: 'Cancelar' },
                {
                    text: '<b>Ok</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        $http({
                            method: 'DELETE',
                            url: SERVER_URL + "services/budgetUserProducts.php?id=" + produtoId,
                            data: $scope.produto
                        }).then(function(response) {
                            if ( !response.data.success ) {
                                $ionicPopup.alert({
                                    title: '<h3>Algo deu errado :(</h3>',
                                    template: 'Não foi possível deletar o produto, tente novamente por favor',
                                    cssClass: 'popup-default',
                                    okType: 'button-popup-default',
                                    okText: 'Ok'
                                });
                                return;
                            } else
                            {
                                $scope.produtos.splice(index, 1);
                            }
                        })
                    }
                }
                ]

            });

            confirmPopup.then(function(res) {
                if(res) {
                    $scope.produtos.splice(index, 1);
                } else {
                console.log('You are not sure');
                }
            });
        }

        $scope.getPrice = function ( price ) {
            if ( !price )
                return "";

            var p = price;
            p = p.replace( "R$", "" );
            p = p.replace( ".", "" );
            p = p.replace( ",", "." );
            p = p.replace( /\s/g, "" );
            p = parseFloat( p, 10 );
            return p
        };

        $scope.getHighPrice = function ( price ) {
            if ( !price )
                return "";

            var p = price;
            p = p.replace( "R$", "" );
            p = p.replace( ".", "" );
            p = p.replace( ",", "." );
            p = p.replace( /\s/g, "" );
            p = parseFloat( p, 10 )/100;
            return p
        };

        $scope.produto = $scope.produto || {};

        $scope.saveProduto = function (form) {

            if ( form.$invalid )
                return;

            $scope.produto.price = $scope.getPrice($scope.produto.price);

            var loading = $ionicPopup.show({
                template: '<h1>Salvando...</h1>',
                cssClass: 'popup-default',
            });

            var method = $stateParams.produtoId ? "PUT" : "POST";
            var query  = $stateParams.produtoId ? "?id=" + $stateParams.produtoId : "";

            $scope.produto.user_id = userdata.userid;

            $http({
                method: method,
                url: SERVER_URL + "services/budgetUserProducts.php" + query,
                data: $scope.produto
            }).then(
                function ( response ) {

                    loading.close();

                    if ( !response.data.success ) {
                        $ionicPopup.alert({
                            title: '<h3>Algo deu errado :(</h3>',
                            template: 'Não foi possível salvar sua informação, tente novamente por favor',
                            cssClass: 'popup-default',
                            okType: 'button-popup-default',
                            okText: 'Ok'
                        });
                        return;
                    }

                    $state.go( "tab.produtos" );

                    $scope.produto = response.data.data;

                }
            );
        }
    })
    .controller('ServicosCtrl', function ($scope,$rootScope,$stateParams,SERVER_URL, $http, $window, $ionicPopup, $stateParams, $state, $filter) {

        var oldSoftBack = $rootScope.$ionicGoBack;
        $rootScope.$ionicGoBack = function(backCount) {
            $state.go('tab.servicos');
            oldSoftBack();
        };

        var userdata = JSON.parse( $window.localStorage['profile'] );


        if($stateParams.servicoId)
        {
            var loading = $ionicPopup.show({
                template: '<h1>Carregando...</h1>',
                cssClass: 'popup-default',
            });

            $http({
                method: "GET",
                url: SERVER_URL + "services/budgetUserServices.php?id=" + $stateParams.servicoId,
                cache: false
            }).then(
                function ( response )
                {
                    if ( !response.data.success )
                        return loading.close();

                    $scope.servico = response.data.data;
                    $scope.servico.price = $scope.servico.price.replace('.', ',');
                    $scope.servico.price = "R$ " + $scope.servico.price;

                    loading.close();
                }
            );
        } else {
            var loading = $ionicPopup.show({
                template: '<h1>Carregando...</h1>',
                cssClass: 'popup-default',
            });

            $http({
                method: "GET",
                url: SERVER_URL + "services/budgetUserServices.php?user_id=" + userdata.userid,
                cache: false
            }).then(
                function ( response )
                {
                    if ( !response.data.success )
                        return loading.close();

                    $scope.servicos = response.data.data;

                    loading.close();
                }
            );
        }

        $scope.getPrice = function ( price ) {
            if ( !price )
                return "";

            var p = price;
            p = p.replace( "R$", "" );
            p = p.replace( ".", "" );
            p = p.replace( ",", "." );
            p = p.replace( /\s/g, "" );
            p = parseFloat( p, 10 );
            return p
        };

        $scope.getHighPrice = function ( price ) {
            if ( !price )
                return "";

            var p = price;
            p = p.replace( "R$", "" );
            p = p.replace( ".", "" );
            p = p.replace( ",", "." );
            p = p.replace( /\s/g, "" );
            p = parseFloat( p, 10 )/100;
            return p
        };

        $scope.deletarServico = function(index, servicoId) {

            var confirmPopup = $ionicPopup.show({
                template: '',
                title: 'Tem certeza que deseja deletar esse serviço?',
                cssClass: 'popup-default',
                scope: $scope,
                buttons: [
                { text: 'Cancelar' },
                {
                    text: '<b>Ok</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        $http({
                            method: 'DELETE',
                            url: SERVER_URL + "services/budgetUserServices.php?id=" + servicoId,
                            data: $scope.produto
                        }).then(function(response) {
                            if ( !response.data.success ) {
                                $ionicPopup.alert({
                                    title: '<h3>Algo deu errado :(</h3>',
                                    template: 'Não foi possível deletar o serviço, tente novamente por favor',
                                    cssClass: 'popup-default',
                                    okType: 'button-popup-default',
                                    okText: 'Ok'
                                });
                                return;
                            } else
                            {
                                $scope.servicos.splice(index, 1);
                            }
                        })
                    }
                }
                ]

            });

            confirmPopup.then(function(res) {
                if(res) {
                    $scope.servicos.splice(index, 1);
                } else {
                console.log('You are not sure');
                }
            });
        }

        $scope.servico = $scope.servico || {};

        $scope.saveServico = function (form) {

            if ( form.$invalid )
                return;

                $scope.servico.price = $scope.getPrice($scope.servico.price);

            var loading = $ionicPopup.show({
                template: '<h1>Salvando...</h1>',
                cssClass: 'popup-default',
            });

            var method = $stateParams.servicoId ? "PUT" : "POST";
            var query  = $stateParams.servicoId ? "?id=" + $stateParams.servicoId : "";

            $scope.servico.user_id = userdata.userid;

            $http({
                method: method,
                url: SERVER_URL + "services/budgetUserServices.php" + query,
                data: $scope.servico
            }).then(
                function ( response ) {

                    loading.close();

                    if ( !response.data.success ) {
                        $ionicPopup.alert({
                            title: '<h3>Algo deu errado :(</h3>',
                            template: 'Não foi possível salvar sua informação, tente novamente por favor',
                            cssClass: 'popup-default',
                            okType: 'button-popup-default',
                            okText: 'Ok'
                        });
                        return;
                    }

                    $state.go( "tab.servicos" );

                    $scope.servico = response.data.data;

                }
            );
        }
    })
    .controller('ClientsCtrl', function ($scope,$rootScope,$stateParams,SERVER_URL, $http, $window, $ionicPopup, $stateParams, $state, $filter) {
        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });
        var userdata = JSON.parse( $window.localStorage['profile'] );

        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });


        $scope.client = {'type':'cpf'};
        $scope.cliente = $scope.cliente || {};

        var oldSoftBack = $rootScope.$ionicGoBack;

        if ( $stateParams.clientId && !$scope.cliente.id ) {
            var oldSoftBack = $rootScope.$ionicGoBack;
            $rootScope.$ionicGoBack = function(backCount) {
                $state.go('tab.clientes');
                oldSoftBack();
            };
            var loading = $ionicPopup.show({
                template: '<h1>Carregando...</h1>',
                cssClass: 'popup-default',
            });

            $http({
                method: "GET",
                url: SERVER_URL + "services/clients.php?id=" + $stateParams.clientId,
                cache: false
            }).then(
                function ( response )
                {
                    if ( !response.data.success )
                        return loading.close();

                    $scope.cliente = response.data.data;
                    if($scope.cliente.cnpj.length > 0)
                    {
                        $scope.client.type = 'cnpj';
                    }
                    loading.close();
                }
            );
        } else {

            if ( $state.is( "tab.clientes" ) ) {

                var oldSoftBack = $rootScope.$ionicGoBack;
                $rootScope.$ionicGoBack = function(backCount) {
                    $state.go('tab.tools');
                    oldSoftBack();
                };
                var loading = $ionicPopup.show({
                    template: '<h1>Carregando...</h1>',
                    cssClass: 'popup-default',
                });

                $http({
                    method: "GET",
                    url: SERVER_URL + "services/clients.php?type=byTechnicial&technician_id=" + userdata.userid,
                    cache: false,
                }).then(
                    function ( response )
                    {
                        if ( !response.data.success )
                            return loading.close();

                        $scope.clients = $filter( "orderBy" )( response.data.data, "+full_name" );

                        loading.close();
                    }
                );

            }
            else
            {
                $rootScope.$ionicGoBack = function(backCount) {
                    $state.go('tab.clientes');
                    oldSoftBack();
                };
            }
        }


        $scope.saveClient = function ( form,index ) {

            if ( form.$invalid )
                return;

            var loading = $ionicPopup.show({
                template: '<h1>Salvando...</h1>',
                cssClass: 'popup-default',
            });

            var method = $stateParams.clientId ? "PUT" : "POST";
            var query  = $stateParams.clientId ? "?id=" + $stateParams.clientId : "";

            $scope.cliente.technician_id = userdata.userid;
            if($scope.client.type == 'cnpj')
            {
                delete $scope.cliente.cpf;
            }

            if($scope.client.type == 'cpf')
            {
                delete $scope.cliente.cnpj;
            }

            $http({
                method: method,
                url: SERVER_URL + "services/clients.php" + query,
                data: $scope.cliente
            }).then(
                function ( response ) {

                    loading.close();

                    if ( !response.data.success ) {
                        $ionicPopup.alert({
                            title: '<h3>Algo deu errado :(</h3>',
                            template: 'Não foi possível salvar sua informação, tente novamente por favor',
                            cssClass: 'popup-default',
                            okType: 'button-popup-default',
                            okText: 'Ok'
                        });
                        return;
                    }

                    $state.go( "tab.clientes" );

                    $scope.cliente = response.data.data;

                }
            );
        }

    })
    .controller('NewBudgetCtrl', function ($rootScope, $scope, $stateParams,SERVER_URL, $ionicPopup, $ionicModal, $window, $http, $filter, $state) {
        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });
        var oldSoftBack = $rootScope.$ionicGoBack;
        if($state.current.name == 'tab.budget-review')
        {
            $rootScope.$ionicGoBack = function(backCount) {
                $state.go('tab.budget-new');
                oldSoftBack();
            };
        }
        else
        {
            $rootScope.$ionicGoBack = function(backCount) {
                $state.go('tab.budget');
                oldSoftBack();
            };
        }


        var userdata = JSON.parse( $window.localStorage['profile'] );

        $scope.client = {'type':'cpf'};

        $scope.cliente = {};
        $scope.user    = userdata;

        $scope.clientsModal   = null;
        $scope.newClientModal = null;
        $scope.produtosModal = null;
        $scope.servicosModal = null;
        $scope.produtosSelecionados = [];
        $scope.servicosSelecionados = [];


        $rootScope.budget = $rootScope.budget || {
            products: [ {} ],
            labors  : [ {} ]
        };

        $scope.baction = function($scope,$stateParams){
            $ionicModal.fromTemplateUrl( "templates/modais/budget-client-list.html", {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.clientsModal = modal;
            });

            $ionicModal.fromTemplateUrl( "templates/modais/budget-client-new.html", {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.newClientModal = modal;
            });

            $ionicModal.fromTemplateUrl( "templates/modais/budget-pdf.html", {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.pdfModal = modal;
            });


            $ionicModal.fromTemplateUrl( "templates/modais/budget-produtos.html", {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.produtosModal = modal;
            });

            $ionicModal.fromTemplateUrl( "templates/modais/budget-servicos.html", {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.servicosModal = modal;
            });

            $scope.openServicosModal = function ()
            {
                $scope.servicosModal.show();

                var loading = $ionicPopup.show({
                    template: '<h1>Carregando...</h1>',
                    cssClass: 'popup-default',
                });

                $http({
                    method: "GET",
                    url: SERVER_URL + "services/budgetUserServices.php?user_id=" + userdata.userid,
                    cache: false,
                }).then(
                    function ( response )
                    {
                        if ( !response.data.success )
                            return loading.close();

                        $scope.labors = $filter( "orderBy" )( response.data.data, "+name" );

                        loading.close();
                    }
                );
            };

            $scope.updateServicosSelecionados = function(servico){
                if(servico.selecionado){
                    $scope.servicosSelecionados.push(servico);
                } else{
                    $scope.servicosSelecionados.splice($scope.servicosSelecionados.indexOf(servico.id), 1);
                }
            };

            $scope.selecionarServicos = function()
            {
                $scope.budget.labors = $scope.servicosSelecionados;
                $scope.closeServicosModal();
                let index = $scope.budget.labors.length;
                $(function() {
                    $('.p_price_service').maskMoney('mask',{
                        decimal: ',',
                        thousands: '.',
                        precision: 2,
                        prefix: 'R$ ',
                        allowZero: true
                    });
                    $('.p_price_service').change();
                });

            }

            $scope.openProdutosModal = function ()
            {
                $scope.produtosModal.show();

                var loading = $ionicPopup.show({
                    template: '<h1>Carregando...</h1>',
                    cssClass: 'popup-default',
                });

                $http({
                    method: "GET",
                    url: SERVER_URL + "services/budgetUserProducts.php?user_id=" + userdata.userid,
                    cache: false,
                }).then(
                    function ( response )
                    {
                        if ( !response.data.success )
                            return loading.close();

                        $scope.produtos = $filter( "orderBy" )( response.data.data, "+name" );
                        loading.close();
                    }
                );
            };


            $scope.updateProdutosSelecionados = function(produto){
                if(produto.selecionado){
                    $scope.produtosSelecionados.push(produto);
                } else{
                    $scope.produtosSelecionados.splice($scope.produtosSelecionados.indexOf(produto.id), 1);
                }

            };

            $scope.selecionarProdutos = function()
            {
                $scope.budget.products = $scope.produtosSelecionados;
                $scope.closeProdutosModal();
                let index = $scope.budget.products.length;

                $(function() {
                    $('.p_price').maskMoney('mask',{
                        decimal: ',',
                        thousands: '.',
                        precision: 2,
                        prefix: 'R$ ',
                        allowZero: true
                    });
                    $('.p_price').change();
                });
            }

            $scope.openClientsModal = function ()
            {
                $scope.clientsModal.show();

                var loading = $ionicPopup.show({
                    template: '<h1>Carregando...</h1>',
                    cssClass: 'popup-default',
                });

                $http({
                    method: "GET",
                    url: SERVER_URL + "services/clients.php?type=byTechnicial&technician_id=" + userdata.userid,
                    cache: false,
                }).then(
                    function ( response )
                    {
                        if ( !response.data.success )
                            return loading.close();

                        $scope.clients = $filter( "orderBy" )( response.data.data, "+full_name" );

                        loading.close();
                    }
                );
            };


            $scope.closeClientsModal = function ()
            {
                $scope.clientsModal.hide();
            };

            $scope.closeProdutosModal = function ()
            {
                $scope.produtosModal.hide();
            };


            $scope.closeServicosModal = function ()
            {
                $scope.servicosModal.hide();
            };

            $scope.openNewClientModal = function ()
            {
                $scope.newClientModal.show();
            };

            $scope.closeNewClientModal = function ()
            {
                $scope.newClientModal.hide();
            };

            $scope.selectClient = function ( client )
            {
                $rootScope.budget.client = client;
                $rootScope.budget.client_id = client.id;
                $scope.clientsModal.hide();
            };

            $scope.saveClient = function ( form, client )
            {
                if ( form.$invalid )
                    return;

                $rootScope.budget.client = client;
                $rootScope.budget.client_id = [client];
                $scope.newClientModal.hide();
            };

            $scope.addNextRow = function ( obj, index, model )
            {
                var add = false;

                for (var p in obj)
                    if ( obj[p] != "" )
                        add = true;

                if ( !add )
                    return;

                if ( model.length - 1 > index )
                    return;


                model.push({});

            };

            $scope.hideRemoveRow = function (model) {
                return model.length <= 1;
            }

            $scope.removeRow = function(index, model)
            {
                model.splice(index, 1);
            };

            $scope.getPrice = function ( price )
            {
                if ( !price )
                    return "";

                var p = price;
                p = p.replace( "R$", "" );
                p = p.replace( ".", "" );
                p = p.replace( ",", "." );
                p = p.replace( /\s/g, "" );
                p = parseFloat( p, 10 );
                return p
            };

            $scope.getTotal = function ( collection )
            {
                var t = 0;
                var cl = collection.length;
                if(typeof collection === 'object')
                {
                    cl = Object.keys(collection).length;
                }

                for (var i = 0; i < cl; i++) {
                    if (!collection[i] || !collection[i].price )
                        continue;

                    var p = $scope.getPrice( collection[i].price );
                    p *= collection[i].qty;

                    t += p;
                }
                return t;
            };

            $scope.alertLeaving = function ()
            {
                var confirmPopup = $ionicPopup.confirm({
                    title: '<h3>Aviso Importante</h3>',
                    template: 'Você está saindo antes de finalizar o orçamento, seus dados serão perdidos.<br><br>Deseja mesmo sair?',
                    cssClass: 'popup-default',
                    okType: 'button-popup-default',
                    okText: 'Ficar',
                    cancelType: 'button-popup-default-cancel',
                    cancelText: 'Sair',
                });

                confirmPopup.then( function ( result ) {
                    if ( !result ){
                        $rootScope.budget = {
                            products: [ {} ],
                            labors  : [ {} ]
                        };
                        $state.go( "tab.account" );
                    }
                });
            };

            $scope.reviewBudget = function ( form )
            {
                var b = angular.copy( $rootScope.budget );
                var error = false;

                for (var i = b.products.length - 1; i > 0; i--) {
                    var item = b.products[i];

                    if (   ( typeof item.name  == "undefined" || !item.name )
                        && ( typeof item.qty   == "undefined" || !item.qty )
                        && ( typeof item.price == "undefined" || !item.price )
                        && ( typeof item.un    == "undefined" || !item.un ) )
                        b.products.splice( i, 1 );
                }

                for (var i = b.labors.length - 1; i > 0; i--) {
                    var item = b.labors[i];

                    if (   ( typeof item.name  == "undefined" || !item.name )
                        && ( typeof item.qty   == "undefined" || !item.qty )
                        && ( typeof item.price == "undefined" || !item.price ) )
                        b.labors.splice( i, 1 );
                }




                if ( form.$invalid || error )
                    return $ionicPopup.alert({
                        title: "<h3>Oops...</h3>",
                        template: "Confira se todos os campos estão corretos e tente novamente.",
                        cssClass: "popup-default",
                        okText: "Ok",
                        okType: "button-popup-default"
                    });


                if($stateParams.budgetId)
                {
                    $state.go("tab.budget-review-edit",{'budgetId':$stateParams.budgetId});
                }
                else
                {
                    $state.go( "tab.budget-review");
                }

            };


            $scope.createBudget = function ()
            {
                var loading = $ionicPopup.show({
                    template: '<h1>Criar PDF...</h1>',
                    cssClass: 'popup-default',
                });

                $rootScope.budget.technician_id = userdata.userid;

                var budget = angular.copy( $rootScope.budget );

                for (var i = budget.products.length - 1; i > 0; i--) {
                    var item = budget.products[i];

                    if (   ( typeof item.name  == "undefined" || !item.name )
                        && ( typeof item.qty   == "undefined" || !item.qty )
                        && ( typeof item.price == "undefined" || !item.price )
                        && ( typeof item.un    == "undefined" || !item.un ) )
                        budget.products.splice( i, 1 );
                }

                for (var i = budget.labors.length - 1; i > 0; i--) {
                    var item = budget.labors[i];

                    if (   ( typeof item.name  == "undefined" || !item.name )
                        && ( typeof item.qty   == "undefined" || !item.qty )
                        && ( typeof item.price == "undefined" || !item.price ) )
                        budget.labors.splice( i, 1 );
                }

                for (var i = 0; i < budget.products.length; i++)
                    budget.products[i].price = $scope.getPrice( budget.products[i].price );

                for (var i = 0; i < budget.labors.length; i++)
                    budget.labors[i].price = $scope.getPrice( budget.labors[i].price );

                $http({
                    method: "POST",
                    url: SERVER_URL + "services/budgets.php",
                    data: budget
                }).then(
                    function ( response ) {

                        loading.close();

                        if ( !response.data.success ) {
                            return $ionicPopup.alert({
                                title: "<h3>Oops...</h3>",
                                template: "Houve um erro ao criar o seu orçamento. Tente novamente por favor. Caso o erro persista entre em contato com nossa equipe.",
                                cssClass: "popup-default",
                                okText: "Ok",
                                okType: "button-popup-default"
                            });
                        }
                        else
                        {
                            $rootScope.budget = {
                                products: [ {} ],
                                labors  : [ {} ]
                            };
                            $state.go('tab.budget-pdf',{'budgetId':response.data.data});
                        }

                    }
                );
            };

            $scope.editBudget = function (budgetId)
            {
                var loading = $ionicPopup.show({
                    template: '<h1>Editando...</h1>',
                    cssClass: 'popup-default',
                });

                $rootScope.budget.technician_id = userdata.userid;

                var budget = angular.copy( $rootScope.budget );

                budget.products.pop();
                budget.labors.pop();

                for (var i = 0; i < budget.products.length; i++)
                    budget.products[i].price = $scope.getPrice( budget.products[i].price );

                for (var i = 0; i < budget.labors.length; i++)
                    budget.labors[i].price = $scope.getPrice( budget.labors[i].price );

                $http({
                    method: "PUT",
                    url: SERVER_URL + "services/budgets.php?id="+budgetId,
                    data: budget
                }).then(
                    function ( response ) {

                        loading.close();

                        if ( !response.data.success ) {
                            return $ionicPopup.alert({
                                title: "<h3>Oops...</h3>",
                                template: "Houve um erro ao editar o seu orçamento. Tente novamente por favor. Caso o erro persista entre em contato com nossa equipe.",
                                cssClass: "popup-default",
                                okText: "Ok",
                                okType: "button-popup-default"
                            });
                        }
                        else
                        {
                            $rootScope.budget = $rootScope.budget || {
                                products: [ {} ],
                                labors  : [ {} ]
                            };
                            $state.go('tab.budget-pdf',{'budgetId':response.data.data});
                        }

                    }
                );
            };

        };

        $scope.edit = false;
        if($state.current.name == 'tab.budget-review' || $state.current.name == 'tab.budget-review-edit')
        {
            if ( $stateParams.budgetId) {
                $scope.edit = true;
                $scope.budgetId = $stateParams.budgetId;
            }
            $scope.baction($scope,$stateParams);
        }
        else
        {
            if ( $stateParams.budgetId)
            {
                $scope.edit = true;
                var loading = $ionicPopup.show({
                    template: '<h1>Carregando...</h1>',
                    cssClass: 'popup-default',
                });

                $http({
                    method: "GET",
                    url: SERVER_URL + "services/budgets.php?id=" + $stateParams.budgetId,
                    cache: false
                }).then(
                    function ( response )
                    {

                        if ( !response.data.success )
                            return loading.close();


                        $rootScope.budget = response.data.data || {
                            products: [ {} ],
                            labors  : [ {} ]
                        };

                        $(function() {
                            $('.p_price').maskMoney('mask',{
                                decimal: ',',
                                thousands: '.',
                                precision: 2,
                                prefix: 'R$ ',
                                allowZero: true
                            });
                        });
                        $scope.baction($scope,$stateParams);

                        loading.close();
                    }
                );
            }
            else
            {
                $scope.baction($scope,$stateParams);
            }
        }


    })
    .controller('budgetPdfCtr', function ($rootScope, $scope,$stateParams,SERVER_URL,$state,$http,$ionicPopup,$ionicHistory) {
        $scope.budgetid=0;

        var loading = $ionicPopup.show({
            template: '<h1>Carregando PDF...</h1>',
            cssClass: 'popup-default',
        });

        $scope.windowWidth = $( window ).width();
        $scope.budgetid = $stateParams.budgetId;

        if ( $rootScope.isIOS )
            $http.get( SERVER_URL+'services/budgetPDF.php?id='+$scope.budgetid+'&return=filepath' )
                .then(function (resp) {
                    $scope.pdf = SERVER_URL+'services/' + resp.data.trim();
                });
        else
            $scope.pdf = SERVER_URL+'services/budgetPDF.php?id='+$scope.budgetid+'&debug';

        $scope.msgShare = 'Olá, segue o orçamento solicitado, criado em http://refriplay.com.br';
        $scope.subjectShare = 'Orçamento Refriplay '+$scope.budgetid;

        $scope.back = function(){
            $state.go('tab.budget', {}, { reload: true });
        };

        $scope.openShare = function()
        {
            setTimeout(function(){
                window.plugins.socialsharing.share($scope.msgShare, $scope.subjectShare, $scope.pdf);
			}, 0);
        }

        setTimeout(function(){
            loading.close();
        },1500);

    })

angular.module('fortinsocial.services', [])
    .factory('userauth', function ($http, SERVER_URL,md5) {
        return {
            dologin: function (email, password) {
                var url = SERVER_URL + "services/checkEmailLogin.php?userpwd=" + md5.createHash(password) + "&useremail=" + email;
                return $http.get(url);
            },
            dologinSemMd5: function (email, password) {
                var url = SERVER_URL + "services/checkEmailLogin.php?userpwd=" + password + "&useremail=" + email;
                return $http.get(url);
            }
        };
    })
    .factory('fbauth', function ($http, SERVER_URL) {
        return {
            dologin: function (email, fbId) {
                var url = SERVER_URL + "services/checkEmailLoginFB.php?FbUserId=" + fbId + "&useremail=" + email;
                return $http.get(url);
            }
        };
    })
    .factory('usersignup', function ($http, SERVER_URL,md5) {
        return {
            signup: function (email, password, name) {
                var url = SERVER_URL + "services/signupEmail.php";

                return $http({
                    method: "POST",
                    url: url,
                    data: {
                        'password': md5.createHash(password),
                        'email': email,
                        'name': name
                    }
                });

            }
        };
    })
    .factory('usersignupfb', function ($http, SERVER_URL) {
        return {
            signup: function (email, fbId, name) {
                var url = SERVER_URL + "services/signupFB.php";

                return $http({
                    method: "POST",
                    url: url,
                    data: {
                        'fbId': fbId,
                        'email': email,
                        'name': name
                    }
                });

            }
        };
    })
    .factory('userpass', function ($http, SERVER_URL) {
        return {
            forgotPass: function (email) {
                var url = SERVER_URL + "services/recoveryPass.php";

                return $http({
                    method: "POST",
                    url: url,
                    data: {
                        'email': email
                    }
                });
            }
        };
    })
    .factory('userChangePass', function ($http, SERVER_URL) {
        return {
            changePass: function (code,password) {
                var url = SERVER_URL + "services/changePass.php";

                return $http({
                    method: "POST",
                    url: url,
                    data: {
                        'code': code,
                        'password':password
                    }
                });
            }
        };
    })
    .factory('checkinplaces', function ($http, GOOGLE_KEY) {
        return {
            getnearbyplaces: function (curr_lat, curr_lng) {
                var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + curr_lat + "," + curr_lng + "&key=" + GOOGLE_KEY;
                return $http.get(url);
            }
        };
    })
    .factory('addnewpost', function ($http, SERVER_URL) {
        return {
            addnewpost: function (post_text, location, userid,feed_view,flagged) {

                var url = SERVER_URL + "services/addNewPost.php";
                return $http({
                    url: url,
                    method: "POST",
                    data: {
                        'post_text': post_text,
                        'location': location,
                        'userid': userid,
                        'feed_view':feed_view,
                        'flagged':flagged
                    }
                });
            }
        };
    })

    .factory('Comments', function($http, SERVER_URL) {
        return {
            getCommentsByPost: function(postid) {
                var url = SERVER_URL + "services/getCommentsByPost.php?postId=" + postid;
                return $http.get(url);
            }
        }
    })
    .factory('getuserfeeds', function ($http, SERVER_URL) {
        return {
            getuserfeeds: function (userid, page,hashtagid) {
                var url = SERVER_URL + "services/getFeedsForUser.php?page=" + page + "&userid=" + userid + "&hashtagid="+hashtagid;
                return $http.get(url);
            },
            getuserfeedsAll: function (userid, page) {
                var url = SERVER_URL + "services/getFeedsForUser.php?page=" + page + "&userid=" + userid + "&hashtagid=0&all="+true;
                return $http.get(url);
            },
            getpostdetails: function ( postId, userId ) {
                var url = SERVER_URL + "services/getPostDetails.php?postId=" + postId + "&userId=" + userId ;

                return $http.get(url);
            }
        };
    })

    .factory('addpostlike', function ($http, SERVER_URL) {
        return {
            addpostlike: function (userid, postid) {
                var url = SERVER_URL + "services/addPostLike.php?post_id=" + postid + "&user_id=" + userid;
                return $http.get(url);
            }
        };
    })

    .factory('addpostcomment', function ($http, SERVER_URL) {
        return {
            addpostcomment: function (userid, postid, comment_text) {
                var url = SERVER_URL + "services/addPostComment.php?post_id=" + postid + "&user_id=" + userid + "&comment_text=" + comment_text;
                return $http.get(url);
            }
        };
    })

    .factory('addfollowing', function ($http, SERVER_URL) {
        return {
            addfollowing: function (userid, followingid) {
                var url = SERVER_URL + "services/addFollowing.php?userid=" + userid + "&followingid=" + followingid;
                return $http.get(url);
            }
        };
    })

    .factory('unfollow', function ($http, SERVER_URL) {
        return {
            unfollow: function (userid, followingid) {
                var url = SERVER_URL + "services/unfollow.php?userid=" + userid + "&followingid=" + followingid;
                return $http.get(url);
            }
        };
    })

    .factory('getfriends', function ($http, SERVER_URL) {
        return {
            getfriends: function (userid, type) {
                var url = '';
                if (type === 'following') {
                    url = SERVER_URL + "services/getFollowingByUser.php?userid=" + userid;
                } else if (type === 'followers') {
                    url = SERVER_URL + "services/getFollowersForUser.php?userid=" + userid;
                } else if (type === 'suggestions') {
                    url = SERVER_URL + "services/getRandomUsers.php?userid=" + userid;
                }

                return $http.get(url);
            }
        };
    })

    .factory('updateprofile', function ($http, SERVER_URL) {
        return {
            updateprofile: function (username, password, feed_view, gender, userid,cpf) {
                var url = SERVER_URL + "services/updateProfile.php";
                return $http({
                    url: url,
                    method: "POST",
                    data: {
                        'full_name': username,
                        'password': password,
                        'feed_view': feed_view,
                        'gender': gender,
                        'userid': userid,
                        'cpf':cpf
                    }
                });
            },
            updateprofileinfo: function (field,value,userid) {
                var url = SERVER_URL + "services/updateProfileInfo.php";
                return $http({
                    url: url,
                    method: "POST",
                    data: {
                        'field': field,
                        'info': value,
                        'userid': userid
                    }
                });
            },
            getFieldInfo: function(field)
            {
                var url = SERVER_URL + "services/getEditAccountInfos.php?field=" + field;
                return $http.get(url);
            },
            getProfileMetrics: function(userid){
                var url = SERVER_URL + "services/getProfileMetrics.php?userid=" + userid;
                return $http.get(url);
            }
        };
    })

    .factory('updatepushid', function ($http, SERVER_URL) {
        return {
            updatepushid: function (pushid, userid) {

                var url = SERVER_URL + "services/updatePushId.php";
                return $http({
                    url: url,
                    method: "POST",
                    data: {
                        'pushid': pushid,
                        'userid': userid
                    }
                });
            }
        };
    })

    .factory('getfeedprofile', function ($http, SERVER_URL) {
        return {
            getfeedprofile: function (userid) {
                var url = SERVER_URL + "services/getFeedProfileForUser.php?&userid=" + userid;
                return $http.get(url);
            }
        };
    })

    .factory('getotheruserprofile', function ($http, SERVER_URL) {
        return {
            getotheruserprofile: function (userid,loggedinuserid) {
                var url = SERVER_URL + "services/getFeedProfileForOtherUser.php?&userid=" + userid +"&loggedinuserid="+loggedinuserid;
                return $http.get(url);
            }
        };
    })

    .factory('getselffeed', function ($http, SERVER_URL) {
        return {
            getselffeed: function (userid, page) {
                var url = SERVER_URL + "services/getFeedsForUser.php?page=" + page + "&userid=" + userid + "&self=yes";
                return $http.get(url);
            }
        };
    })
    .factory('getuserphotos', function ($http, SERVER_URL) {
        return {
            getuserphotos: function (userid) {
                var url = SERVER_URL + "services/getPhotosForUser.php?userid=" + userid;
                return $http.get(url);
            }
        };
    })
    .factory('gettrendinghashtags', function ($http, SERVER_URL) {
        return {
            gettrendinghashtags: function (userid) {
                var url = SERVER_URL + "services/getTrendingHashtags.php";
                return $http.get(url);
            }
        };
    })
    .factory('getstates', function ($http, SERVER_URL) {
        return {
            getstates: function (lat,lng) {
                var url = SERVER_URL + "services/getStates.php?lat="+lat+"&lng="+lng;
                return $http.get(url);
            }
        };
    })
    .factory('setFeedState', function ($http, SERVER_URL) {
        return {
            setFeedState: function (state,userId) {
                var url = SERVER_URL + "services/setFeedState.php";
                return $http({
                    url: url,
                    method: "POST",
                    data: {
                        'state': state,
                        'userId': userId
                    }
                });
            }
        };
    })
    .factory('deletepost', function ($http, SERVER_URL) {
            return {
                deletepost: function (postid) {
                    var url = SERVER_URL + "services/deletePostByUser.php?postid="+postid;
                    return $http.get(url);
                }
            };
    })
    .factory('adv_report', function ($http, SERVER_URL) {
        return {
            count: function (advid,userid,state_view) {
                var url = SERVER_URL + "services/adv_report.php?userid="+userid+"&state_view="+state_view+"&advid="+advid;
                return $http.get(url);
            }
        };
    })
    .factory('Insurance', function($http, SERVER_URL) {
        return {
            getText : function() {
                var url = SERVER_URL + "services/getInsuranceText.php";
                return $http.get(url);                
            }
        }
    })
    .factory('Tools', function ($http, SERVER_URL) {
        return {
            getTools: function () {
                var url = SERVER_URL + "services/getTools.php";
                return $http.get(url);
            }
        };
    })
    .factory('ErrorCategory', function ($http, SERVER_URL) {
        return {
            getErrorCategory: function () {
                var url = SERVER_URL + "services/getErrorCategory.php";
                return $http.get(url);
            }
        };
    })
    .factory('Marks', function ($http, SERVER_URL) {
        return {
            getMarks: function () {
                var url = SERVER_URL + "services/getMarks.php";
                return $http.get(url);
            }
        };
    })
    .factory('Products', function ($http, SERVER_URL) {
        return {
            getProducts: function (mark_id) {
                var url = SERVER_URL + "services/getProducts.php?mark_id="+mark_id;
                return $http.get(url);
            },
            getProductsErrorList: function (products_id) {
                var url = SERVER_URL + "services/getErrorList.php?products_id="+products_id;
                return $http.get(url);
            },
        };
    })
    .factory('Error', function ($http, SERVER_URL) {
        return {
            getError: function (error_id) {
                var url = SERVER_URL + "services/getErrorDetail.php?id="+error_id;
                return $http.get(url);
            },
            getErrorAll: function (error_id) {
                var url = SERVER_URL + "services/getErrorListAll.php";
                return $http.get(url);
            },
        };
    })
    .factory('Solutions', function ($http, SERVER_URL) {
        return {
            getSolutions: function (error_id) {
                var url = SERVER_URL + "services/getSolution.php?error_list_id="+error_id;
                return $http.get(url);
            },
            addSolution: function (user_id,error_list_id,descripition) {
                var url = SERVER_URL + "services/addSolution.php";
                return $http({
                    url: url,
                    method: "POST",
                    data: {
                        'user_id': user_id,
                        'descripition': descripition,
                        'error_list_id': error_list_id
                    }
                });
            },
            disableSolution: function (solution_id) {
                var url = SERVER_URL + "services/disableSolution.php?id="+solution_id;
                return $http.get(url);
            }
        };
    })
    .factory('Likes', function ($http, SERVER_URL) {
        return {
            getLikes: function (solutions_id) {
                var url = SERVER_URL + "services/getLikes.php?solutions_id="+solutions_id;
                console.log("URL DA PUTARIA",url);
                return $http.get(url);
            },
            addLike: function (user_id,like,solutions_id) {
                var url = SERVER_URL + "services/addLike.php";
                return $http({
                    url: url,
                    method: "POST",
                    data: {
                        'user_id': user_id,
                        'like': like,
                        'solutions_id': solutions_id
                    }
                });
            }
        };
    })
    .factory('Notifications', function($http, SERVER_URL) {
        return {
            getUserNotifications : function (userid, page) {
                if(page != 0) {
                    var url = SERVER_URL + "services/getNotificationsByUser.php?userid=" + userid + "&page=" + page;    
                } else {
                    var url = SERVER_URL + "services/getNotificationsByUser.php?userid=" + userid;
                }
                return $http.get(url);
            },
            changeNotificationStatus : function (notificationid) {
                var url = SERVER_URL + "services/changeNotificationStatus.php?notificationid=" + notificationid;
                return $http.get(url);
            },
            getQntNotifications : function (userid) {
                var url = SERVER_URL + "services/getQntNotifications.php?userid=" + userid;
                return $http.get(url);
            },
            readAll : function (userid) {
                var url = SERVER_URL + "services/updateAllNotifications.php?userid="+userid;
                return $http.get(url);
            }
        };
    })
;

;(function (window, document, $, undefined) {
    'use strict';


    if ( !$ ) {
        return;
    }


    if ( $.fn.fancybox ) {

        $.error('fancyBox already initialized');

        return;
    }


    var defaults = {

        loop : false,

        margin : [44, 0],

        gutter : 50,

        keyboard : true,

        arrows : true,

        infobar : false,

        toolbar : true,

        buttons : [
            'slideShow',
            'fullScreen',
            'thumbs',
            'close'
        ],

        idleTime : 4,

        smallBtn : 'auto',

        protect : false,

        modal : false,

        image : {

            preload : "auto",

        },

        ajax : {

            settings : {

                data : {
                    fancybox : true
                }
            }

        },

        iframe : {

            tpl : '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen allowtransparency="true" src=""></iframe>',

            preload : true,

            css : {},

            attr : {
                scrolling : 'auto'
            }

        },

        animationEffect : "zoom",

        animationDuration : 366,

        zoomOpacity : 'auto',

        transitionEffect : "fade",

        transitionDuration : 366,

        slideClass : '',

        baseClass : '',

        baseTpl	:
            '<div class="fancybox-container" role="dialog" tabindex="-1">' +
                '<div class="fancybox-bg"></div>' +
                '<div class="fancybox-inner">' +
                    '<div class="fancybox-infobar">' +
                        '<button data-fancybox-prev title="{{PREV}}" class="fancybox-button fancybox-button--left"></button>' +
                        '<div class="fancybox-infobar__body">' +
                            '<span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span>' +
                        '</div>' +
                        '<button data-fancybox-next title="{{NEXT}}" class="fancybox-button fancybox-button--right"></button>' +
                    '</div>' +
                    '<div class="fancybox-toolbar">' +
                        '{{BUTTONS}}' +
                    '</div>' +
                    '<div class="fancybox-navigation">' +
                        '<button data-fancybox-prev title="{{PREV}}" class="fancybox-arrow fancybox-arrow--left" />' +
                        '<button data-fancybox-next title="{{NEXT}}" class="fancybox-arrow fancybox-arrow--right" />' +
                    '</div>' +
                    '<div class="fancybox-stage"></div>' +
                    '<div class="fancybox-caption-wrap">' +
                        '<div class="fancybox-caption"></div>' +
                    '</div>' +
                '</div>' +
            '</div>',

        spinnerTpl : '<div class="fancybox-loading"></div>',

        errorTpl : '<div class="fancybox-error"><p>{{ERROR}}<p></div>',

        btnTpl : {
            slideShow  : '<button data-fancybox-play class="fancybox-button fancybox-button--play" title="{{PLAY_START}}"></button>',
            fullScreen : '<button data-fancybox-fullscreen class="fancybox-button fancybox-button--fullscreen" title="{{FULL_SCREEN}}"></button>',
            thumbs     : '<button data-fancybox-thumbs class="fancybox-button fancybox-button--thumbs" title="{{THUMBS}}"></button>',
            close      : '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}"></button>',

            smallBtn   : '<button data-fancybox-close class="fancybox-close-small" title="{{CLOSE}}"></button>'
        },

        parentEl : 'body',



        autoFocus : true,

        backFocus : true,

        trapFocus : true,



        fullScreen : {
            autoStart : false,
        },

        touch : {
            vertical : true,  
            momentum : true   
        },

        hash : null,

        media : {},

        slideShow : {
            autoStart : false,
            speed     : 4000
        },

        thumbs : {
            autoStart   : false,   
            hideOnClose : true     
        },



        onInit       : $.noop,  

        beforeLoad   : $.noop,  
        afterLoad    : $.noop,  

        beforeShow   : $.noop,  
        afterShow    : $.noop,  

        beforeClose  : $.noop,  
        afterClose   : $.noop,  

        onActivate   : $.noop,  
        onDeactivate : $.noop,  




        clickContent : function( current, event ) {
            return current.type === 'image' ? 'zoom' : false;
        },

        clickSlide : 'close',

        clickOutside : 'close',

        dblclickContent : false,
        dblclickSlide   : false,
        dblclickOutside : false,



        mobile : {
            clickContent : function( current, event ) {
                return current.type === 'image' ? 'toggleControls' : false;
            },
            clickSlide : function( current, event ) {
                return current.type === 'image' ? 'toggleControls' : "close";
            },
            dblclickContent : function( current, event ) {
                return current.type === 'image' ? 'zoom' : false;
            },
            dblclickSlide : function( current, event ) {
                return current.type === 'image' ? 'zoom' : false;
            }
        },



        lang : 'en',
        i18n : {
            'en' : {
                CLOSE       : 'Close',
                NEXT        : 'Next',
                PREV        : 'Previous',
                ERROR       : 'The requested content cannot be loaded. <br/> Please try again later.',
                PLAY_START  : 'Start slideshow',
                PLAY_STOP   : 'Pause slideshow',
                FULL_SCREEN : 'Full screen',
                THUMBS      : 'Thumbnails'
            },
            'de' : {
                CLOSE       : 'Schliessen',
                NEXT        : 'Weiter',
                PREV        : 'Zurück',
                ERROR       : 'Die angeforderten Daten konnten nicht geladen werden. <br/> Bitte versuchen Sie es später nochmal.',
                PLAY_START  : 'Diaschau starten',
                PLAY_STOP   : 'Diaschau beenden',
                FULL_SCREEN : 'Vollbild',
                THUMBS      : 'Vorschaubilder'
            }
        }

    };


    var $W = $(window);
    var $D = $(document);

    var called = 0;



    var isQuery = function ( obj ) {
        return obj && obj.hasOwnProperty && obj instanceof $;
    };



    var requestAFrame = (function () {
        return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                function (callback) {
                    return window.setTimeout(callback, 1000 / 60);
                };
    })();



    var transitionEnd = (function () {
        var t, el = document.createElement("fakeelement");

        var transitions = {
            "transition"      : "transitionend",
            "OTransition"     : "oTransitionEnd",
            "MozTransition"   : "transitionend",
            "WebkitTransition": "webkitTransitionEnd"
        };

        for (t in transitions) {
            if (el.style[t] !== undefined){
                return transitions[t];
            }
        }
    })();



    var forceRedraw = function( $el ) {
        return ( $el && $el.length && $el[0].offsetHeight );
    };



    var FancyBox = function( content, opts, index ) {
        var self = this;

        self.opts  = $.extend( true, { index : index }, defaults, opts || {} );

        if ( opts && $.isArray( opts.buttons ) ) {
            self.opts.buttons = opts.buttons;
        }

        self.id    = self.opts.id || ++called;
        self.group = [];

        self.currIndex = parseInt( self.opts.index, 10 ) || 0;
        self.prevIndex = null;

        self.prevPos = null;
        self.currPos = 0;

        self.firstRun = null;

        self.createGroup( content );

        if ( !self.group.length ) {
            return;
        }

        self.$lastFocus = $(document.activeElement).blur();

        self.slides = {};

        self.init( content );

    };

    $.extend(FancyBox.prototype, {


        init : function() {
            var self = this;

            var testWidth, $container, buttonStr;

            var firstItemOpts = self.group[ self.currIndex ].opts;

            self.scrollTop  = $D.scrollTop();
            self.scrollLeft = $D.scrollLeft();



            if ( !$.fancybox.getInstance() && !$.fancybox.isMobile && $( 'body' ).css('overflow') !== 'hidden' ) {
                testWidth = $( 'body' ).width();

                $( 'html' ).addClass( 'fancybox-enabled' );

                testWidth = $( 'body' ).width() - testWidth;

                if ( testWidth > 1 ) {
                    $( 'head' ).append( '<style id="fancybox-style-noscroll" type="text/css">.compensate-for-scrollbar, .fancybox-enabled body { margin-right: ' + testWidth + 'px; }</style>' );
                }
            }



            buttonStr = '';

            $.each( firstItemOpts.buttons, function( index, value ) {
                buttonStr += ( firstItemOpts.btnTpl[ value ] || '' );
            });

            $container = $( self.translate( self, firstItemOpts.baseTpl.replace( '\{\{BUTTONS\}\}', buttonStr ) ) )
                .addClass( 'fancybox-is-hidden' )
                .attr('id', 'fancybox-container-' + self.id)
                .addClass( firstItemOpts.baseClass )
                .data( 'FancyBox', self )
                .prependTo( firstItemOpts.parentEl );

            self.$refs = {
                container : $container
            };

            [ 'bg', 'inner', 'infobar', 'toolbar', 'stage', 'caption' ].forEach(function(item) {
                self.$refs[ item ] = $container.find( '.fancybox-' + item );
            });

            if ( !firstItemOpts.arrows || self.group.length < 2 ) {
                $container.find('.fancybox-navigation').remove();
            }

            if ( !firstItemOpts.infobar ) {
                self.$refs.infobar.remove();
            }

            if ( !firstItemOpts.toolbar ) {
                self.$refs.toolbar.remove();
            }

            self.trigger( 'onInit' );

            self.activate();

            self.jumpTo( self.currIndex );
        },



        translate : function( obj, str ) {
            var arr = obj.opts.i18n[ obj.opts.lang ];

            return str.replace(/\{\{(\w+)\}\}/g, function(match, n) {
                var value = arr[n];

                if ( value === undefined ) {
                    return match;
                }

                return value;
            });
        },


        createGroup : function ( content ) {
            var self  = this;
            var items = $.makeArray( content );

            $.each(items, function( i, item ) {
                var obj  = {},
                    opts = {},
                    data = [],
                    $item,
                    type,
                    src,
                    srcParts;


                if ( $.isPlainObject( item ) ) {


                    obj  = item;
                    opts = item.opts || item;

                } else if ( $.type( item ) === 'object' && $( item ).length ) {


                    $item = $( item );
                    data  = $item.data();

                    opts = 'options' in data ? data.options : {};
                    opts = $.type( opts ) === 'object' ? opts : {};

                    obj.src  = 'src' in data ? data.src : ( opts.src || $item.attr( 'href' ) );

                    [ 'width', 'height', 'thumb', 'type', 'filter' ].forEach(function(item) {
                        if ( item in data ) {
                            opts[ item ] = data[ item ];
                        }
                    });

                    if ( 'srcset' in data ) {
                        opts.image = { srcset : data.srcset };
                    }

                    opts.$orig = $item;

                    if ( !obj.type && !obj.src ) {
                        obj.type = 'inline';
                        obj.src  = item;
                    }

                } else {


                    obj = {
                        type : 'html',
                        src  : item + ''
                    };

                }

                obj.opts = $.extend( true, {}, self.opts, opts );

                if ( $.fancybox.isMobile ) {
                    obj.opts = $.extend( true, {}, obj.opts, obj.opts.mobile );
                }



                type = obj.type || obj.opts.type;
                src  = obj.src || '';

                if ( !type && src ) {
                    if ( src.match(/(^data:image\/[a-z0-9+\/=]*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg|ico)((\?|#).*)?$)/i) ) {
                        type = 'image';

                    } else if ( src.match(/\.(pdf)((\?|#).*)?$/i) ) {
                        type = 'pdf';

                    } else if ( src.charAt(0) === '#' ) {
                        type = 'inline';
                    }
                }

                obj.type = type;



                obj.index = self.group.length;

                if ( obj.opts.$orig && !obj.opts.$orig.length ) {
                    delete obj.opts.$orig;
                }

                if ( !obj.opts.$thumb && obj.opts.$orig ) {
                    obj.opts.$thumb = obj.opts.$orig.find( 'img:first' );
                }

                if ( obj.opts.$thumb && !obj.opts.$thumb.length ) {
                    delete obj.opts.$thumb;
                }

                if ( $.type( obj.opts.caption ) === 'function' ) {
                    obj.opts.caption = obj.opts.caption.apply( item, [ self, obj ] );

                } else if ( 'caption' in data ) {
                    obj.opts.caption = data.caption;
                }

                obj.opts.caption = obj.opts.caption === undefined ? '' : obj.opts.caption + '';

                if ( type === 'ajax' ) {
                    srcParts = src.split(/\s+/, 2);

                    if ( srcParts.length > 1 ) {
                        obj.src = srcParts.shift();

                        obj.opts.filter = srcParts.shift();
                    }
                }

                if ( obj.opts.smallBtn == 'auto' ) {

                    if ( $.inArray( type, ['html', 'inline', 'ajax'] ) > -1 ) {
                        obj.opts.toolbar  = false;
                        obj.opts.smallBtn = true;

                    } else {
                        obj.opts.smallBtn = false;
                    }

                }

                if ( type === 'pdf' ) {
                    obj.type = 'iframe';

                    obj.opts.iframe.preload = false;
                }

                if ( obj.opts.modal ) {

                    obj.opts = $.extend(true, obj.opts, {
                        infobar : 0,
                        toolbar : 0,

                        smallBtn : 0,

                        keyboard : 0,

                        slideShow  : 0,
                        fullScreen : 0,
                        thumbs     : 0,
                        touch      : 0,

                        clickContent    : false,
                        clickSlide      : false,
                        clickOutside    : false,
                        dblclickContent : false,
                        dblclickSlide   : false,
                        dblclickOutside : false
                    });

                }


                self.group.push( obj );

            });

        },



        addEvents : function() {
            var self = this;

            self.removeEvents();

            self.$refs.container.on('click.fb-close', '[data-fancybox-close]', function(e) {
                e.stopPropagation();
                e.preventDefault();

                self.close( e );

            }).on( 'click.fb-prev touchend.fb-prev', '[data-fancybox-prev]', function(e) {
                e.stopPropagation();
                e.preventDefault();

                self.previous();

            }).on( 'click.fb-next touchend.fb-next', '[data-fancybox-next]', function(e) {
                e.stopPropagation();
                e.preventDefault();

                self.next();

            });


            $W.on('orientationchange.fb resize.fb', function(e) {

                if ( e && e.originalEvent && e.originalEvent.type === "resize" ) {

                    requestAFrame(function() {
                        self.update();
                    });

                } else {

                    self.$refs.stage.hide();

                    setTimeout(function() {
                        self.$refs.stage.show();

                        self.update();
                    }, 500);

                }

            });

            $D.on('focusin.fb', function(e) {
                var instance = $.fancybox ? $.fancybox.getInstance() : null;

                if ( instance.isClosing || !instance.current || !instance.current.opts.trapFocus || $( e.target ).hasClass( 'fancybox-container' ) || $( e.target ).is( document ) ) {
                    return;
                }

                if ( instance && $( e.target ).css( 'position' ) !== 'fixed' && !instance.$refs.container.has( e.target ).length ) {
                    e.stopPropagation();

                    instance.focus();

                    $W.scrollTop( self.scrollTop ).scrollLeft( self.scrollLeft );
                }
            });


            $D.on('keydown.fb', function (e) {
                var current = self.current,
                    keycode = e.keyCode || e.which;

                if ( !current || !current.opts.keyboard ) {
                    return;
                }

                if ( $(e.target).is('input') || $(e.target).is('textarea') ) {
                    return;
                }

                if ( keycode === 8 || keycode === 27 ) {
                    e.preventDefault();

                    self.close( e );

                    return;
                }

                if ( keycode === 37 || keycode === 38 ) {
                    e.preventDefault();

                    self.previous();

                    return;
                }

                if ( keycode === 39 || keycode === 40 ) {
                    e.preventDefault();

                    self.next();

                    return;
                }

                self.trigger('afterKeydown', e, keycode);
            });


            if ( self.group[ self.currIndex ].opts.idleTime ) {
                self.idleSecondsCounter = 0;

                $D.on('mousemove.fb-idle mouseenter.fb-idle mouseleave.fb-idle mousedown.fb-idle touchstart.fb-idle touchmove.fb-idle scroll.fb-idle keydown.fb-idle', function() {
                    self.idleSecondsCounter = 0;

                    if ( self.isIdle ) {
                        self.showControls();
                    }

                    self.isIdle = false;
                });

                self.idleInterval = window.setInterval(function() {

                    self.idleSecondsCounter++;

                    if ( self.idleSecondsCounter >= self.group[ self.currIndex ].opts.idleTime ) {
                        self.isIdle = true;
                        self.idleSecondsCounter = 0;

                        self.hideControls();
                    }

                }, 1000);
            }

        },



        removeEvents : function () {
            var self = this;

            $W.off( 'orientationchange.fb resize.fb' );
            $D.off( 'focusin.fb keydown.fb .fb-idle' );

            this.$refs.container.off( '.fb-close .fb-prev .fb-next' );

            if ( self.idleInterval ) {
                window.clearInterval( self.idleInterval );

                self.idleInterval = null;
            }
        },



        previous : function( duration ) {
            return this.jumpTo( this.currPos - 1, duration );
        },



        next : function( duration ) {
            return this.jumpTo( this.currPos + 1, duration );
        },



        jumpTo : function ( pos, duration, slide ) {
            var self = this,
                firstRun,
                loop,
                current,
                previous,
                canvasWidth,
                currentPos,
                transitionProps;

            var groupLen = self.group.length;

            if ( self.isSliding || self.isClosing || ( self.isAnimating && self.firstRun ) ) {
                return;
            }

            pos  = parseInt( pos, 10 );
            loop = self.current ? self.current.opts.loop : self.opts.loop;

            if ( !loop && ( pos < 0 || pos >= groupLen ) ) {
                return false;
            }

            firstRun = self.firstRun = ( self.firstRun === null );

            if ( groupLen < 2 && !firstRun && !!self.isSliding ) {
                return;
            }

            previous = self.current;

            self.prevIndex = self.currIndex;
            self.prevPos   = self.currPos;

            current = self.createSlide( pos );

            if ( groupLen > 1 ) {
                if ( loop || current.index > 0 ) {
                    self.createSlide( pos - 1 );
                }

                if ( loop || current.index < groupLen - 1 ) {
                    self.createSlide( pos + 1 );
                }
            }

            self.current   = current;
            self.currIndex = current.index;
            self.currPos   = current.pos;

            self.trigger( 'beforeShow', firstRun );

            self.updateControls();

            currentPos = $.fancybox.getTranslate( current.$slide );

            current.isMoved        = ( currentPos.left !== 0 || currentPos.top !== 0 ) && !current.$slide.hasClass( 'fancybox-animated' );
            current.forcedDuration = undefined;

            if ( $.isNumeric( duration ) ) {
                current.forcedDuration = duration;
            } else {
                duration = current.opts[ firstRun ? 'animationDuration' : 'transitionDuration' ];
            }

            duration = parseInt( duration, 10 );

            if ( firstRun ) {

                if ( current.opts.animationEffect && duration ) {
                    self.$refs.container.css( 'transition-duration', duration + 'ms' );
                }

                self.$refs.container.removeClass( 'fancybox-is-hidden' );

                forceRedraw( self.$refs.container );

                self.$refs.container.addClass( 'fancybox-is-open' );

                current.$slide.addClass( 'fancybox-slide--current' );

                self.loadSlide( current );

                self.preload();

                return;
            }

            $.each(self.slides, function( index, slide ) {
                $.fancybox.stop( slide.$slide );
            });

            current.$slide.removeClass( 'fancybox-slide--next fancybox-slide--previous' ).addClass( 'fancybox-slide--current' );

            if ( current.isMoved ) {
                canvasWidth = Math.round( current.$slide.width() );

                $.each(self.slides, function( index, slide ) {
                    var pos = slide.pos - current.pos;

                    $.fancybox.animate( slide.$slide, {
                        top  : 0,
                        left : ( pos * canvasWidth ) + ( pos * slide.opts.gutter )
                    }, duration, function() {

                        slide.$slide.removeAttr('style').removeClass( 'fancybox-slide--next fancybox-slide--previous' );

                        if ( slide.pos === self.currPos ) {
                            current.isMoved = false;

                            self.complete();
                        }
                    });
                });

            } else {
                self.$refs.stage.children().removeAttr( 'style' );
            }


            if ( current.isLoaded ) {
                self.revealContent( current );

            } else {
                self.loadSlide( current );
            }

            self.preload();

            if ( previous.pos === current.pos ) {
                return;
            }


            transitionProps = 'fancybox-slide--' + ( previous.pos > current.pos ? 'next' : 'previous' );

            previous.$slide.removeClass( 'fancybox-slide--complete fancybox-slide--current fancybox-slide--next fancybox-slide--previous' );

            previous.isComplete = false;

            if ( !duration || ( !current.isMoved && !current.opts.transitionEffect ) ) {
                return;
            }

            if ( current.isMoved ) {
                previous.$slide.addClass( transitionProps );

            } else {

                transitionProps = 'fancybox-animated ' + transitionProps + ' fancybox-fx-' + current.opts.transitionEffect;

                $.fancybox.animate( previous.$slide, transitionProps, duration, function() {
                    previous.$slide.removeClass( transitionProps ).removeAttr( 'style' );
                });

            }

        },



        createSlide : function( pos ) {

            var self = this;
            var $slide;
            var index;

            index = pos % self.group.length;
            index = index < 0 ? self.group.length + index : index;

            if ( !self.slides[ pos ] && self.group[ index ] ) {
                $slide = $('<div class="fancybox-slide"></div>').appendTo( self.$refs.stage );

                self.slides[ pos ] = $.extend( true, {}, self.group[ index ], {
                    pos      : pos,
                    $slide   : $slide,
                    isLoaded : false,
                });

                self.updateSlide( self.slides[ pos ] );
            }

            return self.slides[ pos ];
        },



        scaleToActual : function( x, y, duration ) {

            var self = this;

            var current = self.current;
            var $what   = current.$content;

            var imgPos, posX, posY, scaleX, scaleY;

            var canvasWidth  = parseInt( current.$slide.width(), 10 );
            var canvasHeight = parseInt( current.$slide.height(), 10 );

            var newImgWidth  = current.width;
            var newImgHeight = current.height;

            if ( !( current.type == 'image' && !current.hasError) || !$what || self.isAnimating) {
                return;
            }

            $.fancybox.stop( $what );

            self.isAnimating = true;

            x = x === undefined ? canvasWidth  * 0.5  : x;
            y = y === undefined ? canvasHeight * 0.5  : y;

            imgPos = $.fancybox.getTranslate( $what );

            scaleX  = newImgWidth  / imgPos.width;
            scaleY  = newImgHeight / imgPos.height;

            posX = ( canvasWidth * 0.5  - newImgWidth * 0.5 );
            posY = ( canvasHeight * 0.5 - newImgHeight * 0.5 );

            if ( newImgWidth > canvasWidth ) {
                posX = imgPos.left * scaleX - ( ( x * scaleX ) - x );

                if ( posX > 0 ) {
                    posX = 0;
                }

                if ( posX <  canvasWidth - newImgWidth ) {
                    posX = canvasWidth - newImgWidth;
                }
            }

            if ( newImgHeight > canvasHeight) {
                posY = imgPos.top  * scaleY - ( ( y * scaleY ) - y );

                if ( posY > 0 ) {
                    posY = 0;
                }

                if ( posY <  canvasHeight - newImgHeight ) {
                    posY = canvasHeight - newImgHeight;
                }
            }

            self.updateCursor( newImgWidth, newImgHeight );

            $.fancybox.animate( $what, {
                top    : posY,
                left   : posX,
                scaleX : scaleX,
                scaleY : scaleY
            }, duration || 330, function() {
                self.isAnimating = false;
            });

            if ( self.SlideShow && self.SlideShow.isActive ) {
                self.SlideShow.stop();
            }
        },



        scaleToFit : function( duration ) {

            var self = this;

            var current = self.current;
            var $what   = current.$content;
            var end;

            if ( !( current.type == 'image' && !current.hasError) || !$what || self.isAnimating ) {
                return;
            }

            $.fancybox.stop( $what );

            self.isAnimating = true;

            end = self.getFitPos( current );

            self.updateCursor( end.width, end.height );

            $.fancybox.animate( $what, {
                top    : end.top,
                left   : end.left,
                scaleX : end.width  / $what.width(),
                scaleY : end.height / $what.height()
            }, duration || 330, function() {
                self.isAnimating = false;
            });

        },


        getFitPos : function( slide ) {
            var self  = this;
            var $what = slide.$content;

            var imgWidth  = slide.width;
            var imgHeight = slide.height;

            var margin = slide.opts.margin;

            var canvasWidth, canvasHeight, minRatio, width, height;

            if ( !$what || !$what.length || ( !imgWidth && !imgHeight) ) {
                return false;
            }

            if ( $.type( margin ) === "number" ) {
                margin = [ margin, margin ];
            }

            if ( margin.length == 2 ) {
                margin = [ margin[0], margin[1], margin[0], margin[1] ];
            }

            if ( $W.width() < 800 ) {
                margin = [ 0, 0, 0, 0 ];
            }

            canvasWidth  = parseInt( self.$refs.stage.width(), 10 )  - ( margin[ 1 ] + margin[ 3 ] );
            canvasHeight = parseInt( self.$refs.stage.height(), 10 ) - ( margin[ 0 ] + margin[ 2 ] );

            minRatio = Math.min(1, canvasWidth / imgWidth, canvasHeight / imgHeight );

            width  = Math.floor( minRatio * imgWidth );
            height = Math.floor( minRatio * imgHeight );

            return {
                top    : Math.floor( ( canvasHeight - height ) * 0.5 ) + margin[ 0 ],
                left   : Math.floor( ( canvasWidth  - width )  * 0.5 ) + margin[ 3 ],
                width  : width,
                height : height
            };

        },



        update : function() {

            var self = this;

            $.each( self.slides, function( key, slide ) {
                self.updateSlide( slide );
            });

        },



        updateSlide : function( slide ) {

            var self  = this;
            var $what = slide.$content;

            if ( $what && ( slide.width || slide.height ) ) {
                $.fancybox.stop( $what );

                $.fancybox.setTranslate( $what, self.getFitPos( slide ) );

                if ( slide.pos === self.currPos ) {
                    self.updateCursor();
                }
            }

            slide.$slide.trigger( 'refresh' );

            self.trigger( 'onUpdate', slide );

        },


        updateCursor : function( nextWidth, nextHeight ) {

            var self = this;
            var isScaledDown;

            var $container = self.$refs.container.removeClass('fancybox-is-zoomable fancybox-can-zoomIn fancybox-can-drag fancybox-can-zoomOut');

            if ( !self.current || self.isClosing ) {
                return;
            }

            if ( self.isZoomable() ) {

                $container.addClass( 'fancybox-is-zoomable' );

                if ( nextWidth !== undefined && nextHeight !== undefined ) {
                    isScaledDown = nextWidth < self.current.width && nextHeight < self.current.height;

                } else {
                    isScaledDown = self.isScaledDown();
                }

                if ( isScaledDown ) {

                    $container.addClass('fancybox-can-zoomIn');

                } else {

                    if ( self.current.opts.touch ) {

                        $container.addClass('fancybox-can-drag');

                    } else {
                        $container.addClass('fancybox-can-zoomOut');
                    }

                }

            } else if ( self.current.opts.touch ) {
                $container.addClass('fancybox-can-drag');
            }

        },



        isZoomable : function() {

            var self = this;

            var current = self.current;
            var fitPos;

            if ( !current || self.isClosing ) {
                return;
            }

            if ( current.type === 'image' && current.isLoaded && !current.hasError &&
                ( current.opts.clickContent === 'zoom' || ( $.isFunction( current.opts.clickContent ) && current.opts.clickContent( current ) ===  "zoom" ) )
            ) {

                fitPos = self.getFitPos( current );

                if ( current.width > fitPos.width || current.height > fitPos.height ) {
                    return true;
                }

            }

            return false;

        },



        isScaledDown : function() {

            var self = this;

            var current = self.current;
            var $what   = current.$content;

            var rez = false;

            if ( $what ) {
                rez = $.fancybox.getTranslate( $what );
                rez = rez.width < current.width || rez.height < current.height;
            }

            return rez;

        },



        canPan : function() {

            var self = this;

            var current = self.current;
            var $what   = current.$content;

            var rez = false;

            if ( $what ) {
                rez = self.getFitPos( current );
                rez = Math.abs( $what.width() - rez.width ) > 1  || Math.abs( $what.height() - rez.height ) > 1;

            }

            return rez;

        },



        loadSlide : function( slide ) {

            var self = this, type, $slide;
            var ajaxLoad;

            if ( slide.isLoading ) {
                return;
            }

            if ( slide.isLoaded ) {
                return;
            }

            slide.isLoading = true;

            self.trigger( 'beforeLoad', slide );

            type   = slide.type;
            $slide = slide.$slide;

            $slide
                .off( 'refresh' )
                .trigger( 'onReset' )
                .addClass( 'fancybox-slide--' + ( type || 'unknown' ) )
                .addClass( slide.opts.slideClass );


            switch ( type ) {

                case 'image':

                    self.setImage( slide );

                break;

                case 'iframe':

                    self.setIframe( slide );

                break;

                case 'html':

                    self.setContent( slide, slide.src || slide.content );

                break;

                case 'inline':

                    if ( $( slide.src ).length ) {
                        self.setContent( slide, $( slide.src ) );

                    } else {
                        self.setError( slide );
                    }

                break;

                case 'ajax':

                    self.showLoading( slide );

                    ajaxLoad = $.ajax( $.extend( {}, slide.opts.ajax.settings, {
                        url : slide.src,
                        success : function ( data, textStatus ) {

                            if ( textStatus === 'success' ) {
                                self.setContent( slide, data );
                            }

                        },
                        error : function ( jqXHR, textStatus ) {

                            if ( jqXHR && textStatus !== 'abort' ) {
                                self.setError( slide );
                            }

                        }
                    }));

                    $slide.one( 'onReset', function () {
                        ajaxLoad.abort();
                    });

                break;

                default:

                    self.setError( slide );

                break;

            }

            return true;

        },



        setImage : function( slide ) {

            var self   = this;
            var srcset = slide.opts.image.srcset;

            var found, temp, pxRatio, windowWidth;

            if ( srcset ) {
                pxRatio     = window.devicePixelRatio || 1;
                windowWidth = window.innerWidth  * pxRatio;

                temp = srcset.split(',').map(function ( el ) {
            		var ret = {};

            		el.trim().split(/\s+/).forEach(function ( el, i ) {
                        var value = parseInt( el.substring(0, el.length - 1), 10 );

            			if ( i === 0 ) {
            				return ( ret.url = el );
            			}

                        if ( value ) {
                            ret.value   = value;
                            ret.postfix = el[ el.length - 1 ];
                        }

            		});

            		return ret;
            	});

                temp.sort(function (a, b) {
                  return a.value - b.value;
                });

                for ( var j = 0; j < temp.length; j++ ) {
                    var el = temp[ j ];

                    if ( ( el.postfix === 'w' && el.value >= windowWidth ) || ( el.postfix === 'x' && el.value >= pxRatio ) ) {
                        found = el;
                        break;
                    }
                }

                if ( !found && temp.length ) {
                    found = temp[ temp.length - 1 ];
                }

                if ( found ) {
                    slide.src = found.url;

                    if ( slide.width && slide.height && found.postfix == 'w' ) {
                        slide.height = ( slide.width / slide.height ) * found.value;
                        slide.width  = found.value;
                    }
                }
            }

            slide.$content = $('<div class="fancybox-image-wrap"></div>')
                .addClass( 'fancybox-is-hidden' )
                .appendTo( slide.$slide );


            if ( slide.opts.preload !== false && slide.opts.width && slide.opts.height && ( slide.opts.thumb || slide.opts.$thumb ) ) {

                slide.width  = slide.opts.width;
                slide.height = slide.opts.height;

                slide.$ghost = $('<img />')
                    .one('error', function() {

                        $(this).remove();

                        slide.$ghost = null;

                        self.setBigImage( slide );

                    })
                    .one('load', function() {

                        self.afterLoad( slide );

                        self.setBigImage( slide );

                    })
                    .addClass( 'fancybox-image' )
                    .appendTo( slide.$content )
                    .attr( 'src', slide.opts.thumb || slide.opts.$thumb.attr( 'src' ) );

            } else {

                self.setBigImage( slide );

            }

        },



        setBigImage : function ( slide ) {
            var self = this;
            var $img = $('<img />');

            slide.$image = $img
                .one('error', function() {

                    self.setError( slide );

                })
                .one('load', function() {

                    clearTimeout( slide.timouts );

                    slide.timouts = null;

                    if ( self.isClosing ) {
                        return;
                    }

                    slide.width  = this.naturalWidth;
                    slide.height = this.naturalHeight;

                    if ( slide.opts.image.srcset ) {
                        $img.attr( 'sizes', '100vw' ).attr( 'srcset', slide.opts.image.srcset );
                    }

                    self.hideLoading( slide );

                    if ( slide.$ghost ) {

                        slide.timouts = setTimeout(function() {
                            slide.timouts = null;

                            slide.$ghost.hide();

                        }, Math.min( 300, Math.max( 1000, slide.height / 1600 ) ) );

                    } else {
                        self.afterLoad( slide );
                    }

                })
                .addClass( 'fancybox-image' )
                .attr('src', slide.src)
                .appendTo( slide.$content );

            if ( $img[0].complete ) {
                  $img.trigger( 'load' );

            } else if( $img[0].error ) {
                 $img.trigger( 'error' );

            } else {

                slide.timouts = setTimeout(function() {
                    if ( !$img[0].complete && !slide.hasError ) {
                        self.showLoading( slide );
                    }

                }, 100);

            }

        },



        setIframe : function( slide ) {
            var self	= this,
                opts    = slide.opts.iframe,
                $slide	= slide.$slide,
                $iframe;

            slide.$content = $('<div class="fancybox-content' + ( opts.preload ? ' fancybox-is-hidden' : '' ) + '"></div>')
                .css( opts.css )
                .appendTo( $slide );

            $iframe = $( opts.tpl.replace(/\{rnd\}/g, new Date().getTime()) )
                .attr( opts.attr )
                .appendTo( slide.$content );

            if ( opts.preload ) {

                self.showLoading( slide );


                $iframe.on('load.fb error.fb', function(e) {
                    this.isReady = 1;

                    slide.$slide.trigger( 'refresh' );

                    self.afterLoad( slide );
                });


                $slide.on('refresh.fb', function() {
                    var $wrap = slide.$content,
                        $contents,
                        $body,
                        scrollWidth,
                        frameWidth,
                        frameHeight;

                    if ( $iframe[0].isReady !== 1 ) {
                        return;
                    }


                    try {
                        $contents = $iframe.contents();
                        $body     = $contents.find('body');

                    } catch (ignore) {}

                    if ( $body && $body.length && !( opts.css.width !== undefined && opts.css.height !== undefined ) ) {

                        scrollWidth = $iframe[0].contentWindow.document.documentElement.scrollWidth;

                        frameWidth	= Math.ceil( $body.outerWidth(true) + ( $wrap.width() - scrollWidth ) );
                        frameHeight	= Math.ceil( $body.outerHeight(true) );

                        $wrap.css({
                            'width'  : opts.css.width  === undefined ? frameWidth  + ( $wrap.outerWidth()  - $wrap.innerWidth() )  : opts.css.width,
                            'height' : opts.css.height === undefined ? frameHeight + ( $wrap.outerHeight() - $wrap.innerHeight() ) : opts.css.height
                        });

                    }

                    $wrap.removeClass( 'fancybox-is-hidden' );

                });

            } else {

                this.afterLoad( slide );

            }

            $iframe.attr( 'src', slide.src );

            if ( slide.opts.smallBtn === true ) {
                slide.$content.prepend( self.translate( slide, slide.opts.btnTpl.smallBtn ) );
            }

            $slide.one( 'onReset', function () {

                try {

                    $( this ).find( 'iframe' ).hide().attr( 'src', '//about:blank' );

                } catch ( ignore ) {}

                $( this ).empty();

                slide.isLoaded = false;

            });

        },



        setContent : function ( slide, content ) {

            var self = this;

            if ( self.isClosing ) {
                return;
            }

            self.hideLoading( slide );

            slide.$slide.empty();

            if ( isQuery( content ) && content.parent().length ) {


                content.parent( '.fancybox-slide--inline' ).trigger( 'onReset' );

                slide.$placeholder = $( '<div></div>' ).hide().insertAfter( content );

                content.css('display', 'inline-block');

            } else if ( !slide.hasError ) {

                if ( $.type( content ) === 'string' ) {
                    content = $('<div>').append( $.trim( content ) ).contents();

                    if ( content[0].nodeType === 3 ) {
                        content = $('<div>').html( content );
                    }
                }

                if ( slide.opts.filter ) {
                    content = $('<div>').html( content ).find( slide.opts.filter );
                }

            }

            slide.$slide.one('onReset', function () {

                if ( slide.$placeholder ) {
                    slide.$placeholder.after( content.hide() ).remove();

                    slide.$placeholder = null;
                }

                if ( slide.$smallBtn ) {
                    slide.$smallBtn.remove();

                    slide.$smallBtn = null;
                }

                if ( !slide.hasError ) {
                    $(this).empty();

                    slide.isLoaded = false;
                }

            });

            slide.$content = $( content ).appendTo( slide.$slide );

            if ( slide.opts.smallBtn && !slide.$smallBtn ) {
                slide.$smallBtn = $( self.translate( slide, slide.opts.btnTpl.smallBtn ) ).appendTo( slide.$content );
            }

            this.afterLoad( slide );
        },


        setError : function ( slide ) {

            slide.hasError = true;

            slide.$slide.removeClass( 'fancybox-slide--' + slide.type );

            this.setContent( slide, this.translate( slide, slide.opts.errorTpl ) );

        },



        showLoading : function( slide ) {

            var self = this;

            slide = slide || self.current;

            if ( slide && !slide.$spinner ) {
                slide.$spinner = $( self.opts.spinnerTpl ).appendTo( slide.$slide );
            }

        },


        hideLoading : function( slide ) {

            var self = this;

            slide = slide || self.current;

            if ( slide && slide.$spinner ) {
                slide.$spinner.remove();

                delete slide.$spinner;
            }

        },



        afterLoad : function( slide ) {

            var self = this;

            if ( self.isClosing ) {
                return;
            }

            slide.isLoading = false;
            slide.isLoaded  = true;

            self.trigger( 'afterLoad', slide );

            self.hideLoading( slide );

            if ( slide.opts.protect && slide.$content && !slide.hasError ) {

                slide.$content.on( 'contextmenu.fb', function( e ) {
                     if ( e.button == 2 ) {
                         e.preventDefault();
                     }

                    return true;
                });

                if ( slide.type === 'image' ) {
                    $( '<div class="fancybox-spaceball"></div>' ).appendTo( slide.$content );
                }

            }

            self.revealContent( slide );

        },



        revealContent : function( slide ) {

            var self   = this;
            var $slide = slide.$slide;

            var effect, effectClassName, duration, opacity, end, start = false;

            effect   = slide.opts[ self.firstRun ? 'animationEffect'   : 'transitionEffect' ];
            duration = slide.opts[ self.firstRun ? 'animationDuration' : 'transitionDuration' ];

            duration = parseInt( slide.forcedDuration === undefined ? duration : slide.forcedDuration, 10 );

            if ( slide.isMoved || slide.pos !== self.currPos || !duration ) {
                effect = false;
            }

            if ( effect === 'zoom' && !( slide.pos === self.currPos && duration && slide.type === 'image' && !slide.hasError && ( start = self.getThumbPos( slide ) ) ) ) {
                effect = 'fade';
            }



            if ( effect === 'zoom' ) {
                end = self.getFitPos( slide );

                end.scaleX = Math.round( (end.width  / start.width)  * 100 ) / 100;
                end.scaleY = Math.round( (end.height / start.height) * 100 ) / 100;

                delete end.width;
                delete end.height;

                opacity = slide.opts.zoomOpacity;

                if ( opacity == 'auto' ) {
                    opacity = Math.abs( slide.width / slide.height - start.width / start.height ) > 0.1;
                }

                if ( opacity ) {
                    start.opacity = 0.1;
                    end.opacity   = 1;
                }

                $.fancybox.setTranslate( slide.$content.removeClass( 'fancybox-is-hidden' ), start );

                forceRedraw( slide.$content );

                $.fancybox.animate( slide.$content, end, duration, function() {
                    self.complete();
                });

                return;
            }


            self.updateSlide( slide );



            if ( !effect ) {
                forceRedraw( $slide );

                slide.$content.removeClass( 'fancybox-is-hidden' );

                if ( slide.pos === self.currPos ) {
                    self.complete();
                }

                return;
            }

            $.fancybox.stop( $slide );

            effectClassName = 'fancybox-animated fancybox-slide--' + ( slide.pos > self.prevPos ? 'next' : 'previous' ) + ' fancybox-fx-' + effect;

            $slide.removeAttr( 'style' ).removeClass( 'fancybox-slide--current fancybox-slide--next fancybox-slide--previous' ).addClass( effectClassName );

            slide.$content.removeClass( 'fancybox-is-hidden' );

            forceRedraw( $slide );

            $.fancybox.animate( $slide, 'fancybox-slide--current', duration, function(e) {
                $slide.removeClass( effectClassName ).removeAttr( 'style' );

                if ( slide.pos === self.currPos ) {
                    self.complete();
                }

            }, true);

        },



        getThumbPos : function( slide ) {

            var self = this;
            var rez  = false;

            var isElementVisible = function( $el ) {
                var element = $el[0];

                var elementRect = element.getBoundingClientRect();
                var parentRects = [];

                var visibleInAllParents;

                while ( element.parentElement !== null ) {
                    if ( $(element.parentElement).css('overflow') === 'hidden'  || $(element.parentElement).css('overflow') === 'auto' ) {
                        parentRects.push(element.parentElement.getBoundingClientRect());
                    }

                    element = element.parentElement;
                }

                visibleInAllParents = parentRects.every(function(parentRect){
                    var visiblePixelX = Math.min(elementRect.right, parentRect.right) - Math.max(elementRect.left, parentRect.left);
                    var visiblePixelY = Math.min(elementRect.bottom, parentRect.bottom) - Math.max(elementRect.top, parentRect.top);

                    return visiblePixelX > 0 && visiblePixelY > 0;
                });

                return visibleInAllParents &&
                    elementRect.bottom > 0 && elementRect.right > 0 &&
                    elementRect.left < $(window).width() && elementRect.top < $(window).height();
            };

            var $thumb   = slide.opts.$thumb;
            var thumbPos = $thumb ? $thumb.offset() : 0;
            var slidePos;

            if ( thumbPos && $thumb[0].ownerDocument === document && isElementVisible( $thumb ) ) {
                slidePos = self.$refs.stage.offset();

                rez = {
                    top    : thumbPos.top  - slidePos.top  + parseFloat( $thumb.css( "border-top-width" ) || 0 ),
                    left   : thumbPos.left - slidePos.left + parseFloat( $thumb.css( "border-left-width" ) || 0 ),
                    width  : $thumb.width(),
                    height : $thumb.height(),
                    scaleX : 1,
                    scaleY : 1
                };
            }

            return rez;
        },



        complete : function() {

            var self = this;

            var current = self.current;
            var slides  = {};

            if ( current.isMoved || !current.isLoaded || current.isComplete ) {
                return;
            }

            current.isComplete = true;

            current.$slide.siblings().trigger( 'onReset' );

            forceRedraw( current.$slide );

            current.$slide.addClass( 'fancybox-slide--complete' );

            $.each( self.slides, function( key, slide ) {
                if ( slide.pos >= self.currPos - 1 && slide.pos <= self.currPos + 1 ) {
                    slides[ slide.pos ] = slide;

                } else if ( slide ) {

                    $.fancybox.stop( slide.$slide );

                    slide.$slide.unbind().remove();
                }
            });

            self.slides = slides;

            self.updateCursor();

            self.trigger( 'afterShow' );

            if ( $( document.activeElement ).is( '[disabled]' ) || ( current.opts.autoFocus && !( current.type == 'image' || current.type === 'iframe' ) ) ) {
                self.focus();
            }

        },



        preload : function() {
            var self = this;
            var next, prev;

            if ( self.group.length < 2 ) {
                return;
            }

            next  = self.slides[ self.currPos + 1 ];
            prev  = self.slides[ self.currPos - 1 ];

            if ( next && next.type === 'image' ) {
                self.loadSlide( next );
            }

            if ( prev && prev.type === 'image' ) {
                self.loadSlide( prev );
            }

        },



        focus : function() {
            var current = this.current;
            var $el;

            if ( this.isClosing ) {
                return;
            }

            $el = current && current.isComplete ? current.$slide.find('button,:input,[tabindex],a').filter(':not([disabled]):visible:first') : null;
            $el = $el && $el.length ? $el : this.$refs.container;

            $el.focus();
        },



        activate : function () {
            var self = this;

            $( '.fancybox-container' ).each(function () {
                var instance = $(this).data( 'FancyBox' );

                if (instance && instance.uid !== self.uid && !instance.isClosing) {
                    instance.trigger( 'onDeactivate' );
                }

            });

            if ( self.current ) {
                if ( self.$refs.container.index() > 0 ) {
                    self.$refs.container.prependTo( document.body );
                }

                self.updateControls();
            }

            self.trigger( 'onActivate' );

            self.addEvents();

        },



        close : function( e, d ) {

            var self    = this;
            var current = self.current;

            var effect, duration;
            var $what, opacity, start, end;

            var done = function() {
                self.cleanUp( e );
            };

            if ( self.isClosing ) {
                return false;
            }

            self.isClosing = true;

            if ( self.trigger( 'beforeClose', e ) === false ) {
                self.isClosing = false;

                requestAFrame(function() {
                    self.update();
                });

                return false;
            }

            self.removeEvents();

            if ( current.timouts ) {
                clearTimeout( current.timouts );
            }

            $what    = current.$content;
            effect   = current.opts.animationEffect;
            duration = $.isNumeric( d ) ? d : ( effect ? current.opts.animationDuration : 0 );

            current.$slide.off( transitionEnd ).removeClass( 'fancybox-slide--complete fancybox-slide--next fancybox-slide--previous fancybox-animated' );

            current.$slide.siblings().trigger( 'onReset' ).remove();

            if ( duration ) {
                self.$refs.container.removeClass( 'fancybox-is-open' ).addClass( 'fancybox-is-closing' );
            }

            self.hideLoading( current );

            self.hideControls();

            self.updateCursor();

            if ( effect === 'zoom' && !( e !== true && $what && duration && current.type === 'image' && !current.hasError && ( end = self.getThumbPos( current ) ) ) ) {
                effect = 'fade';
            }

            if ( effect === 'zoom' ) {
                $.fancybox.stop( $what );

                start = $.fancybox.getTranslate( $what );

                start.width  = start.width  * start.scaleX;
                start.height = start.height * start.scaleY;

                opacity = current.opts.zoomOpacity;

                if ( opacity == 'auto' ) {
                    opacity = Math.abs( current.width / current.height - end.width / end.height ) > 0.1;
                }

                if ( opacity ) {
                    end.opacity = 0;
                }

                start.scaleX = start.width  / end.width;
                start.scaleY = start.height / end.height;

                start.width  = end.width;
                start.height = end.height;

                $.fancybox.setTranslate( current.$content, start );

                $.fancybox.animate( current.$content, end, duration, done );

                return true;
            }

            if ( effect && duration ) {

                if ( e === true ) {
                    setTimeout( done, duration );

                } else {
                    $.fancybox.animate( current.$slide.removeClass( 'fancybox-slide--current' ), 'fancybox-animated fancybox-slide--previous fancybox-fx-' + effect, duration, done );
                }

            } else {
                done();
            }

            return true;
        },



        cleanUp : function( e ) {
            var self = this,
                instance;

            self.current.$slide.trigger( 'onReset' );

            self.$refs.container.empty().remove();

            self.trigger( 'afterClose', e );

            if ( self.$lastFocus && !!!self.current.focusBack ) {
                self.$lastFocus.focus();
            }

            self.current = null;

            instance = $.fancybox.getInstance();

            if ( instance ) {
                instance.activate();

            } else {

                $W.scrollTop( self.scrollTop ).scrollLeft( self.scrollLeft );

                $( 'html' ).removeClass( 'fancybox-enabled' );

                $( '#fancybox-style-noscroll' ).remove();
            }

        },



        trigger : function( name, slide ) {
            var args  = Array.prototype.slice.call(arguments, 1),
                self  = this,
                obj   = slide && slide.opts ? slide : self.current,
                rez;

            if ( obj ) {
                args.unshift( obj );

            } else {
                obj = self;
            }

            args.unshift( self );

            if ( $.isFunction( obj.opts[ name ] ) ) {
                rez = obj.opts[ name ].apply( obj, args );
            }

            if ( rez === false ) {
                return rez;
            }

            if ( name === 'afterClose' ) {
                $D.trigger( name + '.fb', args );

            } else {
                self.$refs.container.trigger( name + '.fb', args );
            }

        },



        updateControls : function ( force ) {

            var self = this;

            var current  = self.current;
            var index    = current.index;
            var opts     = current.opts;
            var caption  = opts.caption;
            var $caption = self.$refs.caption;

            current.$slide.trigger( 'refresh' );

            self.$caption = caption && caption.length ? $caption.html( caption ) : null;

            if ( !self.isHiddenControls ) {
                self.showControls();
            }

            $('[data-fancybox-count]').html( self.group.length );
            $('[data-fancybox-index]').html( index + 1 );

            $('[data-fancybox-prev]').prop('disabled', ( !opts.loop && index <= 0 ) );
            $('[data-fancybox-next]').prop('disabled', ( !opts.loop && index >= self.group.length - 1 ) );

        },


        hideControls : function () {

            this.isHiddenControls = true;

            this.$refs.container.removeClass('fancybox-show-infobar fancybox-show-toolbar fancybox-show-caption fancybox-show-nav');

        },

        showControls : function() {

            var self = this;
            var opts = self.current ? self.current.opts : self.opts;
            var $container = self.$refs.container;

            self.isHiddenControls   = false;
            self.idleSecondsCounter = 0;

            $container
                .toggleClass('fancybox-show-toolbar', !!( opts.toolbar && opts.buttons ) )
                .toggleClass('fancybox-show-infobar', !!( opts.infobar && self.group.length > 1 ) )
                .toggleClass('fancybox-show-nav',     !!( opts.arrows && self.group.length > 1 ) )
                .toggleClass('fancybox-is-modal',     !!opts.modal );

            if ( self.$caption ) {
                $container.addClass( 'fancybox-show-caption ');

            } else {
               $container.removeClass( 'fancybox-show-caption' );
           }

       },



       toggleControls : function() {

           if ( this.isHiddenControls ) {
               this.showControls();

           } else {
               this.hideControls();
           }

       },


    });


    $.fancybox = {

        version  : "3.1.20",
        defaults : defaults,



        getInstance : function ( command ) {
            var instance = $('.fancybox-container:not(".fancybox-is-closing"):first').data( 'FancyBox' );
            var args     = Array.prototype.slice.call(arguments, 1);

            if ( instance instanceof FancyBox ) {

                if ( $.type( command ) === 'string' ) {
                    instance[ command ].apply( instance, args );

                } else if ( $.type( command ) === 'function' ) {
                    command.apply( instance, args );

                }

                return instance;
            }

            return false;

        },



        open : function ( items, opts, index ) {
            return new FancyBox( items, opts, index );
        },



        close : function ( all ) {
            var instance = this.getInstance();

            if ( instance ) {
                instance.close();


                if ( all === true ) {
                    this.close();
                }
            }

        },


        destroy : function() {

            this.close( true );

            $D.off( 'click.fb-start' );

        },



        isMobile : document.createTouch !== undefined && /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent),



        use3d : (function() {
            var div = document.createElement('div');

            return window.getComputedStyle && window.getComputedStyle( div ).getPropertyValue('transform') && !(document.documentMode && document.documentMode < 11);
        }()),



        getTranslate : function( $el ) {
            var matrix;

            if ( !$el || !$el.length ) {
                return false;
            }

            matrix  = $el.eq( 0 ).css('transform');

            if ( matrix && matrix.indexOf( 'matrix' ) !== -1 ) {
                matrix = matrix.split('(')[1];
                matrix = matrix.split(')')[0];
                matrix = matrix.split(',');
            } else {
                matrix = [];
            }

            if ( matrix.length ) {

                if ( matrix.length > 10 ) {
                    matrix = [ matrix[13], matrix[12], matrix[0], matrix[5] ];

                } else {
                    matrix = [ matrix[5], matrix[4], matrix[0], matrix[3]];
                }

                matrix = matrix.map(parseFloat);

            } else {
                matrix = [ 0, 0, 1, 1 ];

                var transRegex = /\.*translate\((.*)px,(.*)px\)/i;
                var transRez = transRegex.exec( $el.eq( 0 ).attr('style') );

                if ( transRez ) {
                    matrix[ 0 ] = parseFloat( transRez[2] );
                    matrix[ 1 ] = parseFloat( transRez[1] );
                }
            }

            return {
                top     : matrix[ 0 ],
                left    : matrix[ 1 ],
                scaleX  : matrix[ 2 ],
                scaleY  : matrix[ 3 ],
                opacity : parseFloat( $el.css('opacity') ),
                width   : $el.width(),
                height  : $el.height()
            };

        },



        setTranslate : function( $el, props ) {
            var str  = '';
            var css  = {};

            if ( !$el || !props ) {
                return;
            }

            if ( props.left !== undefined || props.top !== undefined ) {
                str = ( props.left === undefined ? $el.position().left : props.left )  + 'px, ' + ( props.top === undefined ? $el.position().top : props.top ) + 'px';

                if ( this.use3d ) {
                    str = 'translate3d(' + str + ', 0px)';

                } else {
                    str = 'translate(' + str + ')';
                }
            }

            if ( props.scaleX !== undefined && props.scaleY !== undefined ) {
                str = (str.length ? str + ' ' : '') + 'scale(' + props.scaleX + ', ' + props.scaleY + ')';
            }

            if ( str.length ) {
                css.transform = str;
            }

            if ( props.opacity !== undefined ) {
                css.opacity = props.opacity;
            }

            if ( props.width !== undefined ) {
                css.width = props.width;
            }

            if ( props.height !== undefined ) {
                css.height = props.height;
            }

            return $el.css( css );
        },



        animate : function ( $el, to, duration, callback, leaveAnimationName ) {
            var event = transitionEnd || 'transitionend';

            if ( $.isFunction( duration ) ) {
                callback = duration;
                duration = null;
            }

            if ( !$.isPlainObject( to ) ) {
                $el.removeAttr('style');
            }

            $el.on( event, function(e) {

                if ( e && e.originalEvent && ( !$el.is( e.originalEvent.target ) || e.originalEvent.propertyName == 'z-index' ) ) {
                    return;
                }

                $el.off( event );

                if ( $.isPlainObject( to ) ) {

                    if ( to.scaleX !== undefined && to.scaleY !== undefined ) {
                        $el.css( 'transition-duration', '0ms' );

                        to.width  = $el.width()  * to.scaleX;
                        to.height = $el.height() * to.scaleY;

                        to.scaleX = 1;
                        to.scaleY = 1;

                        $.fancybox.setTranslate( $el, to );
                    }

                } else if ( leaveAnimationName !== true ) {
                    $el.removeClass( to );
                }

                if ( $.isFunction( callback ) ) {
                    callback( e );
                }

            });

            if ( $.isNumeric( duration ) ) {
                $el.css( 'transition-duration', duration + 'ms' );
            }

            if ( $.isPlainObject( to ) ) {
                $.fancybox.setTranslate( $el, to );

            } else {
                $el.addClass( to );
            }

            $el.data("timer", setTimeout(function() {
                $el.trigger( 'transitionend' );
            }, duration + 16));

        },

        stop : function( $el ) {
            clearTimeout( $el.data("timer") );

            $el.off( transitionEnd );
        }

    };



    function _run( e ) {
        var target	= e.currentTarget,
            opts	= e.data ? e.data.options : {},
            items	= e.data ? e.data.items : [],
            value	= $(target).attr( 'data-fancybox' ) || '',
            index	= 0;

        e.preventDefault();
        e.stopPropagation();

        if ( value ) {
            items = items.length ? items.filter( '[data-fancybox="' + value + '"]' ) : $( '[data-fancybox="' + value + '"]' );
            index = items.index( target );

            if ( index < 0 ) {
                index = 0;
            }

        } else {
            items = [ target ];
        }

        $.fancybox.open( items, opts, index );
    }



    $.fn.fancybox = function (options) {
        var selector;

        options  = options || {};
        selector = options.selector || false;

        if ( selector ) {

            $( 'body' ).off( 'click.fb-start', selector ).on( 'click.fb-start', selector, {
                items   : $( selector ),
                options : options
            }, _run );

        } else {

            this.off( 'click.fb-start' ).on( 'click.fb-start', {
                items   : this,
                options : options
            }, _run);

        }

        return this;
    };



    $D.on( 'click.fb-start', '[data-fancybox]', _run );

}( window, document, window.jQuery ));

;(function ($) {

	'use strict';


	var format = function (url, rez, params) {
		if ( !url ) {
			return;
		}

		params = params || '';

		if ( $.type(params) === "object" ) {
			params = $.param(params, true);
		}

		$.each(rez, function (key, value) {
			url = url.replace('$' + key, value || '');
		});

		if (params.length) {
			url += (url.indexOf('?') > 0 ? '&' : '?') + params;
		}

		return url;
	};


	var defaults = {
		youtube : {
			matcher : /(youtube\.com|youtu\.be|youtube\-nocookie\.com)\/(watch\?(.*&)?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*))(.*)/i,
			params  : {
				autoplay : 1,
				autohide : 1,
				fs  : 1,
				rel : 0,
				hd  : 1,
				wmode : 'transparent',
				enablejsapi : 1,
				html5 : 1
			},
			paramPlace : 8,
			type  : 'iframe',
			url   : '//www.youtube.com/embed/$4',
			thumb : '//img.youtube.com/vi/$4/hqdefault.jpg'
		},

		vimeo : {
			matcher : /^.+vimeo.com\/(.*\/)?([\d]+)(.*)?/,
			params  : {
				autoplay : 1,
				hd : 1,
				show_title    : 1,
				show_byline   : 1,
				show_portrait : 0,
				fullscreen    : 1,
				api : 1
			},
			paramPlace : 3,
			type : 'iframe',
			url : '//player.vimeo.com/video/$2'
		},

		metacafe : {
			matcher : /metacafe.com\/watch\/(\d+)\/(.*)?/,
			type    : 'iframe',
			url     : '//www.metacafe.com/embed/$1/?ap=1'
		},

		dailymotion : {
			matcher : /dailymotion.com\/video\/(.*)\/?(.*)/,
			params : {
				additionalInfos : 0,
				autoStart : 1
			},
			type : 'iframe',
			url  : '//www.dailymotion.com/embed/video/$1'
		},

		vine : {
			matcher : /vine.co\/v\/([a-zA-Z0-9\?\=\-]+)/,
			type    : 'iframe',
			url     : '//vine.co/v/$1/embed/simple'
		},

		instagram : {
			matcher : /(instagr\.am|instagram\.com)\/p\/([a-zA-Z0-9_\-]+)\/?/i,
			type    : 'image',
			url     : '//$1/p/$2/media/?size=l'
		},

		google_maps : {
			matcher : /(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(((maps\/(place\/(.*)\/)?\@(.*),(\d+.?\d+?)z))|(\?ll=))(.*)?/i,
			type    : 'iframe',
			url     : function (rez) {
				return '//maps.google.' + rez[2] + '/?ll=' + ( rez[9] ? rez[9] + '&z=' + Math.floor(  rez[10]  ) + ( rez[12] ? rez[12].replace(/^\//, "&") : '' )  : rez[12] ) + '&output=' + ( rez[12] && rez[12].indexOf('layer=c') > 0 ? 'svembed' : 'embed' );
			}
		}
	};

	$(document).on('onInit.fb', function (e, instance) {

		$.each(instance.group, function( i, item ) {

			var url	 = item.src || '',
				type = false,
				media,
				thumb,
				rez,
				params,
				urlParams,
				o,
				provider;

			if ( item.type ) {
				return;
			}

			media = $.extend( true, {}, defaults, item.opts.media );

			$.each(media, function ( n, el ) {
				rez = url.match(el.matcher);
				o   = {};
				provider = n;

				if (!rez) {
					return;
				}

				type = el.type;

				if ( el.paramPlace && rez[ el.paramPlace ] ) {
					urlParams = rez[ el.paramPlace ];

					if ( urlParams[ 0 ] == '?' ) {
						urlParams = urlParams.substring(1);
					}

					urlParams = urlParams.split('&');

					for ( var m = 0; m < urlParams.length; ++m ) {
						var p = urlParams[ m ].split('=', 2);

						if ( p.length == 2 ) {
							o[ p[0] ] = decodeURIComponent( p[1].replace(/\+/g, " ") );
						}
					}
				}

				params = $.extend( true, {}, el.params, item.opts[ n ], o );

				url   = $.type(el.url) === "function" ? el.url.call(this, rez, params, item) : format(el.url, rez, params);
				thumb = $.type(el.thumb) === "function" ? el.thumb.call(this, rez, params, item) : format(el.thumb, rez);

				if ( provider === 'vimeo' ) {
					url = url.replace('&%23', '#');
				}

				return false;
			});


			if ( type ) {
				item.src  = url;
				item.type = type;

				if ( !item.opts.thumb && !( item.opts.$thumb && item.opts.$thumb.length ) ) {
					item.opts.thumb = thumb;
				}

				if ( type === 'iframe' ) {
					$.extend(true, item.opts, {
						iframe : {
							preload : false,
							attr : {
								scrolling : "no"
							}
						}
					});

					item.contentProvider = provider;

					item.opts.slideClass += ' fancybox-slide--' + ( provider == 'google_maps' ? 'map' : 'video' );
				}

			} else {

				item.type = 'image';
			}

		});

	});

}(window.jQuery));

;(function (window, document, $) {
	'use strict';

	var requestAFrame = (function () {
        return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                function (callback) {
                    return window.setTimeout(callback, 1000 / 60);
                };
    })();


    var cancelAFrame = (function () {
        return window.cancelAnimationFrame ||
                window.webkitCancelAnimationFrame ||
                window.mozCancelAnimationFrame ||
                window.oCancelAnimationFrame ||
                function (id) {
                    window.clearTimeout(id);
                };
    })();


	var pointers = function( e ) {
		var result = [];

		e = e.originalEvent || e || window.e;
		e = e.touches && e.touches.length ? e.touches : ( e.changedTouches && e.changedTouches.length ? e.changedTouches : [ e ] );

		for ( var key in e ) {

			if ( e[ key ].pageX ) {
				result.push( { x : e[ key ].pageX, y : e[ key ].pageY } );

			} else if ( e[ key ].clientX ) {
				result.push( { x : e[ key ].clientX, y : e[ key ].clientY } );
			}
		}

		return result;
	};

	var distance = function( point2, point1, what ) {
		if ( !point1 || !point2 ) {
			return 0;
		}

		if ( what === 'x' ) {
			return point2.x - point1.x;

		} else if ( what === 'y' ) {
			return point2.y - point1.y;
		}

		return Math.sqrt( Math.pow( point2.x - point1.x, 2 ) + Math.pow( point2.y - point1.y, 2 ) );
	};

	var isClickable = function( $el ) {
		if ( $el.is('a,button,input,select,textarea') || $.isFunction( $el.get(0).onclick ) ) {
			return true;
		}

		for ( var i = 0, atts = $el[0].attributes, n = atts.length; i < n; i++ ) {
            if ( atts[i].nodeName.substr(0, 14) === 'data-fancybox-' ) {
                return true;
            }
        }

	 	return false;
	};

	var hasScrollbars = function( el ) {
		var overflowY = window.getComputedStyle( el )['overflow-y'];
		var overflowX = window.getComputedStyle( el )['overflow-x'];

		var vertical   = (overflowY === 'scroll' || overflowY === 'auto') && el.scrollHeight > el.clientHeight;
		var horizontal = (overflowX === 'scroll' || overflowX === 'auto') && el.scrollWidth > el.clientWidth;

		return vertical || horizontal;
	};

	var isScrollable = function ( $el ) {
		var rez = false;

		while ( true ) {
			rez	= hasScrollbars( $el.get(0) );

			if ( rez ) {
				break;
			}

			$el = $el.parent();

			if ( !$el.length || $el.hasClass( 'fancybox-stage' ) || $el.is( 'body' ) ) {
				break;
			}
		}

		return rez;
	};


	var Guestures = function ( instance ) {
		var self = this;

		self.instance = instance;

		self.$bg        = instance.$refs.bg;
		self.$stage     = instance.$refs.stage;
		self.$container = instance.$refs.container;

		self.destroy();

		self.$container.on( 'touchstart.fb.touch mousedown.fb.touch', $.proxy(self, 'ontouchstart') );
	};

	Guestures.prototype.destroy = function() {
		this.$container.off( '.fb.touch' );
	};

	Guestures.prototype.ontouchstart = function( e ) {
		var self = this;

		var $target  = $( e.target );
		var instance = self.instance;
		var current  = instance.current;
		var $content = current.$content;

		var isTouchDevice = ( e.type == 'touchstart' );

		if ( isTouchDevice ) {
	        self.$container.off( 'mousedown.fb.touch' );
	    }

		if ( !current || self.instance.isAnimating || self.instance.isClosing ) {
			e.stopPropagation();
			e.preventDefault();

			return;
		}

		if ( e.originalEvent && e.originalEvent.button == 2 ) {
			return;
		}

		if ( !$target.length || isClickable( $target ) || isClickable( $target.parent() ) ) {
			return;
		}

		if ( e.originalEvent.clientX > $target[0].clientWidth + $target.offset().left ) {
			return;
		}

		self.startPoints = pointers( e );

		if ( !self.startPoints || ( self.startPoints.length > 1 && instance.isSliding ) ) {
			return;
		}

		self.$target  = $target;
		self.$content = $content;
		self.canTap   = true;

		$(document).off( '.fb.touch' );

		$(document).on( isTouchDevice ? 'touchend.fb.touch touchcancel.fb.touch' : 'mouseup.fb.touch mouseleave.fb.touch',  $.proxy(self, "ontouchend"));
		$(document).on( isTouchDevice ? 'touchmove.fb.touch' : 'mousemove.fb.touch',  $.proxy(self, "ontouchmove"));

		e.stopPropagation();

		if ( !(instance.current.opts.touch || instance.canPan() ) || !( $target.is( self.$stage ) || self.$stage.find( $target ).length ) ) {

			if ( $target.is('img') ) {
				e.preventDefault();
			}

			return;
		}

		if ( !( $.fancybox.isMobile && ( isScrollable( self.$target ) || isScrollable( self.$target.parent() ) ) ) ) {
			e.preventDefault();
		}

		self.canvasWidth  = Math.round( current.$slide[0].clientWidth );
		self.canvasHeight = Math.round( current.$slide[0].clientHeight );

		self.startTime = new Date().getTime();
		self.distanceX = self.distanceY = self.distance = 0;

		self.isPanning = false;
		self.isSwiping = false;
		self.isZooming = false;

		self.sliderStartPos  = self.sliderLastPos || { top: 0, left: 0 };
		self.contentStartPos = $.fancybox.getTranslate( self.$content );
		self.contentLastPos  = null;

		if ( self.startPoints.length === 1 && !self.isZooming ) {
			self.canTap = !instance.isSliding;

			if ( current.type === 'image' && ( self.contentStartPos.width > self.canvasWidth + 1 || self.contentStartPos.height > self.canvasHeight + 1 ) ) {

				$.fancybox.stop( self.$content );

				self.$content.css( 'transition-duration', '0ms' );

				self.isPanning = true;

			} else {

				self.isSwiping = true;
			}

			self.$container.addClass('fancybox-controls--isGrabbing');
		}

		if ( self.startPoints.length === 2 && !instance.isAnimating && !current.hasError && current.type === 'image' && ( current.isLoaded || current.$ghost ) ) {
			self.isZooming = true;

			self.isSwiping = false;
			self.isPanning = false;

			$.fancybox.stop( self.$content );

			self.$content.css( 'transition-duration', '0ms' );

			self.centerPointStartX = ( ( self.startPoints[0].x + self.startPoints[1].x ) * 0.5 ) - $(window).scrollLeft();
			self.centerPointStartY = ( ( self.startPoints[0].y + self.startPoints[1].y ) * 0.5 ) - $(window).scrollTop();

			self.percentageOfImageAtPinchPointX = ( self.centerPointStartX - self.contentStartPos.left ) / self.contentStartPos.width;
			self.percentageOfImageAtPinchPointY = ( self.centerPointStartY - self.contentStartPos.top  ) / self.contentStartPos.height;

			self.startDistanceBetweenFingers = distance( self.startPoints[0], self.startPoints[1] );
		}

	};

	Guestures.prototype.ontouchmove = function( e ) {

		var self = this;

		self.newPoints = pointers( e );

		if ( $.fancybox.isMobile && ( isScrollable( self.$target ) || isScrollable( self.$target.parent() ) ) ) {
			e.stopPropagation();

			self.canTap = false;

			return;
		}

		if ( !( self.instance.current.opts.touch || self.instance.canPan() ) || !self.newPoints || !self.newPoints.length ) {
			return;
		}

		self.distanceX = distance( self.newPoints[0], self.startPoints[0], 'x' );
		self.distanceY = distance( self.newPoints[0], self.startPoints[0], 'y' );

		self.distance = distance( self.newPoints[0], self.startPoints[0] );

		if ( self.distance > 0 ) {

			if ( !( self.$target.is( self.$stage ) || self.$stage.find( self.$target ).length ) ) {
				return;
			}

			e.stopPropagation();
			e.preventDefault();

			if ( self.isSwiping ) {
				self.onSwipe();

			} else if ( self.isPanning ) {
				self.onPan();

			} else if ( self.isZooming ) {
				self.onZoom();
			}

		}

	};

	Guestures.prototype.onSwipe = function() {

		var self = this;

		var swiping = self.isSwiping;
		var left    = self.sliderStartPos.left || 0;
		var angle;

		if ( swiping === true ) {

			if ( Math.abs( self.distance ) > 10 )  {

				self.canTap = false;

				if ( self.instance.group.length < 2 && self.instance.opts.touch.vertical ) {
					self.isSwiping  = 'y';

				} else if ( self.instance.isSliding || self.instance.opts.touch.vertical === false || ( self.instance.opts.touch.vertical === 'auto' && $( window ).width() > 800 ) ) {
					self.isSwiping  = 'x';

				} else {
					angle = Math.abs( Math.atan2( self.distanceY, self.distanceX ) * 180 / Math.PI );

					self.isSwiping = ( angle > 45 && angle < 135 ) ? 'y' : 'x';
				}

				self.instance.isSliding = self.isSwiping;

				self.startPoints = self.newPoints;

				$.each(self.instance.slides, function( index, slide ) {
					$.fancybox.stop( slide.$slide );

					slide.$slide.css( 'transition-duration', '0ms' );

					slide.inTransition = false;

					if ( slide.pos === self.instance.current.pos ) {
						self.sliderStartPos.left = $.fancybox.getTranslate( slide.$slide ).left;
					}
				});


				if ( self.instance.SlideShow && self.instance.SlideShow.isActive ) {
					self.instance.SlideShow.stop();
				}
			}

		} else {

			if ( swiping == 'x' ) {

				if ( self.distanceX > 0 && ( self.instance.group.length < 2 || ( self.instance.current.index === 0 && !self.instance.current.opts.loop ) ) ) {
					left = left + Math.pow( self.distanceX, 0.8 );

				} else if ( self.distanceX < 0 && ( self.instance.group.length < 2 || ( self.instance.current.index === self.instance.group.length - 1 && !self.instance.current.opts.loop ) ) ) {
					left = left - Math.pow( -self.distanceX, 0.8 );

				} else {
					left = left + self.distanceX;
				}

			}

			self.sliderLastPos = {
				top  : swiping == 'x' ? 0 : self.sliderStartPos.top + self.distanceY,
				left : left
			};

			if ( self.requestId ) {
				cancelAFrame( self.requestId );

				self.requestId = null;
			}

			self.requestId = requestAFrame(function() {

				if ( self.sliderLastPos ) {
					$.each(self.instance.slides, function( index, slide ) {
						var pos = slide.pos - self.instance.currPos;

						$.fancybox.setTranslate( slide.$slide, {
							top  : self.sliderLastPos.top,
							left : self.sliderLastPos.left + ( pos * self.canvasWidth ) + ( pos * slide.opts.gutter )
						});
					});

					self.$container.addClass( 'fancybox-is-sliding' );
				}

			});

		}

	};

	Guestures.prototype.onPan = function() {

		var self = this;

		var newOffsetX, newOffsetY, newPos;

		self.canTap = false;

		if ( self.contentStartPos.width > self.canvasWidth ) {
			newOffsetX = self.contentStartPos.left + self.distanceX;

		} else {
			newOffsetX = self.contentStartPos.left;
		}

		newOffsetY = self.contentStartPos.top + self.distanceY;

		newPos = self.limitMovement( newOffsetX, newOffsetY, self.contentStartPos.width, self.contentStartPos.height );

		newPos.scaleX = self.contentStartPos.scaleX;
		newPos.scaleY = self.contentStartPos.scaleY;

		self.contentLastPos = newPos;

		if ( self.requestId ) {
			cancelAFrame( self.requestId );

			self.requestId = null;
		}

		self.requestId = requestAFrame(function() {
			$.fancybox.setTranslate( self.$content, self.contentLastPos );
		});
	};

	Guestures.prototype.limitMovement = function( newOffsetX, newOffsetY, newWidth, newHeight ) {

		var self = this;

		var minTranslateX, minTranslateY, maxTranslateX, maxTranslateY;

		var canvasWidth  = self.canvasWidth;
		var canvasHeight = self.canvasHeight;

		var currentOffsetX = self.contentStartPos.left;
		var currentOffsetY = self.contentStartPos.top;

		var distanceX = self.distanceX;
		var distanceY = self.distanceY;


		minTranslateX = Math.max(0, canvasWidth  * 0.5 - newWidth  * 0.5 );
		minTranslateY = Math.max(0, canvasHeight * 0.5 - newHeight * 0.5 );

		maxTranslateX = Math.min( canvasWidth  - newWidth,  canvasWidth  * 0.5 - newWidth  * 0.5 );
		maxTranslateY = Math.min( canvasHeight - newHeight, canvasHeight * 0.5 - newHeight * 0.5 );

		if ( newWidth > canvasWidth ) {

			if ( distanceX > 0 && newOffsetX > minTranslateX ) {
				newOffsetX = minTranslateX - 1 + Math.pow( -minTranslateX + currentOffsetX + distanceX, 0.8 ) || 0;
			}

			if ( distanceX  < 0 && newOffsetX < maxTranslateX ) {
				newOffsetX = maxTranslateX + 1 - Math.pow( maxTranslateX - currentOffsetX - distanceX, 0.8 ) || 0;
			}

		}

		if ( newHeight > canvasHeight ) {

			if ( distanceY > 0 && newOffsetY > minTranslateY ) {
				newOffsetY = minTranslateY - 1 + Math.pow(-minTranslateY + currentOffsetY + distanceY, 0.8 ) || 0;
			}

			if ( distanceY < 0 && newOffsetY < maxTranslateY ) {
				newOffsetY = maxTranslateY + 1 - Math.pow ( maxTranslateY - currentOffsetY - distanceY, 0.8 ) || 0;
			}

		}

		return {
			top  : newOffsetY,
			left : newOffsetX
		};

	};


	Guestures.prototype.limitPosition = function( newOffsetX, newOffsetY, newWidth, newHeight ) {

		var self = this;

		var canvasWidth  = self.canvasWidth;
		var canvasHeight = self.canvasHeight;

		if ( newWidth > canvasWidth ) {
			newOffsetX = newOffsetX > 0 ? 0 : newOffsetX;
			newOffsetX = newOffsetX < canvasWidth - newWidth ? canvasWidth - newWidth : newOffsetX;

		} else {

			newOffsetX = Math.max( 0, canvasWidth / 2 - newWidth / 2 );

		}

		if ( newHeight > canvasHeight ) {
			newOffsetY = newOffsetY > 0 ? 0 : newOffsetY;
			newOffsetY = newOffsetY < canvasHeight - newHeight ? canvasHeight - newHeight : newOffsetY;

		} else {

			newOffsetY = Math.max( 0, canvasHeight / 2 - newHeight / 2 );

		}

		return {
			top  : newOffsetY,
			left : newOffsetX
		};

	};

	Guestures.prototype.onZoom = function() {

		var self = this;


		var currentWidth  = self.contentStartPos.width;
		var currentHeight = self.contentStartPos.height;

		var currentOffsetX = self.contentStartPos.left;
		var currentOffsetY = self.contentStartPos.top;

		var endDistanceBetweenFingers = distance( self.newPoints[0], self.newPoints[1] );

		var pinchRatio = endDistanceBetweenFingers / self.startDistanceBetweenFingers;

		var newWidth  = Math.floor( currentWidth  * pinchRatio );
		var newHeight = Math.floor( currentHeight * pinchRatio );

		var translateFromZoomingX = (currentWidth  - newWidth)  * self.percentageOfImageAtPinchPointX;
		var translateFromZoomingY = (currentHeight - newHeight) * self.percentageOfImageAtPinchPointY;


		var centerPointEndX = ((self.newPoints[0].x + self.newPoints[1].x) / 2) - $(window).scrollLeft();
		var centerPointEndY = ((self.newPoints[0].y + self.newPoints[1].y) / 2) - $(window).scrollTop();


		var translateFromTranslatingX = centerPointEndX - self.centerPointStartX;
		var translateFromTranslatingY = centerPointEndY - self.centerPointStartY;


		var newOffsetX = currentOffsetX + ( translateFromZoomingX + translateFromTranslatingX );
		var newOffsetY = currentOffsetY + ( translateFromZoomingY + translateFromTranslatingY );

		var newPos = {
			top    : newOffsetY,
			left   : newOffsetX,
			scaleX : self.contentStartPos.scaleX * pinchRatio,
			scaleY : self.contentStartPos.scaleY * pinchRatio
		};

		self.canTap = false;

		self.newWidth  = newWidth;
		self.newHeight = newHeight;

		self.contentLastPos = newPos;

		if ( self.requestId ) {
			cancelAFrame( self.requestId );

			self.requestId = null;
		}

		self.requestId = requestAFrame(function() {
			$.fancybox.setTranslate( self.$content, self.contentLastPos );
		});

	};

	Guestures.prototype.ontouchend = function( e ) {

		var self = this;
		var dMs  = Math.max( (new Date().getTime() ) - self.startTime, 1);

		var swiping = self.isSwiping;
		var panning = self.isPanning;
		var zooming = self.isZooming;

		self.endPoints = pointers( e );

		self.$container.removeClass( 'fancybox-controls--isGrabbing' );

		$(document).off( '.fb.touch' );

		if ( self.requestId ) {
			cancelAFrame( self.requestId );

			self.requestId = null;
		}

		self.isSwiping = false;
		self.isPanning = false;
		self.isZooming = false;

		if ( self.canTap )  {
			return self.onTap( e );
		}

		self.speed = 366;

		self.velocityX = self.distanceX / dMs * 0.5;
		self.velocityY = self.distanceY / dMs * 0.5;

		self.speedX = Math.max( self.speed * 0.5, Math.min( self.speed * 1.5, ( 1 / Math.abs( self.velocityX ) ) * self.speed ) );

		if ( panning ) {
			self.endPanning();

		} else if ( zooming ) {
			self.endZooming();

		} else {
			self.endSwiping( swiping );
		}

		return;
	};

	Guestures.prototype.endSwiping = function( swiping ) {

		var self = this;
		var ret = false;

		self.instance.isSliding = false;
		self.sliderLastPos      = null;

		if ( swiping == 'y' && Math.abs( self.distanceY ) > 50 ) {

			$.fancybox.animate( self.instance.current.$slide, {
				top     : self.sliderStartPos.top + self.distanceY + ( self.velocityY * 150 ),
				opacity : 0
			}, 150 );

			ret = self.instance.close( true, 300 );

		} else if ( swiping == 'x' && self.distanceX > 50 && self.instance.group.length > 1 ) {
			ret = self.instance.previous( self.speedX );

		} else if ( swiping == 'x' && self.distanceX < -50  && self.instance.group.length > 1 ) {
			ret = self.instance.next( self.speedX );
		}

		if ( ret === false && ( swiping == 'x' || swiping == 'y' ) ) {
			self.instance.jumpTo( self.instance.current.index, 150 );
		}

		self.$container.removeClass( 'fancybox-is-sliding' );

	};


	Guestures.prototype.endPanning = function() {

		var self = this;
		var newOffsetX, newOffsetY, newPos;

		if ( !self.contentLastPos ) {
			return;
		}

		if ( self.instance.current.opts.touch.momentum === false ) {
			newOffsetX = self.contentLastPos.left;
			newOffsetY = self.contentLastPos.top;

		} else {

			newOffsetX = self.contentLastPos.left + ( self.velocityX * self.speed );
			newOffsetY = self.contentLastPos.top  + ( self.velocityY * self.speed );
		}

		newPos = self.limitPosition( newOffsetX, newOffsetY, self.contentStartPos.width, self.contentStartPos.height );

		 newPos.width  = self.contentStartPos.width;
		 newPos.height = self.contentStartPos.height;

		$.fancybox.animate( self.$content, newPos, 330 );
	};


	Guestures.prototype.endZooming = function() {

		var self = this;

		var current = self.instance.current;

		var newOffsetX, newOffsetY, newPos, reset;

		var newWidth  = self.newWidth;
		var newHeight = self.newHeight;

		if ( !self.contentLastPos ) {
			return;
		}

		newOffsetX = self.contentLastPos.left;
		newOffsetY = self.contentLastPos.top;

		reset = {
		   	top    : newOffsetY,
		   	left   : newOffsetX,
		   	width  : newWidth,
		   	height : newHeight,
			scaleX : 1,
			scaleY : 1
	   };

	   $.fancybox.setTranslate( self.$content, reset );

		if ( newWidth < self.canvasWidth && newHeight < self.canvasHeight ) {
			self.instance.scaleToFit( 150 );

		} else if ( newWidth > current.width || newHeight > current.height ) {
			self.instance.scaleToActual( self.centerPointStartX, self.centerPointStartY, 150 );

		} else {

			newPos = self.limitPosition( newOffsetX, newOffsetY, newWidth, newHeight );

			$.fancybox.setTranslate( self.content, $.fancybox.getTranslate( self.$content ) );

			$.fancybox.animate( self.$content, newPos, 150 );
		}

	};

	Guestures.prototype.onTap = function(e) {
		var self    = this;
		var $target = $( e.target );

		var instance = self.instance;
		var current  = instance.current;

		var endPoints = ( e && pointers( e ) ) || self.startPoints;

		var tapX = endPoints[0] ? endPoints[0].x - self.$stage.offset().left : 0;
		var tapY = endPoints[0] ? endPoints[0].y - self.$stage.offset().top  : 0;

		var where;

		var process = function ( prefix ) {

			var action = current.opts[ prefix ];

			if ( $.isFunction( action ) ) {
				action = action.apply( instance, [ current, e ] );
			}

			if ( !action) {
				return;
			}

			switch ( action ) {

				case "close" :

					instance.close( self.startEvent );

				break;

				case "toggleControls" :

					instance.toggleControls( true );

				break;

				case "next" :

					instance.next();

				break;

				case "nextOrClose" :

					if ( instance.group.length > 1 ) {
						instance.next();

					} else {
						instance.close( self.startEvent );
					}

				break;

				case "zoom" :

					if ( current.type == 'image' && ( current.isLoaded || current.$ghost ) ) {

						if ( instance.canPan() ) {
							instance.scaleToFit();

						} else if ( instance.isScaledDown() ) {
							instance.scaleToActual( tapX, tapY );

						} else if ( instance.group.length < 2 ) {
							instance.close( self.startEvent );
						}
					}

				break;
			}

		};

		if ( e.originalEvent && e.originalEvent.button == 2 ) {
			return;
		}

		if ( instance.isSliding ) {
			return;
		}

		if ( tapX > $target[0].clientWidth + $target.offset().left ) {
			return;
		}

		if ( $target.is( '.fancybox-bg,.fancybox-inner,.fancybox-outer,.fancybox-container' ) ) {
			where = 'Outside';

		} else if ( $target.is( '.fancybox-slide' ) ) {
			where = 'Slide';

		} else if  ( instance.current.$content && instance.current.$content.has( e.target ).length ) {
		 	where = 'Content';

		} else {
			return;
		}

		if ( self.tapped ) {

			clearTimeout( self.tapped );
			self.tapped = null;

			if ( Math.abs( tapX - self.tapX ) > 50 || Math.abs( tapY - self.tapY ) > 50 || instance.isSliding ) {
				return this;
			}

			process( 'dblclick' + where );

		} else {

			self.tapX = tapX;
			self.tapY = tapY;

			if ( current.opts[ 'dblclick' + where ] && current.opts[ 'dblclick' + where ] !== current.opts[ 'click' + where ] ) {
				self.tapped = setTimeout(function() {
					self.tapped = null;

					process( 'click' + where );

				}, 300);

			} else {
				process( 'click' + where );
			}

		}

		return this;
	};

	$(document).on('onActivate.fb', function (e, instance) {
		if ( instance && !instance.Guestures ) {
			instance.Guestures = new Guestures( instance );
		}
	});

	$(document).on('beforeClose.fb', function (e, instance) {
		if ( instance && instance.Guestures ) {
			instance.Guestures.destroy();
		}
	});


}(window, document, window.jQuery));

;(function (document, $) {
	'use strict';

	var SlideShow = function( instance ) {
		this.instance = instance;
		this.init();
	};

	$.extend( SlideShow.prototype, {
		timer    : null,
		isActive : false,
		$button  : null,
		speed    : 3000,

		init : function() {
			var self = this;

			self.$button = self.instance.$refs.toolbar.find('[data-fancybox-play]').on('click', function() {
				self.toggle();
			});

			if ( self.instance.group.length < 2 || !self.instance.group[ self.instance.currIndex ].opts.slideShow ) {
				self.$button.hide();
			}
		},

		set : function() {
			var self = this;

			if ( self.instance && self.instance.current && (self.instance.current.opts.loop || self.instance.currIndex < self.instance.group.length - 1 )) {
				self.timer = setTimeout(function() {
					self.instance.next();

				}, self.instance.current.opts.slideShow.speed || self.speed);

			} else {
				self.stop();
				self.instance.idleSecondsCounter = 0;
				self.instance.showControls();
			}

		},

		clear : function() {
			var self = this;

			clearTimeout( self.timer );

			self.timer = null;
		},

		start : function() {
			var self = this;
			var current = self.instance.current;

			if ( self.instance && current && ( current.opts.loop || current.index < self.instance.group.length - 1 )) {

				self.isActive = true;

				self.$button
					.attr( 'title', current.opts.i18n[ current.opts.lang ].PLAY_STOP )
					.addClass( 'fancybox-button--pause' );

				if ( current.isComplete ) {
					self.set();
				}
			}
		},

		stop : function() {
			var self = this;
			var current = self.instance.current;

			self.clear();

			self.$button
				.attr( 'title', current.opts.i18n[ current.opts.lang ].PLAY_START )
				.removeClass( 'fancybox-button--pause' );

			self.isActive = false;
		},

		toggle : function() {
			var self = this;

			if ( self.isActive ) {
				self.stop();

			} else {
				self.start();
			}
		}

	});

	$(document).on({
		'onInit.fb' : function(e, instance) {
			if ( instance && !instance.SlideShow ) {
				instance.SlideShow = new SlideShow( instance );
			}
		},

		'beforeShow.fb' : function(e, instance, current, firstRun) {
			var SlideShow = instance && instance.SlideShow;

			if ( firstRun ) {

				if ( SlideShow && current.opts.slideShow.autoStart ) {
					SlideShow.start();
				}

			} else if ( SlideShow && SlideShow.isActive )  {
				SlideShow.clear();
			}
		},

		'afterShow.fb' : function(e, instance, current) {
			var SlideShow = instance && instance.SlideShow;

			if ( SlideShow && SlideShow.isActive ) {
				SlideShow.set();
			}
		},

		'afterKeydown.fb' : function(e, instance, current, keypress, keycode) {
			var SlideShow = instance && instance.SlideShow;

			if ( SlideShow && current.opts.slideShow && ( keycode === 80 || keycode === 32 ) && !$(document.activeElement).is( 'button,a,input' ) ) {
				keypress.preventDefault();

				SlideShow.toggle();
			}
		},

		'beforeClose.fb onDeactivate.fb' : function(e, instance) {
			var SlideShow = instance && instance.SlideShow;

			if ( SlideShow ) {
				SlideShow.stop();
			}
		}
	});

	$(document).on("visibilitychange", function() {
		var instance  = $.fancybox.getInstance();
		var SlideShow = instance && instance.SlideShow;

		if ( SlideShow && SlideShow.isActive ) {
			if ( document.hidden ) {
				SlideShow.clear();

			} else {
				SlideShow.set();
			}
		}
	});

}(document, window.jQuery));

;(function (document, $) {
	'use strict';

	var fn = (function () {

		var fnMap = [
			[
				'requestFullscreen',
				'exitFullscreen',
				'fullscreenElement',
				'fullscreenEnabled',
				'fullscreenchange',
				'fullscreenerror'
			],
			[
				'webkitRequestFullscreen',
				'webkitExitFullscreen',
				'webkitFullscreenElement',
				'webkitFullscreenEnabled',
				'webkitfullscreenchange',
				'webkitfullscreenerror'

			],
			[
				'webkitRequestFullScreen',
				'webkitCancelFullScreen',
				'webkitCurrentFullScreenElement',
				'webkitCancelFullScreen',
				'webkitfullscreenchange',
				'webkitfullscreenerror'

			],
			[
				'mozRequestFullScreen',
				'mozCancelFullScreen',
				'mozFullScreenElement',
				'mozFullScreenEnabled',
				'mozfullscreenchange',
				'mozfullscreenerror'
			],
			[
				'msRequestFullscreen',
				'msExitFullscreen',
				'msFullscreenElement',
				'msFullscreenEnabled',
				'MSFullscreenChange',
				'MSFullscreenError'
			]
		];

		var val;
		var ret = {};
		var i, j;

		for ( i = 0; i < fnMap.length; i++ ) {
			val = fnMap[ i ];

			if ( val && val[ 1 ] in document ) {
				for ( j = 0; j < val.length; j++ ) {
					ret[ fnMap[ 0 ][ j ] ] = val[ j ];
				}

				return ret;
			}
		}

		return false;
	})();

	if ( !fn ) {
		$.fancybox.defaults.btnTpl.fullScreen = false;

		return;
	}

	var FullScreen = {
		request : function ( elem ) {

			elem = elem || document.documentElement;

			elem[ fn.requestFullscreen ]( elem.ALLOW_KEYBOARD_INPUT );

		},
		exit : function () {

			document[ fn.exitFullscreen ]();

		},
		toggle : function ( elem ) {

			elem = elem || document.documentElement;

			if ( this.isFullscreen() ) {
				this.exit();

			} else {
				this.request( elem );
			}

		},
		isFullscreen : function()  {

			return Boolean( document[ fn.fullscreenElement ] );

		},
		enabled : function()  {

			return Boolean( document[ fn.fullscreenEnabled ] );

		}
	};

	$(document).on({
		'onInit.fb' : function(e, instance) {
			var $container;

			var $button = instance.$refs.toolbar.find('[data-fancybox-fullscreen]');

			if ( instance && !instance.FullScreen && instance.group[ instance.currIndex ].opts.fullScreen ) {
				$container = instance.$refs.container;

				$container.on('click.fb-fullscreen', '[data-fancybox-fullscreen]', function(e) {

					e.stopPropagation();
					e.preventDefault();

					FullScreen.toggle( $container[ 0 ] );

				});

				if ( instance.opts.fullScreen && instance.opts.fullScreen.autoStart === true ) {
					FullScreen.request( $container[ 0 ] );
				}

				instance.FullScreen = FullScreen;

			} else {
				$button.hide();
			}

		},

		'afterKeydown.fb' : function(e, instance, current, keypress, keycode) {

			if ( instance && instance.FullScreen && keycode === 70 ) {
				keypress.preventDefault();

				instance.FullScreen.toggle( instance.$refs.container[ 0 ] );
			}

		},

		'beforeClose.fb' : function( instance ) {
			if ( instance && instance.FullScreen ) {
				FullScreen.exit();
			}
		}
	});

	$(document).on(fn.fullscreenchange, function() {
		var instance = $.fancybox.getInstance();

		if ( instance.current && instance.current.type === 'image' && instance.isAnimating ) {
			instance.current.$content.css( 'transition', 'none' );

			instance.isAnimating = false;

			instance.update( true, true, 0 );
		}

	});

}(document, window.jQuery));

;(function (document, $) {
	'use strict';

	var FancyThumbs = function( instance ) {
		this.instance = instance;
		this.init();
	};

	$.extend( FancyThumbs.prototype, {

		$button		: null,
		$grid		: null,
		$list		: null,
		isVisible	: false,

		init : function() {
			var self = this;

			var first  = self.instance.group[0],
				second = self.instance.group[1];

			self.$button = self.instance.$refs.toolbar.find( '[data-fancybox-thumbs]' );

			if ( self.instance.group.length > 1 && self.instance.group[ self.instance.currIndex ].opts.thumbs && (
		    		( first.type == 'image'  || first.opts.thumb  || first.opts.$thumb ) &&
		    		( second.type == 'image' || second.opts.thumb || second.opts.$thumb )
			)) {

				self.$button.on('click', function() {
					self.toggle();
				});

				self.isActive = true;

			} else {
				self.$button.hide();

				self.isActive = false;
			}

		},

		create : function() {
			var instance = this.instance,
				list,
				src;

			this.$grid = $('<div class="fancybox-thumbs"></div>').appendTo( instance.$refs.container );

			list = '<ul>';

			$.each(instance.group, function( i, item ) {

				src = item.opts.thumb || ( item.opts.$thumb ? item.opts.$thumb.attr('src') : null );

				if ( !src && item.type === 'image' ) {
					src = item.src;
				}

				if ( src && src.length ) {
					list += '<li data-index="' + i + '"  tabindex="0" class="fancybox-thumbs-loading"><img data-src="' + src + '" /></li>';
				}

			});

			list += '</ul>';

			this.$list = $( list ).appendTo( this.$grid ).on('click', 'li', function() {
				instance.jumpTo( $(this).data('index') );
			});

			this.$list.find('img').hide().one('load', function() {

				var $parent		= $(this).parent().removeClass('fancybox-thumbs-loading'),
					thumbWidth	= $parent.outerWidth(),
					thumbHeight	= $parent.outerHeight(),
					width,
					height,
					widthRatio,
					heightRatio;

				width  = this.naturalWidth	|| this.width;
				height = this.naturalHeight	|| this.height;


				widthRatio  = width  / thumbWidth;
				heightRatio = height / thumbHeight;

				if (widthRatio >= 1 && heightRatio >= 1) {
					if (widthRatio > heightRatio) {
						width  = width / heightRatio;
						height = thumbHeight;

					} else {
						width  = thumbWidth;
						height = height / widthRatio;
					}
				}

				$(this).css({
					width         : Math.floor(width),
					height        : Math.floor(height),
					'margin-top'  : Math.min( 0, Math.floor(thumbHeight * 0.3 - height * 0.3 ) ),
					'margin-left' : Math.min( 0, Math.floor(thumbWidth  * 0.5 - width  * 0.5 ) )
				}).show();

			})
			.each(function() {
				this.src = $( this ).data( 'src' );
			});

		},

		focus : function() {

			if ( this.instance.current ) {
				this.$list
					.children()
					.removeClass('fancybox-thumbs-active')
					.filter('[data-index="' + this.instance.current.index  + '"]')
					.addClass('fancybox-thumbs-active')
					.focus();
			}

		},

		close : function() {
			this.$grid.hide();
		},

		update : function() {

			this.instance.$refs.container.toggleClass( 'fancybox-show-thumbs', this.isVisible );

			if ( this.isVisible ) {

				if ( !this.$grid ) {
					this.create();
				}

				this.instance.trigger( 'onThumbsShow' );

				this.focus();

			} else if ( this.$grid ) {
				this.instance.trigger( 'onThumbsHide' );
			}

			this.instance.update();

		},

		hide : function() {
			this.isVisible = false;
			this.update();
		},

		show : function() {
			this.isVisible = true;
			this.update();
		},

		toggle : function() {
			this.isVisible = !this.isVisible;
			this.update();
		}

	});

	$(document).on({

		'onInit.fb' : function(e, instance) {
			if ( instance && !instance.Thumbs ) {
				instance.Thumbs = new FancyThumbs( instance );
			}
		},

		'beforeShow.fb' : function(e, instance, item, firstRun) {
			var Thumbs = instance && instance.Thumbs;

			if ( !Thumbs || !Thumbs.isActive ) {
				return;
			}

			if ( item.modal ) {
				Thumbs.$button.hide();

				Thumbs.hide();

				return;
			}

			if ( firstRun && instance.opts.thumbs.autoStart === true ) {
				Thumbs.show();
			}

			if ( Thumbs.isVisible ) {
				Thumbs.focus();
			}
		},

		'afterKeydown.fb' : function(e, instance, current, keypress, keycode) {
			var Thumbs = instance && instance.Thumbs;

			if ( Thumbs && Thumbs.isActive && keycode === 71 ) {
				keypress.preventDefault();

				Thumbs.toggle();
			}
		},

		'beforeClose.fb' : function( e, instance ) {
			var Thumbs = instance && instance.Thumbs;

			if ( Thumbs && Thumbs.isVisible && instance.opts.thumbs.hideOnClose !== false ) {
				Thumbs.close();
			}
		}

	});

}(document, window.jQuery));

;(function (document, window, $) {
	'use strict';

	if ( !$.escapeSelector ) {
		$.escapeSelector = function( sel ) {
			var rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\x80-\uFFFF\w-]/g;
			var fcssescape = function( ch, asCodePoint ) {
				if ( asCodePoint ) {
					if ( ch === "\0" ) {
						return "\uFFFD";
					}

					return ch.slice( 0, -1 ) + "\\" + ch.charCodeAt( ch.length - 1 ).toString( 16 ) + " ";
				}

				return "\\" + ch;
			};

			return ( sel + "" ).replace( rcssescape, fcssescape );
		};
	}

    var currentHash = null;

	var timerID = null;

    function parseUrl() {
        var hash    = window.location.hash.substr( 1 );
        var rez     = hash.split( '-' );
        var index   = rez.length > 1 && /^\+?\d+$/.test( rez[ rez.length - 1 ] ) ? parseInt( rez.pop( -1 ), 10 ) || 1 : 1;
        var gallery = rez.join( '-' );

		if ( index < 1 ) {
			index = 1;
		}

        return {
            hash    : hash,
            index   : index,
            gallery : gallery
        };
    }

	function triggerFromUrl( url ) {
		var $el;

        if ( url.gallery !== '' ) {

			$el = $( "[data-fancybox='" + $.escapeSelector( url.gallery ) + "']" ).eq( url.index - 1 );

            if ( $el.length ) {
				$el.trigger( 'click' );

			} else {

				$( "#" + $.escapeSelector( url.gallery ) + "" ).trigger( 'click' );

			}

        }
	}

	function getGallery( instance ) {
		var opts;

		if ( !instance ) {
			return false;
		}

		opts = instance.current ? instance.current.opts : instance.opts;

		return opts.$orig ? opts.$orig.data( 'fancybox' ) : ( opts.hash || '' );
	}

    $(function() {

		setTimeout(function() {

			if ( $.fancybox.defaults.hash === false ) {
				return;
			}

		    $(document).on({
				'onInit.fb' : function( e, instance ) {
					var url, gallery;

					if ( instance.group[ instance.currIndex ].opts.hash === false ) {
						return;
					}

					url     = parseUrl();
					gallery = getGallery( instance );

					if ( gallery && url.gallery && gallery == url.gallery ) {
						instance.currIndex = url.index - 1;
					}

				},

				'beforeShow.fb' : function( e, instance, current, firstRun ) {
					var gallery;

					if ( current.opts.hash === false ) {
						return;
					}

		            gallery = getGallery( instance );

		            if ( gallery && gallery !== '' ) {

						if ( window.location.hash.indexOf( gallery ) < 0 ) {
			                instance.opts.origHash = window.location.hash;
			            }

						currentHash = gallery + ( instance.group.length > 1 ? '-' + ( current.index + 1 ) : '' );

						if ( 'replaceState' in window.history ) {
							if ( timerID ) {
								clearTimeout( timerID );
							}

							timerID = setTimeout(function() {
								window.history[ firstRun ? 'pushState' : 'replaceState' ]( {} , document.title, window.location.pathname + window.location.search + '#' +  currentHash );

								timerID = null;

							}, 300);

						} else {
							window.location.hash = currentHash;
						}

		            }

		        },

				'beforeClose.fb' : function( e, instance, current ) {
					var gallery, origHash;

					if ( timerID ) {
						clearTimeout( timerID );
					}

					if ( current.opts.hash === false ) {
						return;
					}

					gallery  = getGallery( instance );
					origHash = instance && instance.opts.origHash ? instance.opts.origHash : '';

		            if ( gallery && gallery !== '' ) {

		                if ( 'replaceState' in history ) {
							window.history.replaceState( {} , document.title, window.location.pathname + window.location.search + origHash );

		                } else {
							window.location.hash = origHash;

							$( window ).scrollTop( instance.scrollTop ).scrollLeft( instance.scrollLeft );
		                }
		            }

					currentHash = null;
		        }
		    });

			$(window).on('hashchange.fb', function() {
				var url = parseUrl();

				if ( $.fancybox.getInstance() ) {
					if ( currentHash && currentHash !== url.gallery + '-' + url.index && !( url.index === 1 && currentHash == url.gallery ) ) {
						currentHash = null;

						$.fancybox.close();
					}

				} else if ( url.gallery !== '' ) {
					triggerFromUrl( url );
				}
			});

			$(window).one('unload.fb popstate.fb', function() {
				$.fancybox.getInstance( 'close', true, 0 );
			});

			triggerFromUrl( parseUrl() );

		}, 50);

    });


}(document, window, window.jQuery));
