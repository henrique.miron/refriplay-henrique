angular.module('fortinsocial.directives', [])
.directive('fallbackSrc', function () {
            var fallbackSrc = {
                link: function postLink(scope, iElement, iAttrs) {
                    iElement.bind('error', function () {
                        angular.element(this).attr("src", iAttrs.fallbackSrc);
                    });
                }
            }
            return fallbackSrc;
})
/*
.directive('compileTemplate', function($compile, $parse){
    return {

        link: function(scope, element, attr){
            var parsed = $parse(attr.ngBindHtml);
            //console.log(parsed);
            var getStringValue = function() { return (parsed(scope) || '').toString(); }

            //Recompile if the template changes
            scope.$watch(getStringValue, function() {
                $compile(element, null, -9999)(scope);  //The -9999 makes it skip directives so that we do not recompile ourselves
            });
        }
    }
})
*/

    .directive(
        "fpFloatingLabelInput",
        [
            function ()
            {
                return {
                    restrict: "A",
                    require: "^ngModel",
                    link: function ( scope, elm, attrs, ctrl )
                    {
                        scope.$watch(
                            function ()
                            {
                                if(ctrl.$viewValue)
                                {
                                    return ctrl.$viewValue.trim();
                                }
                                else
                                {
                                    return ctrl.$viewValue;
                                }
                            },
                            function ( value )
                            {
                                if( value && value.length )
                                    angular.element( elm ).addClass( "fp-filled" );
                                else
                                    angular.element( elm ).removeClass( "fp-filled" );
                            }
                        );
                    }
                };
            }
        ]
    )

    .directive(
        "fpCompareTo",
        [
            function ()
            {
                return {
                    require: "ngModel",
                    scope: {
                        otherModelValue: "=fpCompareTo"
                    },
                    link: function(scope, element, attributes, ngModel) {
                        ngModel.$validators.fpCompareTo = function(modelValue) {
                            return modelValue == scope.otherModelValue;
                        };

                        scope.$watch("otherModelValue", function() {
                            ngModel.$validate();
                        });
                    }
                };
            }
        ]
    )
    .directive(
        "fpCompareToNew",
        [
            function ()
            {
                return {
                    require: "ngModel",
                    scope: {
                        otherModelValue: "=fpCompareTo"
                    },
                    link: function(scope, element, attributes, ngModel) {
                        ngModel.$validators.fpCompareTo = function(modelValue) {
                            return modelValue == scope.otherModelValue.$viewValue;
                        };

                        scope.$watch("otherModelValue", function() {
                            ngModel.$validate();
                        });
                    }
                };
            }
        ]
    )
.directive('swifthash', function($compile, $parse) {
    return {
      restrict: 'E',
      link: function(scope, element, attr) {
        scope.$watch(attr.content, function() {
          element.html($parse(attr.content)(scope));
          $compile(element.contents())(scope);
        }, true);
      }
    }
  }).directive('textcomplete', ['Textcomplete', function(Textcomplete) {

          return {
              restrict: 'EA',
              scope: {
                  members: '=',
                  message: '='
              },
              template: '<textarea ng-model=\'message\' type=\'text\'></textarea>',
              link: function(scope, iElement, iAttrs) {
                  var mentions = scope.members;
                  var ta = iElement.find('textarea');
                  var textcomplete = new Textcomplete(ta, [
                      {
                          match: /(^|\s)!(\w*)$/,
                          search: function(term, callback) {
                              callback($.map(mentions, function(mention) {
                                  return mention.toLowerCase().indexOf(term.toLowerCase()) === 0 ? mention : null;
                              }));
                          },
                          maxCount:3,
                          index: 2,
                          replace: function(mention) {
                              return '$1!' + mention + '';
                          }
                      }
                  ]);

                  $(textcomplete).on({
                      'textComplete:select': function (e, value) {
                          scope.$apply(function() {
                              scope.message = value
                          })
                      },
                      'textComplete:show': function (e) {
                          $(this).data('autocompleting', true);
                      },
                      'textComplete:hide': function (e) {
                          $(this).data('autocompleting', false);
                      }
                  });
              }
          }
      }]).filter("BR_money", function(){
    return function(n){
        if(n > 0)
        {
            return n.replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
        }
        else
        {
            return n;
        }
    }
}).directive("fpMaskMoney",function(){
    return function(scope,element,attr){
        return element.maskMoney({ decimal: ',', thousands: '.', precision: 2 ,prefix:'R$ ',allowZero:true},scope.info);
    }
})

.directive(
    "fpResizeOnChange",
    function ()
    {
        return {
            restrict: "A",
            link: function ( scope, elm )
            {
                elm.on( "keyup", function () {
                    window.setTimeout( function () {
                        elm[0].style.height = "auto";
                        elm[0].style.height = elm[0].scrollHeight + "px";
                    }, 1);
                });
            }
        }
    }
).directive('pdfonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {

            $(element).on('change',function(){
                alert('oi');
            })

        }
    };
});
;
