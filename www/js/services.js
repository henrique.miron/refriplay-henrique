angular.module('fortinsocial.services', [])
//var server_url="http://localhost:8100/services/";
    .factory('userauth', function ($http, SERVER_URL,md5) {
        return {
            dologin: function (email, password) {
                var url = SERVER_URL + "services/checkEmailLogin.php?userpwd=" + md5.createHash(password) + "&useremail=" + email;
                return $http.get(url);
            },
            dologinSemMd5: function (email, password) {
                var url = SERVER_URL + "services/checkEmailLogin.php?userpwd=" + password + "&useremail=" + email;
                return $http.get(url);
            }
        };
    })
    .factory('fbauth', function ($http, SERVER_URL) {
        return {
            dologin: function (email, fbId) {
                var url = SERVER_URL + "services/checkEmailLoginFB.php?FbUserId=" + fbId + "&useremail=" + email;
                return $http.get(url);
            }
        };
    })
    .factory('usersignup', function ($http, SERVER_URL,md5) {
        return {
            signup: function (email, password, name) {
                // var url = SERVER_URL + "services/registerUserEmail.php?password=" + password + "&email=" + email + "&name=" + name;
                // return $http.get(url);
                //console.log($http);
                var url = SERVER_URL + "services/signupEmail.php";

                return $http({
                    method: "POST",
                    url: url,
                    data: {
                        'password': md5.createHash(password),
                        'email': email,
                        'name': name
                    }
                });

            }
        };
    })
    .factory('usersignupfb', function ($http, SERVER_URL) {
        return {
            signup: function (email, fbId, name) {
                // var url = SERVER_URL + "services/registerUserEmail.php?password=" + password + "&email=" + email + "&name=" + name;
                // return $http.get(url);
                //console.log($http);
                var url = SERVER_URL + "services/signupFB.php";

                return $http({
                    method: "POST",
                    url: url,
                    data: {
                        'fbId': fbId,
                        'email': email,
                        'name': name
                    }
                });

            }
        };
    })
    .factory('userpass', function ($http, SERVER_URL) {
        return {
            forgotPass: function (email) {
                var url = SERVER_URL + "services/recoveryPass.php";

                return $http({
                    method: "POST",
                    url: url,
                    data: {
                        'email': email
                    }
                });
            }
        };
    })
    .factory('userChangePass', function ($http, SERVER_URL) {
        return {
            changePass: function (code,password) {
                var url = SERVER_URL + "services/changePass.php";

                return $http({
                    method: "POST",
                    url: url,
                    data: {
                        'code': code,
                        'password':password
                    }
                });
            }
        };
    })
    .factory('checkinplaces', function ($http, GOOGLE_KEY) {
        return {
            getnearbyplaces: function (curr_lat, curr_lng) {
                var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + curr_lat + "," + curr_lng + "&key=" + GOOGLE_KEY;
                return $http.get(url);
            }
        };
    })
    .factory('addnewpost', function ($http, SERVER_URL) {
        return {
            addnewpost: function (post_text, location, userid,feed_view,flagged) {

                var url = SERVER_URL + "services/addNewPost.php";
                return $http({
                    url: url,
                    method: "POST",
                    data: {
                        'post_text': post_text,
                        'location': location,
                        'userid': userid,
                        'feed_view':feed_view,
                        'flagged':flagged
                    }
                });
            }
        };
    })

    .factory('Comments', function($http, SERVER_URL) {
        return {
            getCommentsByPost: function(postid) {
                var url = SERVER_URL + "services/getCommentsByPost.php?postId=" + postid;
                return $http.get(url);
            }
        }
    })
    .factory('getuserfeeds', function ($http, SERVER_URL) {
        return {
            getuserfeeds: function (userid, page,hashtagid) {
                var url = SERVER_URL + "services/getFeedsForUser.php?page=" + page + "&userid=" + userid + "&hashtagid="+hashtagid;
                // var url = SERVER_URL + "services/getFeedsForUser.php?userid=" + userid;
                return $http.get(url);
            },
            getuserfeedsAll: function (userid, page) {
                var url = SERVER_URL + "services/getFeedsForUser.php?page=" + page + "&userid=" + userid + "&hashtagid=0&all="+true;
                // var url = SERVER_URL + "services/getFeedsForUser.php?userid=" + userid;
                return $http.get(url);
            },
            getpostdetails: function ( postId, userId ) {
                var url = SERVER_URL + "services/getPostDetails.php?postId=" + postId + "&userId=" + userId ;

                return $http.get(url);
            }
        };
    })

    .factory('addpostlike', function ($http, SERVER_URL) {
        return {
            addpostlike: function (userid, postid) {
                var url = SERVER_URL + "services/addPostLike.php?post_id=" + postid + "&user_id=" + userid;
                return $http.get(url);
            }
        };
    })

    .factory('addpostcomment', function ($http, SERVER_URL) {
        return {
            addpostcomment: function (userid, postid, comment_text) {
                var url = SERVER_URL + "services/addPostComment.php?post_id=" + postid + "&user_id=" + userid + "&comment_text=" + comment_text;
                return $http.get(url);
            }
        };
    })

    .factory('addfollowing', function ($http, SERVER_URL) {
        return {
            addfollowing: function (userid, followingid) {
                var url = SERVER_URL + "services/addFollowing.php?userid=" + userid + "&followingid=" + followingid;
                return $http.get(url);
            }
        };
    })

    .factory('unfollow', function ($http, SERVER_URL) {
        return {
            unfollow: function (userid, followingid) {
                var url = SERVER_URL + "services/unfollow.php?userid=" + userid + "&followingid=" + followingid;
                return $http.get(url);
            }
        };
    })

    .factory('getfriends', function ($http, SERVER_URL) {
        return {
            getfriends: function (userid, type) {
                var url = '';
                if (type === 'following') {
                    url = SERVER_URL + "services/getFollowingByUser.php?userid=" + userid;
                } else if (type === 'followers') {
                    url = SERVER_URL + "services/getFollowersForUser.php?userid=" + userid;
                } else if (type === 'suggestions') {
                    url = SERVER_URL + "services/getRandomUsers.php?userid=" + userid;
                }

                return $http.get(url);
            }
        };
    })

    .factory('updateprofile', function ($http, SERVER_URL) {
        return {
            updateprofile: function (username, password, feed_view, gender, userid,cpf) {
                var url = SERVER_URL + "services/updateProfile.php";
                return $http({
                    url: url,
                    method: "POST",
                    data: {
                        'full_name': username,
                        'password': password,
                        'feed_view': feed_view,
                        'gender': gender,
                        'userid': userid,
                        'cpf':cpf
                    }
                });
            },
            updateprofileinfo: function (field,value,userid) {
                var url = SERVER_URL + "services/updateProfileInfo.php";
                return $http({
                    url: url,
                    method: "POST",
                    data: {
                        'field': field,
                        'info': value,
                        'userid': userid
                    }
                });
            },
            getFieldInfo: function(field)
            {
                var url = SERVER_URL + "services/getEditAccountInfos.php?field=" + field;
                return $http.get(url);
            },
            getProfileMetrics: function(userid){
                var url = SERVER_URL + "services/getProfileMetrics.php?userid=" + userid;
                return $http.get(url);
            }
        };
    })

    .factory('updatepushid', function ($http, SERVER_URL) {
        return {
            updatepushid: function (pushid, userid) {

                var url = SERVER_URL + "services/updatePushId.php";
                return $http({
                    url: url,
                    method: "POST",
                    data: {
                        'pushid': pushid,
                        'userid': userid
                    }
                });
            }
        };
    })

    .factory('getfeedprofile', function ($http, SERVER_URL) {
        return {
            getfeedprofile: function (userid) {
                var url = SERVER_URL + "services/getFeedProfileForUser.php?&userid=" + userid;
                return $http.get(url);
            }
        };
    })

    .factory('getotheruserprofile', function ($http, SERVER_URL) {
        return {
            getotheruserprofile: function (userid,loggedinuserid) {
                var url = SERVER_URL + "services/getFeedProfileForOtherUser.php?&userid=" + userid +"&loggedinuserid="+loggedinuserid;
                return $http.get(url);
            }
        };
    })

    .factory('getselffeed', function ($http, SERVER_URL) {
        return {
            getselffeed: function (userid, page) {
                var url = SERVER_URL + "services/getFeedsForUser.php?page=" + page + "&userid=" + userid + "&self=yes";
                return $http.get(url);
            }
        };
    })
    .factory('getuserphotos', function ($http, SERVER_URL) {
        return {
            getuserphotos: function (userid) {
                var url = SERVER_URL + "services/getPhotosForUser.php?userid=" + userid;
                return $http.get(url);
            }
        };
    })
    .factory('gettrendinghashtags', function ($http, SERVER_URL) {
        return {
            gettrendinghashtags: function (userid) {
                var url = SERVER_URL + "services/getTrendingHashtags.php";
                return $http.get(url);
            }
        };
    })
    .factory('getstates', function ($http, SERVER_URL) {
        return {
            getstates: function (lat,lng) {
                var url = SERVER_URL + "services/getStates.php?lat="+lat+"&lng="+lng;
                return $http.get(url);
            }
        };
    })
    .factory('setFeedState', function ($http, SERVER_URL) {
        return {
            setFeedState: function (state,userId) {
                var url = SERVER_URL + "services/setFeedState.php";
                return $http({
                    url: url,
                    method: "POST",
                    data: {
                        'state': state,
                        'userId': userId
                    }
                });
            }
        };
    })
    .factory('deletepost', function ($http, SERVER_URL) {
            return {
                deletepost: function (postid) {
                    var url = SERVER_URL + "services/deletePostByUser.php?postid="+postid;
                    return $http.get(url);
                }
            };
    })
    .factory('adv_report', function ($http, SERVER_URL) {
        return {
            count: function (advid,userid,state_view) {
                var url = SERVER_URL + "services/adv_report.php?userid="+userid+"&state_view="+state_view+"&advid="+advid;
                return $http.get(url);
            }
        };
    })
    .factory('Insurance', function($http, SERVER_URL) {
        return {
            getText : function() {
                var url = SERVER_URL + "services/getInsuranceText.php";
                return $http.get(url);                
            }
        }
    })
    .factory('Tools', function ($http, SERVER_URL) {
        return {
            getTools: function () {
                var url = SERVER_URL + "services/getTools.php";
                return $http.get(url);
            }
        };
    })
    .factory('ErrorCategory', function ($http, SERVER_URL) {
        return {
            getErrorCategory: function () {
                var url = SERVER_URL + "services/getErrorCategory.php";
                return $http.get(url);
            }
        };
    })
    .factory('Marks', function ($http, SERVER_URL) {
        return {
            getMarks: function () {
                var url = SERVER_URL + "services/getMarks.php";
                return $http.get(url);
            }
        };
    })
    .factory('Products', function ($http, SERVER_URL) {
        return {
            getProducts: function (mark_id) {
                var url = SERVER_URL + "services/getProducts.php?mark_id="+mark_id;
                return $http.get(url);
            },
            getProductsErrorList: function (products_id) {
                var url = SERVER_URL + "services/getErrorList.php?products_id="+products_id;
                return $http.get(url);
            },
        };
    })
    .factory('Error', function ($http, SERVER_URL) {
        return {
            getError: function (error_id) {
                var url = SERVER_URL + "services/getErrorDetail.php?id="+error_id;
                return $http.get(url);
            },
            getErrorAll: function (error_id) {
                var url = SERVER_URL + "services/getErrorListAll.php";
                return $http.get(url);
            },
        };
    })
    .factory('Solutions', function ($http, SERVER_URL) {
        return {
            getSolutions: function (error_id) {
                var url = SERVER_URL + "services/getSolution.php?error_list_id="+error_id;
                return $http.get(url);
            },
            addSolution: function (user_id,error_list_id,descripition) {
                var url = SERVER_URL + "services/addSolution.php";
                return $http({
                    url: url,
                    method: "POST",
                    data: {
                        'user_id': user_id,
                        'descripition': descripition,
                        'error_list_id': error_list_id
                    }
                });
            },
            disableSolution: function (solution_id) {
                var url = SERVER_URL + "services/disableSolution.php?id="+solution_id;
                return $http.get(url);
            }
        };
    })
    .factory('Likes', function ($http, SERVER_URL) {
        return {
            getLikes: function (solutions_id) {
                var url = SERVER_URL + "services/getLikes.php?solutions_id="+solutions_id;
                console.log("URL DA PUTARIA",url);
                return $http.get(url);
            },
            addLike: function (user_id,like,solutions_id) {
                var url = SERVER_URL + "services/addLike.php";
                return $http({
                    url: url,
                    method: "POST",
                    data: {
                        'user_id': user_id,
                        'like': like,
                        'solutions_id': solutions_id
                    }
                });
            }
        };
    })
    .factory('Notifications', function($http, SERVER_URL) {
        return {
            getUserNotifications : function (userid, page) {
                if(page != 0) {
                    var url = SERVER_URL + "services/getNotificationsByUser.php?userid=" + userid + "&page=" + page;    
                } else {
                    var url = SERVER_URL + "services/getNotificationsByUser.php?userid=" + userid;
                }
                return $http.get(url);
            },
            changeNotificationStatus : function (notificationid) {
                var url = SERVER_URL + "services/changeNotificationStatus.php?notificationid=" + notificationid;
                return $http.get(url);
            },
            getQntNotifications : function (userid) {
                var url = SERVER_URL + "services/getQntNotifications.php?userid=" + userid;
                return $http.get(url);
            },
            readAll : function (userid) {
                var url = SERVER_URL + "services/updateAllNotifications.php?userid="+userid;
                return $http.get(url);
            }
        };
    })
;
