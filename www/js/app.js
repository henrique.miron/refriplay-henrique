// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module(
        'fortinsocial',   
        [
            'ionic',
            'fortinsocial.controllers',
            'fortinsocial.config',
            'fortinsocial.services',
            'ngMessages',
            'ion-gallery',
            'ngCordova',
            'fortinsocial.filters',
            'fortinsocial.directives',
            'angular-md5',
            'ngCookies',
            'pascalprecht.translate',
            'ngAnimate',
            'ngTextcomplete',
            'ionic-modal-select',
            'ngMask',
            'pdf-viewer',
        ]
    )
    .run(function ($ionicPlatform,$http, $translate, $location, $state, $rootScope,$window,$ionicHistory) {
            $ionicPlatform.ready(function () {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)

                $rootScope.platformName = 'browser';
                if($ionicPlatform.is('android'))
                {
                    $rootScope.platformName = 'android';
                }

                if($ionicPlatform.is('ios'))
                {
                    $rootScope.platformName = 'ios';
                }


                $ionicPlatform.registerBackButtonAction(function(e){
                    if($location.path()=='/tab/feeds'){
                        $('.fancybox-button--close').click();
                        e.preventDefault();
                    }else{
                        $ionicHistory.goBack();
                    }
                }, 100);

                var pushid = "f86625e9-5a69-4157-b696-640fa6dfdc62"; // app id from one signal
                var googleprojectno = "refriplay-167920"; // google project id

                if(window.plugins && window.plugins.OneSignal)
                {

                    var notificationOpenedCallback = function (jsonData) {
                        var redirectTo = jsonData.notification.payload.additionalData.redirectTo;
                        $rootScope.$broadcast( 'app:notification', jsonData.notification.payload.additionalData );
                    };

                    window.plugins.OneSignal
                        .startInit(pushid)
                        .handleNotificationOpened(notificationOpenedCallback)
                        .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
                        .endInit();

                    window.plugins.OneSignal.getIds(function (ids) {
                        // alert("OneSignalPushToken" + ids.pushToken);
                        // alert('getIds: ' + JSON.stringify(ids));
                        localStorage.setItem("playerid", ids.userId);

                    });


                }



                if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                    cordova.plugins.Keyboard.disableScroll(true);

                }
                if (window.StatusBar) {
                    // org.apache.cordova.statusbar required
                    StatusBar.styleDefault();
                }

                // For google analytics
                if (window.ga) {
                    window.ga.startTrackerWithId('UA-106020434-1'); // add your google analytics Id here
                    window.ga.trackView('Iniciou o app');
                    if($window.localStorage['profile'])
                    {
                        var profiledata = JSON.parse($window.localStorage['profile']);
                        window.ga.startTrackerWithId(profiledata.userid); // add your google analytics Id here
                    }

                }

                // for analytics end

                // For Push Notifications

                // For Push Notifications ends here

            });


            document.addEventListener("deviceready", function(){

                navigator.globalization.getPreferredLanguage(function (language) {
                    var lang = language.value.substr(0, 2);
                    $translate.use(lang);
                });

            }, false);

             // For Admob
           /*  if ((/(ipad|iphone|ipod|android|windows phone)/i.test(navigator.userAgent))) {
             document.addEventListener('deviceready', initApp, false);
             } else {
             initApp();
             }

             var admobid = {};
             if (/(android)/i.test(navigator.userAgent)) {
             admobid = {// for Android
             banner: 'ca-app-pub-6192865524332826/6700998798'
             };
             } else if (/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
             admobid = {// for iOS
             banner: 'ca-app-pub-6192865524332826/2131198391'
             };
             }

             function initApp() {
             //  alert("init ad"+AdMob);
             if (!AdMob) {
             alert('admob plugin not ready');
             return false;
             }
             initAd();
             // display the banner at startup
             //createSelectedBanner();
             }

             function initAd() {


             // it will display smart banner at top center, using the default options
             if (AdMob)
             AdMob.createBanner({
             adId: admobid.banner,
             bannerId: admobid.banner,
             position: AdMob.AD_POSITION.BOTTOM_CENTER,
             autoShow: true,
             isTesting: false,
             success: function () {
             console.log('banner created');
             },
             error: function () {
             console.log('failed to create banner');
             }
             });

             } */
             // For Admob ends here

        })

        .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, ionGalleryConfigProvider,$httpProvider, $translateProvider,$sceDelegateProvider) {
            //$httpProvider.defaults.withCredentials = true;
            //$httpProvider.defaults.headers.common['Authorization'] = 'Basic cmVmcmlwbGF5c2VydmljZTpiZjlhNDIyZWUwMTgwZjI5ZWEzN2Q5NTFkNDkxNzdjZg==';
            $ionicConfigProvider.tabs.position('bottom');
            // Ionic uses AngularUI Router which uses the concept of states
            // Learn more here: https://github.com/angular-ui/ui-router
            // Set up the various states which the app can be in.
            // Each state's controller can be found in controllers.js

            $ionicConfigProvider.backButton.previousTitleText(false);
            $ionicConfigProvider.backButton.icon('ion-chevron-left');
            $ionicConfigProvider.backButton.text('');

            /*cloudinaryProvider
                .set("cloud_name", "refriplay-com-br")
                .set("secure", true);*/



            // translate
            $translateProvider.useMessageFormatInterpolation();
            $translateProvider.preferredLanguage('pt');
            $translateProvider.useStaticFilesLoader({
                prefix: 'languages/',
                suffix: '.json'
            });


            $stateProvider

                    // setup an abstract state for the tabs directive

                    .state('landing', {
                        url: '/landing',
                        templateUrl: 'templates/landing.html',
                        controller: 'LandingCtrl'
                    })

                    .state('tab', {
                        cache:false,
                        url: '/tab',
                        abstract: true,
                        templateUrl: 'templates/tabs.html',
                        controller: 'TabsCtrl',
                    })

                    .state('tab.feeds', {
                        url: '/feeds',
                        views: {
                            'tab-feeds': {
                                templateUrl: 'templates/tab-feeds.html',
                                controller: 'FeedsCtrl',

                            }
                        },
                        params: {
                                    hashtagid: 0,
                                    hashtag: null
                                }
                    })
                    .state('tab.feeds-brasil', {
                        url: '/feeds/{brasil}',
                        views: {
                            'tab-feeds': {
                                templateUrl: 'templates/tab-feeds.html',
                                controller: 'FeedsCtrl',

                            }
                        },
                        params: {
                                    hashtagid: 0,
                                    hashtag: null
                                }
                    })

                    .state('tab.newpost', {
                        url: '/newpost',
                        views: {
                            'tab-newpost': {
                                templateUrl: 'templates/tab-newpost.html',
                                controller: 'NewpostCtrl'
                            }
                        }
                    })

                    .state('post', {
                        url: '/post/{postId:int}',
                        templateUrl: 'templates/post.html',
                        controller: 'FeedsCtrl'
                    })

                    .state('tab.friends', {
                        url: '/friends',
                        views: {
                            'tab-friends': {
                                templateUrl: 'templates/tab-friends.html',
                                controller: 'FriendsCtrl'
                            }
                        }
                    })
                    .state('tab.profile', {
                        url: '/profile',
                        views: {
                            'tab-profile': {
                                templateUrl: 'templates/tab-profile.html',
                                controller: 'ProfileCtrl'
                            }
                        }
                    })
                    .state('tab.chat-detail', {
                        url: '/chats/:chatId',
                        views: {
                            'tab-chats': {
                                templateUrl: 'templates/chat-detail.html',
                                controller: 'ChatDetailCtrl'
                            }
                        }
                    })

                    .state('tab.account', {
                        url: '/account',
                        views: {
                            'tab-account': {
                                templateUrl: 'templates/tab-account.html',
                                controller: 'AccountCtrl'
                            }
                        }
                    })

                    .state('tab.notifications', {
                        url: '/notifications',
                        views: {
                            'tab-notifications' : {
                                templateUrl: 'templates/tab-notifications.html',
                                controller: 'NotificationsCtrl'
                            }
                        }
                    })

                    .state('tab.account-edit', {
                        url: '/account/edit/{field}',
                        views: {
                            'tab-account': {
                                templateUrl: 'templates/tab-account-edit.html',
                                controller: 'AccountCtrl'
                            }
                        }
                    })

                    .state('tab.account-modal', {
                        url: '/account/modal',
                        views: {
                            'tab-account': {
                                templateUrl: 'templates/tab-account-modal.html',
                                controller: 'AccountCtrl'
                            }
                        }
                    })


                    .state('tab.budget', {
                        url: '/budget',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-budget.html',
                                controller: 'BudgetCtrl'
                            }
                        }
                    })

                    .state('tab.produtos', {
                        url: '/produtos',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-produtos.html',
                                controller: 'ProdutosCtrl'
                            }
                        }
                    })
                    
                    .state('tab.produtos-new', {
                        url: '/produtos-new',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-produtos-form.html',
                                controller: 'ProdutosCtrl'
                            }
                        }
                    })

                    .state('tab.produtos-edit', {
                        url: '/produtos-edit/{produtoId}',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-produtos-form.html',
                                controller: 'ProdutosCtrl'
                            }
                        }
                    })


                    .state('tab.servicos', {
                        url: '/servicos',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-servicos.html',
                                controller: 'ServicosCtrl'
                            }
                        }
                    })
                    
                    .state('tab.servicos-new', {
                        url: '/servicos-new',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-servicos-form.html',
                                controller: 'ServicosCtrl'
                            }
                        }
                    })

                    .state('tab.servicos-edit', {
                        url: '/servicos-edit/{servicoId}',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-servicos-form.html',
                                controller: 'ServicosCtrl'
                            }
                        }
                    })

                    .state('tab.clientes', {
                        url: '/clients',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-clientes.html',
                                controller: 'ClientsCtrl'
                            }
                        }
                    })

                    .state('tab.clientes-new', {
                        url: '/clients-new',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-clientes-edit.html',
                                controller: 'ClientsCtrl'
                            }
                        }
                    })
                    

                    .state('tab.clientes-edit', {
                        url: '/clients-edit/{clientId}',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-clientes-edit.html',
                                controller: 'ClientsCtrl'
                            }
                        }
                    })

                    .state('tab.budget-new', {
                        url: '/budget-new',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-budget-new.html',
                                controller: 'NewBudgetCtrl'
                            }
                        }
                    })
                    .state('tab.budget-edit', {
                        url: '/budget-new/{budgetId}',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-budget-new.html',
                                controller: 'NewBudgetCtrl'
                            }
                        }
                    })
                    .state('tab.budget-review', {
                        url: '/budget-review',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-budget-budget-review.html',
                                controller: 'NewBudgetCtrl'
                            }
                        }
                    })
                .state('tab.budget-review-edit', {
                    url: '/budget-review/{budgetId}',
                    views: {
                        'tab-tools': {
                            templateUrl: 'templates/tab-budget-budget-review.html',
                            controller: 'NewBudgetCtrl'
                        }
                    }
                })
                .state('tab.budget-pdf', {
                    url: '/budget-pdf/{budgetId}',
                    views: {
                        'tab-tools': {
                            templateUrl: 'templates/tab-budget-pdf.html',
                            controller: 'budgetPdfCtr'
                        }
                    }
                })

                    // For auth tabs
                    .state('tabauth', {
                        url: '/tabauth',
                        abstract: true,
                        templateUrl: 'templates/tabs-auth.html'
                    })
                    .state('login-intro', {
                        url: '/login-intro',
                        templateUrl: 'templates/login-intro.html',
                        controller: 'LoginCtrl'
                    })
                    .state('login', {
                        url: '/login',
                        templateUrl: 'templates/login.html',
                        controller: 'LoginCtrl'
                    })
                    .state('criar-conta', {
                        url: '/criar-conta',
                        templateUrl: 'templates/criar-conta.html',
                        controller: 'SignupCtrl'
                    })
                    .state('esqueceu-a-senha', {
                        url: '/esqueceu-a-senha',
                        templateUrl: 'templates/esqueceu-a-senha.html',
                        controller: 'SignupCtrl'
                    })
                    .state('senha-confirmacao', {
                        url: '/senha-confirmacao',
                        templateUrl: 'templates/senha-confirmacao.html',
                        controller: 'SignupCtrl'
                    })
                    .state('aguardando-aprovacao', {
                        url: '/aguardando-aprovacao',
                        templateUrl: 'templates/aguardando-aprovacao.html',
                        controller: 'SignupCtrl'
                    })
                    .state('conta-reprovada', {
                        url: '/conta-reprovada',
                        templateUrl: 'templates/conta-reprovada.html',
                        controller: 'SignupCtrl'
                    })
                    .state('photogallery', {
                        url: '/photogallery',
                        templateUrl: 'templates/photo-gallery.html',
                        controller: 'PhotoGalleryCtrl',
                        params: {
                            fromprofile: null,
                            userid:0
                        },
                    })
                    .state('profilepage', {
                        url: '/profilepage',
                        params: {
                            userid: 0,
                            currpage: null
                        },
                        templateUrl: 'templates/profile-page.html',
                        controller: 'ProfilePageCtrl'
                    })
                    .state('trendinghashtags', {
                        url: '/trendinghashtags',
                        templateUrl: 'templates/trendinghashtags.html',
                        controller: 'TrendinghashtagsCtrl'
                    })
                    .state('selecionar-estado', {
                        url: '/selecionar-estado',
                        templateUrl: 'templates/selecionar-estado.html',
                        controller: 'estadosCtrl'
                    })
                    .state('tab.tools', {
                        cache: false,
                        url: '/tools',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-tools.html',
                                controller: 'ToolsCtrl'
                            }
                        }
                    })
                    .state('tab.tools-category', {
                        cache: false,
                        url: '/tools/category',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-tools-category.html',
                                controller: 'ErrorCategoryCtrl'
                            }
                        }
                    })
                    .state('tab.tools-insurance', {
                        cache: false,
                        url: '/tools/insurance',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/tab-insurance.html',
                                controller: 'InsuranceCtrl'
                            }
                        }
                    })
                    .state('tab.marks', {
                        cache: false,
                        url: '/marks',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/marks.html',
                                controller: 'MarksCtrl'
                            }
                        }
                    })
                    .state('tab.products-list', {
                        cache: false,
                        url: '/products/list/:mark_id',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/products-list.html',
                                controller: 'ProductsCtrl'
                            }
                        }
                    })
                    .state('tab.products-error-list', {
                        cache: false,
                        url: '/products/list/error/:products_id',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/products-error-list.html',
                                controller: 'ProductsCtrl'
                            }
                        }
                    })
                    .state('tab.products-error-detail', {
                        cache: false,
                        url: '/products/list/error/detail/:error_id',
                        views: {
                            'tab-tools': {
                                templateUrl: 'templates/products-error-detail.html',
                                controller: 'ErrorCtrl'
                            }
                        }
                    })
                    ;
            ionGalleryConfigProvider.setGalleryConfig({
                action_label: 'Done',
                toggle: false,
                row_size: 3,
                fixed_row_size: false
            });

            // if none of the above states are matched, use this as the fallback
            $urlRouterProvider.otherwise('login-intro');
            //$urlRouterProvider.otherwise('/landing');

        });
