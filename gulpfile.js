var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var strip = require('gulp-strip-comments');

var paths = {
  sass: ['./scss/**/*.scss'],
  js: ['./www/js/**/*.js']
};

gulp.task('ionic:watch:before', ['default', 'watch']);

gulp.task('default', ['sass', 'scripts', 'concat-libs']);

gulp.task('scripts', function() {
  return gulp.src([
    "./www/js/app.js",
    "./www/js/config.js",
    "./www/js/directives.js",
    "./www/js/filters.js",
    "./www/js/controllers.js",
    "./www/js/services.js",
    "./www/js/jquery.fancybox.js"
  ])
  .pipe(strip())
  .pipe(concat('all.js'))
  .pipe(gulp.dest('./www/js/'));
});

gulp.task('concat-libs', function() {
  gulp.src([
    "./www/lib/ng-textcomplete/ng-textcomplete.min.js",
    "./www/lib/ionic-modal-select/dist/ionic-modal-select.js",
    "./www/lib/ngMask/dist/ngMask.min.js",
    "./www/lib/jquery-maskmoney/dist/jquery.maskMoney.min.js"
  ])
  
  .pipe(concat('script.js'))
  .pipe(gulp.dest('./www/js/'));

  gulp.src([
    "./www/lib/angular-md5/angular-md5.min.js",
    "./www/lib/angular-cookies/angular-cookies.min.js",
    "./www/lib/angular-translate/angular-translate.min.js",
    "./www/lib/angular-translate-storage-cookie/angular-translate-storage-cookie.min.js",
    "./www/lib/angular-translate-storage-local/angular-translate-storage-local.min.js",
    "./www/lib/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js",
    "./www/lib/messageformat/messageformat.min.js",
    "./www/lib/angular-translate-interpolation-messageformat/angular-translate-interpolation-messageformat.min.js",
    './www/lib/js-sha1/build/sha1.min.js',
    "./www/lib/pdfjs-dist/build/pdf.combined.js",
    "./www/lib/ng-pdf-viewer/src/ng-pdf-viewer.min.js",
  ]).pipe(concat('libs.js'))
    .pipe(gulp.dest('./www/js/'));
})


gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')

  
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'));


    gulp.src('./scss/default.scss')
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(gulp.dest('./www/css/'))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest('./www/css/'))
        .on('end', done);
		
	gulp.src("./scss/fonts/**.*")
      .pipe(gulp.dest('./www/css/fonts'));	
	  
    gulp.src("./scss/feather.css")
  
    
      .pipe(gulp.dest('./www/css'));
	  
    gulp.src("./scss/style.css")
  
    
      .pipe(gulp.dest('./www/css'));

    gulp.src("./node_modules/angular-md5/*")
        .pipe(gulp.dest('./www/lib/angular-md5'));

    gulp.src([
      './www/css/style.css', 
      './www/css/default.css', 
      './www/css/feather.css'])
      .pipe(concat('all.css'))
        .pipe(gulp.dest('./www/css'));

});

gulp.task('watch', function() {
  gulp.watch(paths.sass,  ['sass']);
  gulp.watch(paths.js, ['scripts']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
